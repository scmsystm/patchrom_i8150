.class Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;
.super Landroid/os/Handler;
.source "PasswordUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PasswordUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 330
    iput-object p1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .parameter "msg"

    .prologue
    .line 332
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "status"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 334
    .local v5, status:Ljava/lang/String;
    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 335
    .local v2, devTok:[Ljava/lang/String;
    const/4 v4, 0x0

    .line 336
    .local v4, doUpdateText:Z
    const/4 v1, 0x0

    .line 338
    .local v1, batchFail:Z
    const-string v7, "PasswordUnlockScreen"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "PasswordUnlockScreen got: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const-string v7, "batchfail"

    const/4 v8, 0x2

    aget-object v8, v2, v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 341
    const/4 v1, 0x1

    .line 345
    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$500()Z

    move-result v7

    if-nez v7, :cond_1

    .line 346
    const/4 v7, 0x1

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$502(Z)Z

    .line 347
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #calls: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->removePasswordUI()V
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$600(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)V

    .line 348
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #getter for: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$700(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    move-result-object v7

    const v8, 0x124f80

    invoke-interface {v7, v8}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->pokeWakelock(I)V

    .line 349
    if-nez v1, :cond_1

    .line 350
    new-instance v7, Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    iget-object v8, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$400()Z

    move-result v9

    invoke-direct {v7, v8, v9}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;-><init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;Z)V

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$802(Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;)Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    .line 351
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$900()Landroid/widget/RelativeLayout;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 352
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1000()Landroid/view/View;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 356
    :cond_1
    if-eqz v1, :cond_5

    .line 357
    const-string v7, "1"

    const/4 v8, 0x4

    aget-object v8, v2, v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$402(Z)Z

    .line 358
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$800()Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$800()Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;->stop()V

    .line 359
    :cond_2
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$900()Landroid/widget/RelativeLayout;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 360
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1000()Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 361
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$400()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 362
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #getter for: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$000(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/widget/TextView;

    move-result-object v7

    const v8, 0x10404ad

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 367
    :goto_1
    const/4 v7, 0x0

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$502(Z)Z

    .line 368
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #getter for: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$700(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    move-result-object v7

    invoke-interface {v7}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->pokeWakelock()V

    .line 428
    :goto_2
    return-void

    .line 342
    :cond_3
    const-string v7, "Encryption"

    const/4 v8, 0x3

    aget-object v8, v2, v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 343
    const/4 v7, 0x1

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$402(Z)Z

    goto/16 :goto_0

    .line 364
    :cond_4
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #getter for: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$000(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/widget/TextView;

    move-result-object v7

    const v8, 0x10404ae

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 372
    :cond_5
    const/4 v7, 0x1

    aget-object v7, v2, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1102(I)I

    .line 373
    const/4 v7, 0x2

    aget-object v3, v2, v7

    .line 375
    .local v3, device:Ljava/lang/String;
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1200()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 376
    invoke-static {v3}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1202(Ljava/lang/String;)Ljava/lang/String;

    .line 377
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1308()I

    .line 378
    const/4 v4, 0x1

    .line 381
    :cond_6
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1100()I

    move-result v7

    const/16 v8, 0x64

    if-ge v7, v8, :cond_a

    .line 382
    if-eqz v4, :cond_7

    .line 383
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$400()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 384
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #getter for: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1500(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x10404a9

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1402(Ljava/lang/String;)Ljava/lang/String;

    .line 410
    :cond_7
    :goto_3
    if-eqz v4, :cond_8

    .line 411
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #getter for: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$000(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/widget/TextView;

    move-result-object v7

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1400()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 413
    :cond_8
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1600()Landroid/widget/ProgressBar;

    move-result-object v7

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1100()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 416
    const-string v7, "data"

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1200()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 417
    const/4 v7, 0x1

    const-string v8, "dbdata"

    invoke-static {v8}, Landroid/deviceencryption/DeviceEncryptionManager;->isTargetToEncrypt(Ljava/lang/String;)Z

    move-result v8

    if-ne v7, v8, :cond_d

    .line 418
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1100()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "% (1/2)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1702(Ljava/lang/String;)Ljava/lang/String;

    .line 425
    :goto_4
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1800()Landroid/widget/TextView;

    move-result-object v7

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1700()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 386
    :cond_9
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #getter for: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1500(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x10404aa

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1402(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3

    .line 389
    :cond_a
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1100()I

    move-result v7

    const/16 v8, 0x64

    if-ne v7, v8, :cond_c

    .line 390
    const/4 v4, 0x1

    .line 391
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$400()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 392
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #getter for: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1500(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x10404ab

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1402(Ljava/lang/String;)Ljava/lang/String;

    .line 397
    :goto_5
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1300()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_7

    .line 398
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    iget-object v7, v7, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEncryptHandler:Landroid/os/Handler;

    invoke-virtual {v7}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 399
    .local v6, tmpmsg:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 400
    .local v0, b:Landroid/os/Bundle;
    const-string v7, "status"

    invoke-virtual {v0, v7, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    invoke-virtual {v6, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 403
    const-wide/16 v7, 0x1

    invoke-virtual {p0, v6, v7, v8}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_3

    .line 394
    .end local v0           #b:Landroid/os/Bundle;
    .end local v6           #tmpmsg:Landroid/os/Message;
    :cond_b
    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;->this$0:Lcom/android/internal/policy/impl/PasswordUnlockScreen;

    #getter for: Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1500(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x10404ac

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1402(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_5

    .line 406
    :cond_c
    const-string v7, "PasswordUnlockScreen"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Got invalid progressCnt: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 420
    :cond_d
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1100()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1702(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_4

    .line 422
    :cond_e
    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1300()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_f

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1100()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1702(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_4

    .line 423
    :cond_f
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1100()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "% (2/2)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->access$1702(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_4
.end method
