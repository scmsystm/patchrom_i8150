.class public Lcom/android/internal/policy/impl/PasswordUnlockScreen;
.super Landroid/widget/LinearLayout;
.source "PasswordUnlockScreen.java"

# interfaces
.implements Lcom/android/internal/policy/impl/KeyguardScreen;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$InfoCallback;
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;,
        Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;
    }
.end annotation


# static fields
.field private static final ERROR_MESSAGE_TIMEOUT:J = 0x7d0L

.field private static final MAINLAYOUT_INDEX:I = 0x0

.field private static final MINIMUM_PASSWORD_LENGTH_BEFORE_REPORT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "PasswordUnlockScreen"

.field private static final TIMEOUT_AFTER_VALID_PWD:I = 0x124f80

.field private static encrypt:Z

.field private static mBatchData:Z

.field private static mCryptImage:Landroid/widget/ImageView;

.field private static mCryptImageSwapper:Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

.field private static mCurrDevice:Ljava/lang/String;

.field private static mCurrDeviceCnt:I

.field private static mDoCryptVolume:Z

.field private static mEncryptListener:Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;

.field private static mFillView:Landroid/view/View;

.field private static mIsInDeadlineCountDown:Z

.field private static mLastEncryptionText:Ljava/lang/String;

.field private static mLastProgressCnt:I

.field private static mLastProgressText:Ljava/lang/String;

.field private static mProgressBar:Landroid/widget/ProgressBar;

.field private static mProgressBarLayout:Landroid/widget/RelativeLayout;

.field private static mProgressBarText:Landroid/widget/TextView;

.field private static mRamBoot:Z


# instance fields
.field private final mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

.field private mConfiguration:Landroid/content/res/Configuration;

.field private mContext:Landroid/content/Context;

.field private mCountdownTimer:Landroid/os/CountDownTimer;

.field private mCreationHardKeyboardHidden:I

.field private mCreationOrientation:I

.field private mEmergencyCallButton:Landroid/widget/Button;

.field mEncryptHandler:Landroid/os/Handler;

.field mEncryptService:Landroid/os/storage/IEncryptService;

.field private mHandler:Landroid/os/Handler;

.field private mIsRecoveryMode:Z

.field private mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

.field private mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLockscreenWallpaperUpdater:Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;

.field private mPasswordEntry:Landroid/widget/EditText;

.field private mPasswordLockscreenWallpaper:Landroid/widget/RelativeLayout;

.field private mTitle:Landroid/widget/TextView;

.field private final mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

.field passwordChangeNeeded:Z

.field private r:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 110
    sput-boolean v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mIsInDeadlineCountDown:Z

    .line 117
    sput-boolean v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mRamBoot:Z

    .line 124
    sput-boolean v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mDoCryptVolume:Z

    .line 125
    sput-object v1, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEncryptListener:Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;

    .line 126
    sput-object v1, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCurrDevice:Ljava/lang/String;

    .line 127
    sput v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCurrDeviceCnt:I

    .line 128
    sput-boolean v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->encrypt:Z

    .line 129
    sput-boolean v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mBatchData:Z

    .line 130
    sput-object v1, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastEncryptionText:Ljava/lang/String;

    .line 131
    sput-object v1, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressText:Ljava/lang/String;

    .line 132
    const/4 v0, -0x1

    sput v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressCnt:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/KeyguardScreenCallback;)V
    .locals 9
    .parameter "context"
    .parameter "configuration"
    .parameter "lockPatternUtils"
    .parameter "updateMonitor"
    .parameter "callback"

    .prologue
    .line 143
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 85
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordLockscreenWallpaper:Landroid/widget/RelativeLayout;

    .line 94
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEncryptService:Landroid/os/storage/IEncryptService;

    .line 101
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->passwordChangeNeeded:Z

    .line 103
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    iput-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mHandler:Landroid/os/Handler;

    .line 105
    new-instance v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen$1;

    invoke-direct {v6, p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$1;-><init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)V

    iput-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->r:Ljava/lang/Runnable;

    .line 330
    new-instance v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;

    invoke-direct {v6, p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$2;-><init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)V

    iput-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEncryptHandler:Landroid/os/Handler;

    .line 145
    iget v6, p2, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCreationHardKeyboardHidden:I

    .line 146
    iget v6, p2, Landroid/content/res/Configuration;->orientation:I

    iput v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCreationOrientation:I

    .line 147
    iput-object p4, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    .line 148
    iput-object p5, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    .line 149
    iput-object p3, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    .line 151
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iput-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mConfiguration:Landroid/content/res/Configuration;

    .line 152
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 153
    .local v2, layoutInflater:Landroid/view/LayoutInflater;
    iget v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCreationOrientation:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_2

    .line 154
    const v6, 0x1090037

    const/4 v7, 0x1

    invoke-virtual {v2, v6, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 160
    :goto_0
    const v6, 0x10201ee

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object p2

    .end local p2
    check-cast p2, Landroid/widget/RelativeLayout;

    iput-object p2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordLockscreenWallpaper:Landroid/widget/RelativeLayout;

    .line 161
    new-instance v6, Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;

    invoke-direct {v6, p1}, Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockscreenWallpaperUpdater:Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;

    .line 163
    const-string v6, "ram"

    const-string v7, "encryption.bootmode"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    sput-boolean v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mRamBoot:Z

    .line 164
    sget-boolean v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mRamBoot:Z

    if-eqz v6, :cond_3

    .line 165
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockscreenWallpaperUpdater:Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;->setVisibility(I)V

    .line 166
    const v6, 0x106000c

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->setBackgroundResource(I)V

    .line 171
    :goto_1
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordLockscreenWallpaper:Landroid/widget/RelativeLayout;

    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockscreenWallpaperUpdater:Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 173
    invoke-virtual {p3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    move-result v3

    .line 174
    .local v3, quality:I
    const/high16 v6, 0x4

    if-eq v6, v3, :cond_0

    const/high16 v6, 0x5

    if-ne v6, v3, :cond_4

    :cond_0
    const/4 v6, 0x1

    move v1, v6

    .line 177
    .local v1, isAlpha:Z
    :goto_2
    const v6, 0x1020160

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/android/internal/widget/PasswordEntryKeyboardView;

    iput-object p2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    .line 178
    const v6, 0x10201f0

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/EditText;

    iput-object p2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    .line 179
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->showCursorController(Z)V

    .line 180
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    invoke-virtual {v6, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 181
    const v6, 0x10201d9

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEmergencyCallButton:Landroid/widget/Button;

    .line 182
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v6, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v6, v7}, Lcom/android/internal/widget/LockPatternUtils;->updateEmergencyCallButtonState(Landroid/widget/Button;)V

    .line 184
    const v6, 0x10201ef

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    .line 187
    const v6, 0x10201f4

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ProgressBar;

    sput-object p2, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBar:Landroid/widget/ProgressBar;

    .line 188
    const v6, 0x10201f3

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    sput-object p2, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarText:Landroid/widget/TextView;

    .line 189
    const/4 v6, 0x0

    sput-object v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCryptImage:Landroid/widget/ImageView;

    .line 190
    iget v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCreationOrientation:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_5

    .line 191
    const v6, 0x10201f2

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    sput-object p2, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCryptImage:Landroid/widget/ImageView;

    .line 196
    :goto_3
    const v6, 0x10201f1

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/RelativeLayout;

    sput-object p2, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarLayout:Landroid/widget/RelativeLayout;

    .line 197
    sget-object v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarLayout:Landroid/widget/RelativeLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 198
    const v6, 0x10201f5

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object v6

    sput-object v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mFillView:Landroid/view/View;

    .line 199
    const/4 v6, 0x0

    sput-boolean v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mDoCryptVolume:Z

    .line 203
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v6}, Lcom/android/internal/widget/LockPatternUtils;->checkDevicePasswordExpired()Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->passwordChangeNeeded:Z

    .line 206
    iput-object p1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mContext:Landroid/content/Context;

    .line 209
    invoke-virtual {p4, p0}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->registerInfoCallback(Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$InfoCallback;)V

    .line 213
    new-instance v6, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    invoke-direct {v6, p1, v7, p0}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;-><init>(Landroid/content/Context;Landroid/inputmethodservice/KeyboardView;Landroid/view/View;)V

    iput-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    .line 214
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    if-eqz v1, :cond_6

    const/4 v7, 0x0

    :goto_4
    invoke-virtual {v6, v7}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->setKeyboardMode(I)V

    .line 217
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    iget v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCreationHardKeyboardHidden:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_7

    const/4 v7, 0x4

    :goto_5
    invoke-virtual {v6, v7}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setVisibility(I)V

    .line 219
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    .line 221
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardHelper:Lcom/android/internal/widget/PasswordEntryKeyboardHelper;

    iget-object v7, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternUtils;->isTactileFeedbackEnabled()Z

    move-result v7

    if-eqz v7, :cond_8

    const v7, 0x1070017

    :goto_6
    invoke-virtual {v6, v7}, Lcom/android/internal/widget/PasswordEntryKeyboardHelper;->setVibratePattern(I)V

    .line 225
    sget-boolean v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mRamBoot:Z

    if-eqz v6, :cond_1

    invoke-static {}, Landroid/deviceencryption/DeviceEncryptionManager;->isLockPasswordEnabled()Z

    move-result v6

    if-nez v6, :cond_1

    .line 226
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->setUIForBatchData()V

    .line 227
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->removePasswordUI()V

    .line 229
    :try_start_0
    const-string v6, "onetimeboot"

    const-string v7, "process"

    invoke-static {v6, v7}, Landroid/deviceencryption/DeviceEncryptionManager;->setFileCryptProperty(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const-string v6, "encrypt"

    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Landroid/os/storage/IEncryptService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IEncryptService;

    move-result-object v0

    .line 231
    .local v0, instencsve:Landroid/os/storage/IEncryptService;
    if-eqz v0, :cond_9

    .line 232
    invoke-interface {v0}, Landroid/os/storage/IEncryptService;->processBatchData()V

    .line 233
    new-instance v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;-><init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;Lcom/android/internal/policy/impl/PasswordUnlockScreen$1;)V

    sput-object v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEncryptListener:Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;

    .line 234
    sget-object v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEncryptListener:Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;

    invoke-interface {v0, v6}, Landroid/os/storage/IEncryptService;->registerListener(Landroid/os/storage/IEncryptServiceListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    .end local v0           #instencsve:Landroid/os/storage/IEncryptService;
    :cond_1
    :goto_7
    return-void

    .line 156
    .end local v1           #isAlpha:Z
    .end local v3           #quality:I
    .restart local p2
    :cond_2
    const v6, 0x1090036

    const/4 v7, 0x1

    invoke-virtual {v2, v6, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto/16 :goto_0

    .line 168
    .end local p2
    :cond_3
    iget-object v6, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockscreenWallpaperUpdater:Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;->setVisibility(I)V

    goto/16 :goto_1

    .line 174
    .restart local v3       #quality:I
    :cond_4
    const/4 v6, 0x0

    move v1, v6

    goto/16 :goto_2

    .line 193
    .restart local v1       #isAlpha:Z
    :cond_5
    const v6, 0x10201f2

    invoke-virtual {p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 194
    .local v5, temp:Landroid/widget/ImageView;
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 214
    .end local v5           #temp:Landroid/widget/ImageView;
    :cond_6
    const/4 v7, 0x1

    goto :goto_4

    .line 217
    :cond_7
    const/4 v7, 0x0

    goto :goto_5

    .line 221
    :cond_8
    const/4 v7, 0x0

    goto :goto_6

    .line 237
    .restart local v0       #instencsve:Landroid/os/storage/IEncryptService;
    :cond_9
    :try_start_1
    const-string v6, "PasswordUnlockScreen"

    const-string v7, "IEncryptService instance is null!!"

    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_7

    .line 238
    .end local v0           #instencsve:Landroid/os/storage/IEncryptService;
    :catch_0
    move-exception v6

    move-object v4, v6

    .line 239
    .local v4, rex:Ljava/lang/Exception;
    const-string v6, "PasswordUnlockScreen"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IEncryptService exception  is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000()Landroid/view/View;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mFillView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1100()I
    .locals 1

    .prologue
    .line 72
    sget v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressCnt:I

    return v0
.end method

.method static synthetic access$1102(I)I
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    sput p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressCnt:I

    return p0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCurrDevice:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    sput-object p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCurrDevice:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1300()I
    .locals 1

    .prologue
    .line 72
    sget v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCurrDeviceCnt:I

    return v0
.end method

.method static synthetic access$1308()I
    .locals 2

    .prologue
    .line 72
    sget v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCurrDeviceCnt:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCurrDeviceCnt:I

    return v0
.end method

.method static synthetic access$1400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastEncryptionText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    sput-object p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastEncryptionText:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1600()Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1702(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    sput-object p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressText:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1800()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->runEncryptOrRemount()V

    return-void
.end method

.method static synthetic access$200()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCryptImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Landroid/widget/EditText;
    .locals 1
    .parameter "x0"

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Lcom/android/internal/widget/PasswordEntryKeyboardView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    return-object v0
.end method

.method static synthetic access$2202(Z)Z
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    sput-boolean p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mIsInDeadlineCountDown:Z

    return p0
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->encrypt:Z

    return v0
.end method

.method static synthetic access$402(Z)Z
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    sput-boolean p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->encrypt:Z

    return p0
.end method

.method static synthetic access$500()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mBatchData:Z

    return v0
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    sput-boolean p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mBatchData:Z

    return p0
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->removePasswordUI()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)Lcom/android/internal/policy/impl/KeyguardScreenCallback;
    .locals 1
    .parameter "x0"

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    return-object v0
.end method

.method static synthetic access$800()Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCryptImageSwapper:Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;)Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    sput-object p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCryptImageSwapper:Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    return-object p0
.end method

.method static synthetic access$900()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private handleAttemptLockout(J)V
    .locals 8
    .parameter "elapsedRealtimeDeadline"

    .prologue
    const/4 v1, 0x0

    .line 676
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 677
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setEnabled(Z)V

    .line 678
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 679
    .local v6, elapsedRealtime:J
    new-instance v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$4;

    sub-long v2, p1, v6

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$4;-><init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;JJ)V

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$4;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCountdownTimer:Landroid/os/CountDownTimer;

    .line 698
    return-void
.end method

.method private handleScreenRotation()V
    .locals 2

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->removePasswordUI()V

    .line 286
    new-instance v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    sget-boolean v1, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->encrypt:Z

    invoke-direct {v0, p0, v1}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;-><init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;Z)V

    sput-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCryptImageSwapper:Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    .line 287
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 288
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mFillView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 290
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastEncryptionText:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    sget-object v1, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastEncryptionText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    :cond_0
    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressText:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarText:Landroid/widget/TextView;

    sget-object v1, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    :cond_1
    sget v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressCnt:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBar:Landroid/widget/ProgressBar;

    sget v1, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressCnt:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 293
    :cond_2
    return-void
.end method

.method private removePasswordUI()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 247
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 249
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->setFocusableInTouchMode(Z)V

    .line 250
    return-void
.end method

.method private runEncryptOrRemount()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const-string v7, "PasswordUnlockScreen"

    .line 435
    :try_start_0
    const-string v5, "onetimeboot"

    const-string v6, "process"

    invoke-static {v5, v6}, Landroid/deviceencryption/DeviceEncryptionManager;->setFileCryptProperty(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v5, "encrypt"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Landroid/os/storage/IEncryptService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IEncryptService;

    move-result-object v1

    .line 437
    .local v1, instencsve:Landroid/os/storage/IEncryptService;
    if-eqz v1, :cond_7

    .line 438
    sget-boolean v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mRamBoot:Z

    if-eqz v5, :cond_5

    .line 439
    const-string v5, "PasswordUnlockScreen"

    const-string v6, "Prepare encryption"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    const-string v5, "data"

    invoke-static {v5}, Landroid/deviceencryption/DeviceEncryptionManager;->getCryptBatchStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 442
    .local v2, internalStatus:Ljava/lang/String;
    invoke-static {}, Landroid/deviceencryption/DeviceEncryptionManager;->getInternalStorageStatus()Z

    move-result v5

    sput-boolean v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->encrypt:Z

    .line 443
    const/4 v0, 0x0

    .line 444
    .local v0, current:Z
    const/4 v3, 0x0

    .line 445
    .local v3, progress:Z
    const/4 v5, 0x1

    sput-boolean v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mBatchData:Z

    .line 447
    const-string v5, "NULL"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 448
    const/4 v0, 0x0

    .line 455
    :goto_0
    sget-boolean v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->encrypt:Z

    if-ne v5, v0, :cond_0

    if-eqz v3, :cond_1

    .line 456
    :cond_0
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->removePasswordUI()V

    .line 457
    iget-object v5, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    const v6, 0x124f80

    invoke-interface {v5, v6}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->pokeWakelock(I)V

    .line 458
    new-instance v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    sget-boolean v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->encrypt:Z

    invoke-direct {v5, p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;-><init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;Z)V

    sput-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCryptImageSwapper:Lcom/android/internal/policy/impl/PasswordUnlockScreen$CryptImageSwapper;

    .line 459
    sget-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarLayout:Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 460
    sget-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mFillView:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 462
    sget-boolean v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->encrypt:Z

    if-ne v5, v8, :cond_4

    .line 463
    iget-object v5, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mContext:Landroid/content/Context;

    const v6, 0x10404a9

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastEncryptionText:Ljava/lang/String;

    .line 469
    :goto_1
    iget-object v5, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    sget-object v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastEncryptionText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    const/4 v5, 0x0

    sput v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressCnt:I

    .line 471
    sget-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBar:Landroid/widget/ProgressBar;

    sget v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressCnt:I

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 472
    const-string v5, "0%"

    sput-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressText:Ljava/lang/String;

    .line 473
    sget-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mProgressBarText:Landroid/widget/TextView;

    sget-object v6, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastProgressText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    :cond_1
    invoke-interface {v1}, Landroid/os/storage/IEncryptService;->processBatchData()V

    .line 476
    new-instance v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;-><init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;Lcom/android/internal/policy/impl/PasswordUnlockScreen$1;)V

    sput-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEncryptListener:Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;

    .line 477
    sget-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEncryptListener:Lcom/android/internal/policy/impl/PasswordUnlockScreen$EncryptServiceListener;

    invoke-interface {v1, v5}, Landroid/os/storage/IEncryptService;->registerListener(Landroid/os/storage/IEncryptServiceListener;)V

    .line 494
    .end local v0           #current:Z
    .end local v1           #instencsve:Landroid/os/storage/IEncryptService;
    .end local v2           #internalStatus:Ljava/lang/String;
    .end local v3           #progress:Z
    :goto_2
    return-void

    .line 449
    .restart local v0       #current:Z
    .restart local v1       #instencsve:Landroid/os/storage/IEncryptService;
    .restart local v2       #internalStatus:Ljava/lang/String;
    .restart local v3       #progress:Z
    :cond_2
    const-string v5, "COMPLETE"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 450
    const/4 v0, 0x1

    goto :goto_0

    .line 452
    :cond_3
    const/4 v3, 0x1

    goto :goto_0

    .line 466
    :cond_4
    iget-object v5, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mContext:Landroid/content/Context;

    const v6, 0x10404aa

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLastEncryptionText:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 491
    .end local v0           #current:Z
    .end local v1           #instencsve:Landroid/os/storage/IEncryptService;
    .end local v2           #internalStatus:Ljava/lang/String;
    .end local v3           #progress:Z
    :catch_0
    move-exception v5

    move-object v4, v5

    .line 492
    .local v4, rex:Ljava/lang/Exception;
    const-string v5, "PasswordUnlockScreen"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IEncryptService exception  is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 479
    .end local v4           #rex:Ljava/lang/Exception;
    .restart local v1       #instencsve:Landroid/os/storage/IEncryptService;
    :cond_5
    :try_start_1
    invoke-static {}, Landroid/deviceencryption/DeviceEncryptionManager;->getInternalStorageStatus()Z

    move-result v5

    if-nez v5, :cond_6

    .line 481
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Landroid/os/storage/IEncryptService;->mountVolume(Ljava/lang/String;)I

    goto :goto_2

    .line 485
    :cond_6
    const-string v5, "PasswordUnlockScreen"

    const-string v6, "Already mountVolume is run!!"

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 489
    :cond_7
    const-string v5, "PasswordUnlockScreen"

    const-string v6, "IEncryptService instance is null!!"

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private setUIForBatchData()V
    .locals 7

    .prologue
    const v4, 0x10404af

    const v3, 0x10404a9

    const/4 v2, 0x0

    const-string v6, "dbdata"

    const-string v5, "COMPLETE"

    .line 254
    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setEnabled(Z)V

    .line 255
    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 258
    const-string v1, "policy"

    invoke-static {v1}, Landroid/deviceencryption/DeviceEncryptionManager;->getFileCryptProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, policy:Ljava/lang/String;
    const-string v1, "PasswordUnlockScreen"

    const-string v2, "DEVENC remove callback "

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->r:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 265
    if-eqz v0, :cond_0

    const-string v1, "internal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_1

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    const v2, 0x10404aa

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 281
    :goto_0
    return-void

    .line 267
    :cond_1
    const-string v1, "COMPLETE"

    const-string v1, "data"

    invoke-static {v1}, Landroid/deviceencryption/DeviceEncryptionManager;->getFileCryptProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 269
    const/4 v1, 0x1

    const-string v2, "dbdata"

    invoke-static {v6}, Landroid/deviceencryption/DeviceEncryptionManager;->isTargetToEncrypt(Ljava/lang/String;)Z

    move-result v2

    if-ne v1, v2, :cond_3

    .line 270
    const-string v1, "COMPLETE"

    const-string v1, "dbdata"

    invoke-static {v6}, Landroid/deviceencryption/DeviceEncryptionManager;->getFileCryptProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 271
    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 273
    :cond_2
    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 276
    :cond_3
    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 280
    :cond_4
    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private verifyPasswordAndUnlock()V
    .locals 14

    .prologue
    .line 560
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 563
    .local v2, entry:Ljava/lang/String;
    const-string v10, "true"

    const-string v11, "ro.wtldatapassword"

    invoke-static {v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    sget-boolean v10, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mDoCryptVolume:Z

    if-eqz v10, :cond_0

    .line 564
    const-string v10, "PETER"

    const-string v11, "verifyPasswordAndUnlock called again after crypt"

    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    :goto_0
    return-void

    .line 568
    :cond_0
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v10, v2}, Lcom/android/internal/widget/LockPatternUtils;->checkPassword(Ljava/lang/String;)Z

    move-result v3

    .line 569
    .local v3, isConfirmed:Z
    if-eqz v3, :cond_7

    .line 571
    sget-object v10, Landroid/deviceencryption/DeviceEncryptionManager;->enabled:Ljava/lang/String;

    const-string v11, "true"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 572
    sget-boolean v10, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mRamBoot:Z

    if-eqz v10, :cond_3

    .line 573
    const/4 v10, 0x1

    sput-boolean v10, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mDoCryptVolume:Z

    .line 574
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->setUIForBatchData()V

    .line 615
    :cond_1
    :goto_1
    sget-boolean v10, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mDoCryptVolume:Z

    if-eqz v10, :cond_2

    .line 618
    const-string v10, "onetimeboot"

    const-string v11, "process"

    invoke-static {v10, v11}, Landroid/deviceencryption/DeviceEncryptionManager;->setFileCryptProperty(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mHandler:Landroid/os/Handler;

    new-instance v11, Lcom/android/internal/policy/impl/PasswordUnlockScreen$3;

    invoke-direct {v11, p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen$3;-><init>(Lcom/android/internal/policy/impl/PasswordUnlockScreen;)V

    invoke-virtual {v10, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 671
    :cond_2
    :goto_2
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    const-string v11, ""

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 578
    :cond_3
    iget-boolean v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->passwordChangeNeeded:Z

    if-eqz v10, :cond_4

    .line 579
    const-string v10, "PasswordUnlockScreen"

    const-string v11, "password change needed"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    move-result-object v5

    .line 581
    .local v5, mDPM:Landroid/app/admin/DevicePolicyManager;
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    move-result v9

    .line 582
    .local v9, quality:I
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;)I

    move-result v7

    .line 583
    .local v7, minLength:I
    invoke-virtual {v5, v9}, Landroid/app/admin/DevicePolicyManager;->getPasswordMaximumLength(I)I

    move-result v6

    .line 585
    .local v6, maxLength:I
    new-instance v4, Landroid/content/Intent;

    const-string v10, "android.app.action.CHANGE_DEVICE_PASSWORD"

    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 587
    .local v4, it:Landroid/content/Intent;
    const-string v10, "lockscreen.password_type"

    invoke-virtual {v4, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 588
    const-string v10, "lockscreen.password_min"

    invoke-virtual {v4, v10, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 589
    const-string v10, "lockscreen.password_max"

    invoke-virtual {v4, v10, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 591
    const-string v10, "lockscreen.password_old"

    invoke-virtual {v4, v10, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 592
    const-string v10, "confirm_credentials"

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 593
    const/high16 v10, 0x1000

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 594
    const/high16 v10, 0x40

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 595
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 596
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    const-string v11, ""

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 599
    .end local v4           #it:Landroid/content/Intent;
    .end local v5           #mDPM:Landroid/app/admin/DevicePolicyManager;
    .end local v6           #maxLength:I
    .end local v7           #minLength:I
    .end local v9           #quality:I
    :cond_4
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->keyguardDone(Z)V

    .line 600
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    invoke-interface {v10}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    .line 604
    const-string v8, ""

    .line 606
    .local v8, onetimeboot:Ljava/lang/String;
    const-string v10, "onetimeboot"

    invoke-static {v10}, Landroid/deviceencryption/DeviceEncryptionManager;->getFileCryptProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 607
    const-string v10, "PETER"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onetimeboot :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    const-string v10, "init"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 610
    const/4 v10, 0x1

    sput-boolean v10, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mDoCryptVolume:Z

    .line 611
    const-string v10, "PETER"

    const-string v11, "init > process"

    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 630
    .end local v8           #onetimeboot:Ljava/lang/String;
    :cond_5
    iget-boolean v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->passwordChangeNeeded:Z

    if-eqz v10, :cond_6

    .line 631
    const-string v10, "PasswordUnlockScreen"

    const-string v11, "password change needed"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    move-result-object v5

    .line 634
    .restart local v5       #mDPM:Landroid/app/admin/DevicePolicyManager;
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    move-result v9

    .line 635
    .restart local v9       #quality:I
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;)I

    move-result v7

    .line 636
    .restart local v7       #minLength:I
    invoke-virtual {v5, v9}, Landroid/app/admin/DevicePolicyManager;->getPasswordMaximumLength(I)I

    move-result v6

    .line 638
    .restart local v6       #maxLength:I
    new-instance v4, Landroid/content/Intent;

    const-string v10, "android.app.action.CHANGE_DEVICE_PASSWORD"

    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 640
    .restart local v4       #it:Landroid/content/Intent;
    const-string v10, "lockscreen.password_type"

    invoke-virtual {v4, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 641
    const-string v10, "lockscreen.password_min"

    invoke-virtual {v4, v10, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 642
    const-string v10, "lockscreen.password_max"

    invoke-virtual {v4, v10, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 643
    const-string v10, "confirm_credentials"

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 644
    const/high16 v10, 0x1000

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 645
    const/high16 v10, 0x40

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 646
    const/high16 v10, 0x80

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 647
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 648
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    const-string v11, ""

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 651
    .end local v4           #it:Landroid/content/Intent;
    .end local v5           #mDPM:Landroid/app/admin/DevicePolicyManager;
    .end local v6           #maxLength:I
    .end local v7           #minLength:I
    .end local v9           #quality:I
    :cond_6
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->keyguardDone(Z)V

    .line 652
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    invoke-interface {v10}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    goto/16 :goto_2

    .line 654
    :cond_7
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x3

    if-le v10, v11, :cond_2

    .line 657
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    invoke-interface {v10}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->reportFailedUnlockAttempt()V

    .line 658
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v10}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v10

    rem-int/lit8 v10, v10, 0x5

    if-nez v10, :cond_8

    .line 659
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mHandler:Landroid/os/Handler;

    iget-object v11, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->r:Ljava/lang/Runnable;

    invoke-virtual {v10, v11}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 660
    const/4 v10, 0x1

    sput-boolean v10, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mIsInDeadlineCountDown:Z

    .line 661
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v10}, Lcom/android/internal/widget/LockPatternUtils;->setLockoutAttemptDeadline()J

    move-result-wide v0

    .line 662
    .local v0, deadline:J
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->handleAttemptLockout(J)V

    .line 666
    .end local v0           #deadline:J
    :cond_8
    if-nez v3, :cond_2

    sget-boolean v10, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mIsInDeadlineCountDown:Z

    if-nez v10, :cond_2

    .line 667
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mTitle:Landroid/widget/TextView;

    const v11, 0x104032c

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    .line 668
    iget-object v10, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mHandler:Landroid/os/Handler;

    iget-object v11, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->r:Ljava/lang/Runnable;

    const-wide/16 v12, 0x7d0

    invoke-virtual {v10, v11, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2
.end method


# virtual methods
.method public cleanUp()V
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v0, p0}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    .line 549
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockscreenWallpaperUpdater:Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;

    invoke-virtual {v0}, Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;->cleanUp()V

    .line 550
    return-void
.end method

.method public needsInput()Z
    .locals 1

    .prologue
    .line 517
    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 709
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 710
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 711
    .local v0, config:Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    iget v2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCreationOrientation:I

    if-ne v1, v2, :cond_0

    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iget v2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCreationHardKeyboardHidden:I

    if-eq v1, v2, :cond_1

    .line 713
    :cond_0
    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    invoke-interface {v1, v0}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->recreateMe(Landroid/content/res/Configuration;)V

    .line 715
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 553
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEmergencyCallButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->takeEmergencyCallAction()V

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->pokeWakelock()V

    .line 557
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .parameter "newConfig"

    .prologue
    .line 720
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 721
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCreationOrientation:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iget v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCreationHardKeyboardHidden:I

    if-eq v0, v1, :cond_1

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    invoke-interface {v0, p1}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->recreateMe(Landroid/content/res/Configuration;)V

    .line 725
    :cond_1
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 734
    if-nez p2, :cond_1

    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mIsRecoveryMode:Z

    if-nez v0, :cond_1

    .line 735
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 736
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->verifyPasswordAndUnlock()V

    .line 738
    :cond_0
    const/4 v0, 0x1

    .line 740
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 703
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->pokeWakelock()V

    .line 704
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyboardChange(Z)V
    .locals 2
    .parameter "isKeyboardOpen"

    .prologue
    .line 729
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mKeyboardView:Lcom/android/internal/widget/PasswordEntryKeyboardView;

    if-eqz p1, :cond_0

    const/4 v1, 0x4

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/PasswordEntryKeyboardView;->setVisibility(I)V

    .line 730
    return-void

    .line 729
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 523
    return-void
.end method

.method public onPhoneStateChanged(Ljava/lang/String;)V
    .locals 2
    .parameter "newState"

    .prologue
    .line 744
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->updateEmergencyCallButtonState(Landroid/widget/Button;)V

    .line 745
    return-void
.end method

.method public onRefreshBatteryInfo(ZZI)V
    .locals 0
    .parameter "showBatteryInfo"
    .parameter "pluggedIn"
    .parameter "batteryLevel"

    .prologue
    .line 749
    return-void
.end method

.method public onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0
    .parameter "plmn"
    .parameter "spn"

    .prologue
    .line 753
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 1
    .parameter "direction"
    .parameter "previouslyFocusedRect"

    .prologue
    .line 512
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 527
    iget-object v2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockscreenWallpaperUpdater:Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;

    invoke-virtual {v2}, Lcom/android/internal/policy/impl/LockscreenWallpaperUpdater;->onResume()V

    .line 529
    iget-object v2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 530
    iget-object v2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mPasswordEntry:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 531
    iget-object v2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v3, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->updateEmergencyCallButtonState(Landroid/widget/Button;)V

    .line 534
    iget-object v2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->getLockoutAttemptDeadline()J

    move-result-wide v0

    .line 535
    .local v0, deadline:J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 536
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->handleAttemptLockout(J)V

    .line 540
    :cond_0
    sget-boolean v2, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mBatchData:Z

    if-eqz v2, :cond_1

    .line 541
    iget-object v2, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/KeyguardScreenCallback;

    const v3, 0x124f80

    invoke-interface {v2, v3}, Lcom/android/internal/policy/impl/KeyguardScreenCallback;->pokeWakelock(I)V

    .line 542
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->handleScreenRotation()V

    .line 544
    :cond_1
    return-void
.end method

.method public onRingerModeChanged(I)V
    .locals 0
    .parameter "state"

    .prologue
    .line 757
    return-void
.end method

.method public onTimeChanged()V
    .locals 0

    .prologue
    .line 761
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .parameter "hasWindowFocus"

    .prologue
    .line 766
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    .line 767
    iget-object v0, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/internal/policy/impl/PasswordUnlockScreen;->mEmergencyCallButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->updateEmergencyCallButtonState(Landroid/widget/Button;)V

    .line 768
    return-void
.end method
