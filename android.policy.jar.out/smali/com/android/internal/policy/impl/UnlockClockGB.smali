.class public Lcom/android/internal/policy/impl/UnlockClockGB;
.super Landroid/widget/LinearLayout;
.source "UnlockClockGB.java"

# interfaces
.implements Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SimStateCallback;
.implements Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$InfoCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/UnlockClockGB$1;,
        Lcom/android/internal/policy/impl/UnlockClockGB$Status;
    }
.end annotation


# static fields
.field private static final AMPMselection:I = 0x1

.field private static final DBG:Z = false

.field private static final DISPLAY_CARRIER_NAME:Z = true

.field private static final TAG:Ljava/lang/String; = "UnlockClock"


# instance fields
.field private layoutPosition:I

.field private mAM:Landroid/widget/TextView;

.field private mAM_PM_Check01:Z

.field private mCarrier:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mCreatedInPortrait:Z

.field private mDateFormatString:Ljava/lang/String;

.field private mDate_Month:Landroid/widget/TextView;

.field private mEmergencyCallText:Landroid/widget/TextView;

.field private mHour01:Landroid/widget/ImageView;

.field private mHour02:Landroid/widget/ImageView;

.field private mMin01:Landroid/widget/ImageView;

.field private mMin02:Landroid/widget/ImageView;

.field private mPM:Landroid/widget/TextView;

.field private mRightNow:Ljava/util/Calendar;

.field private mStatus:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

.field private final mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

.field private tz:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcom/android/internal/policy/impl/KeyguardUpdateMonitor;)V
    .locals 6
    .parameter "context"
    .parameter "createdInPortrait"
    .parameter "updateMonitor"

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const-string v5, "UnlockClock"

    const-string v3, "ro.csc.sales_code"

    .line 125
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 70
    iput-object v4, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->tz:Ljava/lang/String;

    .line 74
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->Normal:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mStatus:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    .line 127
    invoke-virtual {p0, v2}, Lcom/android/internal/policy/impl/UnlockClockGB;->setOrientation(I)V

    .line 129
    iput-object p1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mContext:Landroid/content/Context;

    .line 132
    iput-boolean p2, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCreatedInPortrait:Z

    .line 134
    iput-object p3, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    .line 138
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 139
    .local v0, inflater:Landroid/view/LayoutInflater;
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCreatedInPortrait:Z

    if-eqz v1, :cond_0

    .line 140
    const v1, 0x1090078

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 152
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mRightNow:Ljava/util/Calendar;

    .line 153
    iput-object v4, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->tz:Ljava/lang/String;

    .line 155
    const v1, 0x102026d

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mHour01:Landroid/widget/ImageView;

    .line 156
    const v1, 0x102026e

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mHour02:Landroid/widget/ImageView;

    .line 157
    const v1, 0x1020270

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mMin01:Landroid/widget/ImageView;

    .line 158
    const v1, 0x1020271

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mMin02:Landroid/widget/ImageView;

    .line 159
    const v1, 0x1020272

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mAM:Landroid/widget/TextView;

    .line 160
    const v1, 0x1020273

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mPM:Landroid/widget/TextView;

    .line 161
    const v1, 0x1020274

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mDate_Month:Landroid/widget/TextView;

    .line 164
    const v1, 0x102026c

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCarrier:Landroid/widget/TextView;

    .line 165
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCarrier:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 166
    const v1, 0x1020205

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    .line 168
    const-string v1, "TEL"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "TLP"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "OPS"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "XSA"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "VAU"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "TMH"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "PAN"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "EPL"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    :cond_1
    const-string v1, "UnlockClock"

    const-string v1, "PLMN display"

    invoke-static {v5, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :goto_0
    invoke-virtual {p3, p0}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->registerInfoCallback(Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$InfoCallback;)V

    .line 184
    invoke-virtual {p3, p0}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->registerSimStateCallback(Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SimStateCallback;)V

    .line 186
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->resetStatusInfo(Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;)V

    .line 189
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/UnlockClockGB;->refreshTimeAndDateDisplay()V

    .line 190
    return-void

    .line 179
    :cond_2
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    const-string v1, "UnlockClock"

    const-string v1, "NO PLMN display"

    invoke-static {v5, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private changeTimeStringToDrawable(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .parameter "hourString"
    .parameter "minString"

    .prologue
    const/4 v4, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const-string v5, ""

    .line 318
    new-array v0, v4, [I

    .line 319
    .local v0, choiceHourNumber:[I
    new-array v1, v4, [I

    .line 325
    .local v1, choiceMinNumber:[I
    const-string v4, ""

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 326
    .local v2, timePattern1:[Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {p2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 328
    .local v3, timePattern2:[Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/UnlockClockGB;->sortTimeDrawables([Ljava/lang/String;)[I

    move-result-object v0

    .line 329
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/UnlockClockGB;->sortTimeDrawables([Ljava/lang/String;)[I

    move-result-object v1

    .line 331
    iget-object v4, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mHour01:Landroid/widget/ImageView;

    aget v5, v0, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 332
    iget-object v4, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mHour02:Landroid/widget/ImageView;

    aget v5, v0, v7

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 334
    iget-object v4, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mMin01:Landroid/widget/ImageView;

    aget v5, v1, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 335
    iget-object v4, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mMin02:Landroid/widget/ImageView;

    aget v5, v1, v7

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 336
    return-void
.end method

.method private checkTimeAMPM(I)Ljava/lang/String;
    .locals 1
    .parameter "minInt"

    .prologue
    .line 290
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 292
    .local v0, minString:Ljava/lang/String;
    return-object v0
.end method

.method private checkTimeValue(I)Ljava/lang/String;
    .locals 3
    .parameter "minInt"

    .prologue
    .line 278
    const/16 v1, 0xa

    if-ge p1, v1, :cond_0

    .line 279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, minString:Ljava/lang/String;
    :goto_0
    return-object v0

    .line 281
    .end local v0           #minString:Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0       #minString:Ljava/lang/String;
    goto :goto_0
.end method

.method static getCarrierString(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2
    .parameter "telephonyPlmn"
    .parameter "telephonySpn"

    .prologue
    .line 510
    if-eqz p0, :cond_0

    if-nez p1, :cond_0

    move-object v0, p0

    .line 520
    :goto_0
    return-object v0

    .line 512
    :cond_0
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 513
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    move-object v0, p0

    .line 514
    goto :goto_0

    .line 516
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 517
    :cond_2
    if-nez p0, :cond_3

    if-eqz p1, :cond_3

    move-object v0, p1

    .line 518
    goto :goto_0

    .line 520
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method private getCurrentStatus(Lcom/android/internal/telephony/IccCard$State;)Lcom/android/internal/policy/impl/UnlockClockGB$Status;
    .locals 3
    .parameter "simState"

    .prologue
    .line 401
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/internal/telephony/IccCard$State;->ABSENT:Lcom/android/internal/telephony/IccCard$State;

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    move v0, v1

    .line 402
    .local v0, missingAndNotProvisioned:Z
    :goto_0
    if-eqz v0, :cond_1

    .line 403
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->SimMissingLocked:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    .line 425
    :goto_1
    return-object v1

    .line 401
    .end local v0           #missingAndNotProvisioned:Z
    :cond_0
    const/4 v1, 0x0

    move v0, v1

    goto :goto_0

    .line 408
    .restart local v0       #missingAndNotProvisioned:Z
    :cond_1
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$1;->$SwitchMap$com$android$internal$telephony$IccCard$State:[I

    invoke-virtual {p1}, Lcom/android/internal/telephony/IccCard$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 425
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->SimMissing:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    goto :goto_1

    .line 410
    :pswitch_0
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->SimMissing:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    goto :goto_1

    .line 412
    :pswitch_1
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->SimMissing:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    goto :goto_1

    .line 414
    :pswitch_2
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->SimLocked:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    goto :goto_1

    .line 416
    :pswitch_3
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->NetworkLocked:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    goto :goto_1

    .line 418
    :pswitch_4
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->SimPukLocked:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    goto :goto_1

    .line 420
    :pswitch_5
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->Normal:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    goto :goto_1

    .line 422
    :pswitch_6
    sget-object v1, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->SimMissing:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    goto :goto_1

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private setAMPM()I
    .locals 5

    .prologue
    .line 264
    iget-object v3, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mRightNow:Ljava/util/Calendar;

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 265
    .local v0, am_pmInt01:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x107000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 267
    .local v2, strAMPM:[Ljava/lang/String;
    add-int/lit8 v3, v0, 0x1

    aget-object v1, v2, v3

    .line 270
    .local v1, am_pmString01:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mPM:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    return v0
.end method

.method private setAMPMMode()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 243
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mAM_PM_Check01:Z

    if-ne v1, v4, :cond_1

    .line 244
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mAM:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 245
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mPM:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    invoke-direct {p0}, Lcom/android/internal/policy/impl/UnlockClockGB;->setAMPM()I

    .line 249
    const/4 v0, 0x1

    .line 251
    .local v0, am_pm:I
    if-nez v0, :cond_2

    .line 252
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mAM:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mPM:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 255
    :cond_2
    if-ne v0, v4, :cond_0

    .line 256
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mAM:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 257
    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mPM:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private sortTimeDrawables([Ljava/lang/String;)[I
    .locals 7
    .parameter "timePattern"

    .prologue
    .line 347
    const/16 v6, 0xa

    new-array v5, v6, [I

    fill-array-data v5, :array_0

    .line 356
    .local v5, unlock_clock_drawables:[I
    const/4 v6, 0x4

    new-array v0, v6, [I

    .line 358
    .local v0, choiceNumber:[I
    array-length v4, p1

    .line 360
    .local v4, k:I
    const/4 v2, 0x1

    .local v2, i:I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 362
    :try_start_0
    aget-object v6, p1, v2

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 366
    .local v3, j:I
    :goto_1
    aget v6, v5, v3

    aput v6, v0, v2

    .line 360
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 363
    .end local v3           #j:I
    :catch_0
    move-exception v6

    move-object v1, v6

    .line 364
    .local v1, e:Ljava/lang/NumberFormatException;
    const/4 v3, 0x0

    .restart local v3       #j:I
    goto :goto_1

    .line 369
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .end local v3           #j:I
    :cond_0
    return-object v0

    .line 347
    :array_0
    .array-data 0x4
        0x36t 0x4t 0x8t 0x1t
        0x37t 0x4t 0x8t 0x1t
        0x39t 0x4t 0x8t 0x1t
        0x3at 0x4t 0x8t 0x1t
        0x3bt 0x4t 0x8t 0x1t
        0x3ct 0x4t 0x8t 0x1t
        0x3dt 0x4t 0x8t 0x1t
        0x3et 0x4t 0x8t 0x1t
        0x3ft 0x4t 0x8t 0x1t
        0x40t 0x4t 0x8t 0x1t
    .end array-data
.end method

.method private transformHourData(I)Ljava/lang/String;
    .locals 2
    .parameter "hourInt"

    .prologue
    .line 300
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mAM_PM_Check01:Z

    if-nez v1, :cond_2

    .line 301
    const/16 v1, 0xc

    if-le p1, v1, :cond_1

    add-int/lit8 p1, p1, -0xc

    .line 303
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/UnlockClockGB;->checkTimeValue(I)Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, hourString:Ljava/lang/String;
    :goto_1
    return-object v0

    .line 302
    .end local v0           #hourString:Ljava/lang/String;
    :cond_1
    if-nez p1, :cond_0

    const/16 p1, 0xc

    goto :goto_0

    .line 308
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/UnlockClockGB;->checkTimeValue(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0       #hourString:Ljava/lang/String;
    goto :goto_1
.end method

.method private updateLayout(Lcom/android/internal/policy/impl/UnlockClockGB$Status;)V
    .locals 6
    .parameter "status"

    .prologue
    const v5, 0x1040332

    const/16 v4, 0x8

    const-string v3, "UnlockClock"

    const-string v2, "ro.csc.sales_code"

    .line 437
    const-string v0, "TEL"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "OPS"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "XSA"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "VAU"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "TMH"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PAN"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EPL"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 444
    :cond_0
    const-string v0, "UnlockClock"

    const-string v0, "PLMN display"

    invoke-static {v3, v0}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    sget-object v0, Lcom/android/internal/policy/impl/UnlockClockGB$1;->$SwitchMap$com$android$internal$policy$impl$UnlockClockGB$Status:[I

    invoke-virtual {p1}, Lcom/android/internal/policy/impl/UnlockClockGB$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 507
    :goto_0
    return-void

    .line 447
    :cond_1
    const-string v0, "UnlockClock"

    const-string v0, "No PLMN display"

    invoke-static {v3, v0}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 454
    :pswitch_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCarrier:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getTelephonyPlmn()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getTelephonySpn()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/UnlockClockGB;->getCarrierString(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 458
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 464
    :pswitch_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCarrier:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/internal/policy/impl/UnlockClockGB;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1040336

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getTelephonyPlmn()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/UnlockClockGB;->getCarrierString(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 468
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 472
    :pswitch_2
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCarrier:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 473
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getTelephonyPlmn()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getTelephonySpn()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/UnlockClockGB;->getCarrierString(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_2

    .line 478
    const-string v0, "UnlockClock"

    const-string v0, "mEmergencyCallText is null"

    invoke-static {v3, v0}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :goto_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 480
    :cond_2
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCarrier:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 487
    :pswitch_3
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCarrier:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getTelephonyPlmn()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/internal/policy/impl/UnlockClockGB;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/UnlockClockGB;->getCarrierString(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 494
    :pswitch_4
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCarrier:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getTelephonyPlmn()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/internal/policy/impl/UnlockClockGB;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x1040339

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/UnlockClockGB;->getCarrierString(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 497
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 501
    :pswitch_5
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mCarrier:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getTelephonyPlmn()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/internal/policy/impl/UnlockClockGB;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x1040337

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/internal/policy/impl/UnlockClockGB;->getCarrierString(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 504
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mEmergencyCallText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public cleanUp()V
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->tz:Ljava/lang/String;

    .line 342
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mUpdateMonitor:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-virtual {v0, p0}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    .line 343
    return-void
.end method

.method public onPhoneStateChanged(Ljava/lang/String;)V
    .locals 0
    .parameter "newState"

    .prologue
    .line 393
    return-void
.end method

.method public onRefreshBatteryInfo(ZZI)V
    .locals 0
    .parameter "showBatteryInfo"
    .parameter "pluggedIn"
    .parameter "batteryLevel"

    .prologue
    .line 381
    return-void
.end method

.method public onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1
    .parameter "plmn"
    .parameter "spn"

    .prologue
    .line 376
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mStatus:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/UnlockClockGB;->updateLayout(Lcom/android/internal/policy/impl/UnlockClockGB$Status;)V

    .line 377
    return-void
.end method

.method public onRingerModeChanged(I)V
    .locals 0
    .parameter "state"

    .prologue
    .line 389
    return-void
.end method

.method public onSimStateChanged(Lcom/android/internal/telephony/IccCard$State;)V
    .locals 3
    .parameter "simState"

    .prologue
    .line 525
    const-string v0, "UnlockClock"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSimStateChanged(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/UnlockClockGB;->getCurrentStatus(Lcom/android/internal/telephony/IccCard$State;)Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mStatus:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    .line 527
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mStatus:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/UnlockClockGB;->updateLayout(Lcom/android/internal/policy/impl/UnlockClockGB$Status;)V

    .line 528
    return-void
.end method

.method public onTimeChanged()V
    .locals 0

    .prologue
    .line 385
    return-void
.end method

.method protected refreshTimeAndDateDisplay()V
    .locals 8

    .prologue
    .line 199
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mRightNow:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 207
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->tz:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 208
    const-string v5, "persist.sys.timezone"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, current:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mRightNow:Ljava/util/Calendar;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 215
    .end local v0           #current:Ljava/lang/String;
    :cond_0
    :goto_0
    const-string v5, "UnlockClock"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GMT_update mRightNow.getTimeZone().getID() == "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mRightNow:Ljava/util/Calendar;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mRightNow:Ljava/util/Calendar;

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 223
    .local v1, hourInt01:I
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mRightNow:Ljava/util/Calendar;

    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 225
    .local v3, minInt01:I
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/UnlockClockGB;->transformHourData(I)Ljava/lang/String;

    move-result-object v2

    .line 226
    .local v2, hourString01:Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/android/internal/policy/impl/UnlockClockGB;->checkTimeValue(I)Ljava/lang/String;

    move-result-object v4

    .line 228
    .local v4, minString01:Ljava/lang/String;
    invoke-direct {p0, v2, v4}, Lcom/android/internal/policy/impl/UnlockClockGB;->changeTimeStringToDrawable(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mAM_PM_Check01:Z

    .line 231
    invoke-direct {p0}, Lcom/android/internal/policy/impl/UnlockClockGB;->setAMPMMode()V

    .line 237
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mContext:Landroid/content/Context;

    const v6, 0x10400ab

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mDateFormatString:Ljava/lang/String;

    .line 238
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mDate_Month:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mDateFormatString:Ljava/lang/String;

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-static {v6, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    return-void

    .line 211
    .end local v1           #hourInt01:I
    .end local v2           #hourString01:Ljava/lang/String;
    .end local v3           #minInt01:I
    .end local v4           #minString01:Ljava/lang/String;
    :cond_1
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mRightNow:Ljava/util/Calendar;

    if-eqz v5, :cond_0

    .line 212
    iget-object v5, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mRightNow:Ljava/util/Calendar;

    iget-object v6, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->tz:Ljava/lang/String;

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    goto :goto_0
.end method

.method public resetClockInfo(Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;)V
    .locals 1
    .parameter "updateMonitor"

    .prologue
    .line 193
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getChangedTimeZone()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->tz:Ljava/lang/String;

    .line 194
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/UnlockClockGB;->refreshTimeAndDateDisplay()V

    .line 195
    return-void
.end method

.method public resetStatusInfo(Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;)V
    .locals 1
    .parameter "updateMonitor"

    .prologue
    .line 534
    invoke-virtual {p1}, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCard$State;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/UnlockClockGB;->getCurrentStatus(Lcom/android/internal/telephony/IccCard$State;)Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mStatus:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    .line 535
    iget-object v0, p0, Lcom/android/internal/policy/impl/UnlockClockGB;->mStatus:Lcom/android/internal/policy/impl/UnlockClockGB$Status;

    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/UnlockClockGB;->updateLayout(Lcom/android/internal/policy/impl/UnlockClockGB$Status;)V

    .line 536
    return-void
.end method
