.class Lcom/android/server/EncryptService$1;
.super Landroid/content/BroadcastReceiver;
.source "EncryptService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/EncryptService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/EncryptService;


# direct methods
.method constructor <init>(Lcom/android/server/EncryptService;)V
    .locals 0
    .parameter

    .prologue
    .line 113
    iput-object p1, p0, Lcom/android/server/EncryptService$1;->this$0:Lcom/android/server/EncryptService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private disableADB(Landroid/content/Context;)V
    .locals 8
    .parameter "context"

    .prologue
    const-string v7, "persist.sys.disableadb"

    const-string v6, "persist.service.adb.enable"

    const-string v5, "0"

    .line 235
    const-string v2, "persist.service.adb.enable"

    const-string v2, "0"

    invoke-static {v6, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, prevADB:Ljava/lang/String;
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 238
    const-string v2, "persist.sys.disableadb"

    const-string v2, "0"

    invoke-static {v7, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, disableadb:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/EncryptService;->access$000()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "DisableADB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disableADB ADB : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", disableadb : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_0
    const-string v2, "0"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 243
    const-string v2, "persist.sys.disableadb"

    const-string v2, "2"

    invoke-static {v7, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v2, "persist.service.adb.enable"

    const-string v2, "0"

    invoke-static {v6, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    .end local v0           #disableadb:Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private enableADB(Landroid/content/Context;)V
    .locals 10
    .parameter "context"

    .prologue
    const/4 v6, 0x1

    const-string v9, "persist.service.adb.enable"

    const-string v8, "adb_enabled"

    const-string v7, "1"

    const-string v5, "0"

    .line 214
    const-string v3, "persist.service.adb.enable"

    const-string v3, "0"

    invoke-static {v9, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 216
    .local v2, prevADB:Ljava/lang/String;
    const-string v3, "0"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 217
    const-string v3, "persist.sys.disableadb"

    const-string v4, "0"

    invoke-static {v3, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 219
    .local v1, disableadb:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/EncryptService;->access$000()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "DisableADB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "enableADB ADB : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", disableadb : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_0
    const-string v3, "2"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v6, :cond_1

    .line 221
    const-string v3, "persist.sys.disableadb"

    const-string v4, "1"

    invoke-static {v3, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 225
    .local v0, cr:Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    const-string v3, "adb_enabled"

    const/4 v3, 0x0

    invoke-static {v0, v8, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_2

    .line 226
    const-string v3, "adb_enabled"

    invoke-static {v0, v8, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 232
    .end local v0           #cr:Landroid/content/ContentResolver;
    .end local v1           #disableadb:Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 228
    .restart local v0       #cr:Landroid/content/ContentResolver;
    .restart local v1       #disableadb:Ljava/lang/String;
    :cond_2
    const-string v3, "persist.service.adb.enable"

    const-string v3, "1"

    invoke-static {v9, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 21
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 116
    invoke-static {}, Lcom/android/server/EncryptService;->access$000()Z

    move-result v18

    if-eqz v18, :cond_0

    const-string v18, "DisableADB"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "sendBroadcast : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    const-string v19, "android.intent.action.SCREEN_OFF"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 120
    const-string v18, "storage"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/os/storage/StorageManager;

    .line 122
    .local v15, sm:Landroid/os/storage/StorageManager;
    if-eqz v15, :cond_2

    invoke-virtual {v15}, Landroid/os/storage/StorageManager;->isUsbMassStorageEnabled()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 123
    invoke-static {}, Lcom/android/server/EncryptService;->access$000()Z

    move-result v18

    if-eqz v18, :cond_1

    const-string v18, "DisableADB"

    const-string v19, "ignore USB storage enabled"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    .end local v15           #sm:Landroid/os/storage/StorageManager;
    :cond_1
    :goto_0
    return-void

    .line 128
    .restart local v15       #sm:Landroid/os/storage/StorageManager;
    :cond_2
    const/4 v13, 0x0

    .line 131
    .local v13, raf:Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v14, Ljava/io/RandomAccessFile;

    const-string v18, "/sys/devices/platform/android_usb/tethering"

    const-string v19, "r"

    move-object v0, v14

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 132
    .end local v13           #raf:Ljava/io/RandomAccessFile;
    .local v14, raf:Ljava/io/RandomAccessFile;
    const/16 v18, 0x2

    :try_start_1
    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v16, v0

    .line 133
    .local v16, stored:[B
    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object v0, v14

    move-object/from16 v1, v16

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v8

    .line 134
    .local v8, got:I
    const/16 v18, 0x1

    const/16 v19, 0x0

    aput-byte v19, v16, v18

    .line 136
    if-lez v8, :cond_4

    .line 137
    new-instance v17, Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 138
    .local v17, v:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/EncryptService;->access$000()Z

    move-result v18

    if-eqz v18, :cond_3

    const-string v18, "DisableADB"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/sys/devices/platform/android_usb/tethering "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_3
    const-string v18, "1"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    move-result v18

    if-eqz v18, :cond_4

    .line 146
    if-eqz v14, :cond_1

    .line 147
    :try_start_2
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v9

    .line 150
    .local v9, ioe:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 146
    .end local v9           #ioe:Ljava/io/IOException;
    .end local v17           #v:Ljava/lang/String;
    :cond_4
    if-eqz v14, :cond_5

    .line 147
    :try_start_3
    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_5
    move-object v13, v14

    .line 154
    .end local v8           #got:I
    .end local v14           #raf:Ljava/io/RandomAccessFile;
    .end local v16           #stored:[B
    .restart local v13       #raf:Ljava/io/RandomAccessFile;
    :cond_6
    :goto_1
    invoke-direct/range {p0 .. p1}, Lcom/android/server/EncryptService$1;->disableADB(Landroid/content/Context;)V

    goto :goto_0

    .line 149
    .end local v13           #raf:Ljava/io/RandomAccessFile;
    .restart local v8       #got:I
    .restart local v14       #raf:Ljava/io/RandomAccessFile;
    .restart local v16       #stored:[B
    :catch_1
    move-exception v9

    .line 150
    .restart local v9       #ioe:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    move-object v13, v14

    .line 152
    .end local v14           #raf:Ljava/io/RandomAccessFile;
    .restart local v13       #raf:Ljava/io/RandomAccessFile;
    goto :goto_1

    .line 141
    .end local v8           #got:I
    .end local v9           #ioe:Ljava/io/IOException;
    .end local v16           #stored:[B
    :catch_2
    move-exception v18

    move-object/from16 v7, v18

    .line 142
    .local v7, ex:Ljava/lang/Exception;
    :goto_2
    :try_start_4
    const-string v18, "DisableADB"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Exception during tetherging status"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object v1, v7

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 146
    if-eqz v13, :cond_6

    .line 147
    :try_start_5
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 149
    :catch_3
    move-exception v9

    .line 150
    .restart local v9       #ioe:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 145
    .end local v7           #ex:Ljava/lang/Exception;
    .end local v9           #ioe:Ljava/io/IOException;
    :catchall_0
    move-exception v18

    .line 146
    :goto_3
    if-eqz v13, :cond_7

    .line 147
    :try_start_6
    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 151
    :cond_7
    :goto_4
    throw v18

    .line 149
    :catch_4
    move-exception v9

    .line 150
    .restart local v9       #ioe:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 155
    .end local v9           #ioe:Ljava/io/IOException;
    .end local v13           #raf:Ljava/io/RandomAccessFile;
    .end local v15           #sm:Landroid/os/storage/StorageManager;
    :cond_8
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    const-string v19, "android.intent.action.USER_PRESENT"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 156
    invoke-direct/range {p0 .. p1}, Lcom/android/server/EncryptService$1;->enableADB(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 157
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    const-string v19, "android.intent.action.SCREEN_ON"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 158
    const-string v18, "keyguard"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/KeyguardManager;

    .line 160
    .local v11, km:Landroid/app/KeyguardManager;
    invoke-static {}, Lcom/android/server/EncryptService;->access$000()Z

    move-result v18

    if-eqz v18, :cond_a

    if-eqz v11, :cond_a

    const-string v18, "DisableADB"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "inKeyguardRestrictedInputMode() : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v11}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_a
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v18

    if-nez v18, :cond_1

    .line 163
    invoke-direct/range {p0 .. p1}, Lcom/android/server/EncryptService$1;->enableADB(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 165
    .end local v11           #km:Landroid/app/KeyguardManager;
    :cond_b
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    const-string v19, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_11

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/EncryptService$1;->this$0:Lcom/android/server/EncryptService;

    move-object/from16 v18, v0

    #getter for: Lcom/android/server/EncryptService;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/android/server/EncryptService;->access$100(Lcom/android/server/EncryptService;)Landroid/content/Context;

    move-result-object v18

    const-string v19, "keyguard"

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/KeyguardManager;

    .line 167
    .restart local v11       #km:Landroid/app/KeyguardManager;
    const/4 v10, 0x0

    .line 168
    .local v10, keyguard:Z
    const-string v18, "persist.sys.disableadb"

    const-string v19, "0"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 169
    .local v5, disableadb:Ljava/lang/String;
    const-string v18, "persist.service.adb.enable"

    const-string v19, "0"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 171
    .local v12, prevADB:Ljava/lang/String;
    if-eqz v11, :cond_c

    invoke-virtual {v11}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v10

    .line 173
    :cond_c
    invoke-static {}, Lcom/android/server/EncryptService;->access$000()Z

    move-result v18

    if-eqz v18, :cond_d

    const-string v18, "DisableADB"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ACTION_BOOT_COMPLETED ADB : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object v1, v12

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", disableadb : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    :cond_d
    invoke-static {}, Lcom/android/server/EncryptService;->access$000()Z

    move-result v18

    if-eqz v18, :cond_e

    const-string v18, "DisableADB"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "inKeyguardRestrictedInputMode() : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move v1, v10

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_e
    const-string v18, "2"

    move-object/from16 v0, v18

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_10

    .line 178
    const/16 v18, 0x1

    move v0, v10

    move/from16 v1, v18

    if-ne v0, v1, :cond_f

    .line 179
    const-string v18, "persist.sys.disableadb"

    const-string v19, "2"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v18, "persist.service.adb.enable"

    const-string v19, "0"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    :cond_f
    const-string v18, "persist.sys.disableadb"

    const-string v19, "1"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 186
    .local v4, cr:Landroid/content/ContentResolver;
    if-eqz v4, :cond_1

    const-string v18, "adb_enabled"

    const/16 v19, 0x0

    move-object v0, v4

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v18

    if-nez v18, :cond_1

    .line 187
    const-string v18, "adb_enabled"

    const/16 v19, 0x1

    move-object v0, v4

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 190
    .end local v4           #cr:Landroid/content/ContentResolver;
    :cond_10
    const-string v18, "1"

    move-object/from16 v0, v18

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    const/16 v18, 0x1

    move v0, v10

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 191
    const-string v18, "persist.sys.disableadb"

    const-string v19, "2"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v18, "persist.service.adb.enable"

    const-string v19, "0"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 194
    .end local v5           #disableadb:Ljava/lang/String;
    .end local v10           #keyguard:Z
    .end local v11           #km:Landroid/app/KeyguardManager;
    .end local v12           #prevADB:Ljava/lang/String;
    :cond_11
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    const-string v19, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 195
    const-string v18, "device_policy"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/admin/DevicePolicyManager;

    .line 197
    .local v6, dpm:Landroid/app/admin/DevicePolicyManager;
    if-eqz v6, :cond_1

    const/16 v18, 0x0

    move-object v0, v6

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getRequireDeviceEncryption(Landroid/content/ComponentName;)Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_12

    const/16 v18, 0x0

    move-object v0, v6

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getRequireStorageCardEncryption(Landroid/content/ComponentName;)Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 200
    :cond_12
    const-string v18, "persist.sys.disableadb"

    const-string v19, "0"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 202
    .restart local v5       #disableadb:Ljava/lang/String;
    const-string v18, "0"

    move-object/from16 v0, v18

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 203
    const-string v18, "persist.sys.disableadb"

    const-string v19, "1"

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v18, "keyguard"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/KeyguardManager;

    .line 205
    .restart local v11       #km:Landroid/app/KeyguardManager;
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 206
    invoke-direct/range {p0 .. p1}, Lcom/android/server/EncryptService$1;->disableADB(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 145
    .end local v5           #disableadb:Ljava/lang/String;
    .end local v6           #dpm:Landroid/app/admin/DevicePolicyManager;
    .end local v11           #km:Landroid/app/KeyguardManager;
    .restart local v14       #raf:Ljava/io/RandomAccessFile;
    .restart local v15       #sm:Landroid/os/storage/StorageManager;
    :catchall_1
    move-exception v18

    move-object v13, v14

    .end local v14           #raf:Ljava/io/RandomAccessFile;
    .restart local v13       #raf:Ljava/io/RandomAccessFile;
    goto/16 :goto_3

    .line 141
    .end local v13           #raf:Ljava/io/RandomAccessFile;
    .restart local v14       #raf:Ljava/io/RandomAccessFile;
    :catch_5
    move-exception v18

    move-object/from16 v7, v18

    move-object v13, v14

    .end local v14           #raf:Ljava/io/RandomAccessFile;
    .restart local v13       #raf:Ljava/io/RandomAccessFile;
    goto/16 :goto_2
.end method
