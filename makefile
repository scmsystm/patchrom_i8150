#
# Makefile for i8150
#

# The original zip file, MUST be specified by each product
local-zip-file     := I8150_DXLC1.zip

# The output zip file of MIUI rom, the default is porting_miui.zip if not specified
local-out-zip-file := update-MIUI-I8150DXLC1-`date +'%Y%m%d'`.zip

# All apps from original ZIP, but has smali files chanded
local-modified-apps := LogsProvider Phone MediaProvider TelephonyProvider Settings 

local-miui-modified-apps := Launcher2 Mms Contacts ThemeManager

# All apks from MIUI execept MIUISystemUI and framework-miui-res.apk
local-miui-apps     := ContactsProvider DownloadProvider TelocationProvider Notes Music DownloadProviderUi Updater

# All apps need to be removed from original ZIP file
local-remove-apps   := BuddiesNow Memo MiniDiary MinimalHome SocialHub TouchWiz30Launcher \
    Days DigitalClock Dlna DualClock PostIt Protips QuickSearchBox SamsungApps SamsungAppsUNA3 \
    SamsungWidget_ProgramMonitor SamsungWidget_News SamsungWidget_StockClock SamsungWidget_WeatherClock SecretWallpaper1 SecretWallpaper2 \
    SnsAccountTw SnsAccountFb SnsAccountLi SnsDisclaimer SnsImageCache SnsProvider \
    Tasks TasksProvider Term TrimApp TwCalendarAppWidget GameHub MusicPlayer NetworkLocation

# To include the local targets before and after zip the final ZIP file, 
# and the local-targets should:
# (1) be defined after including porting.mk if using any global variable(see porting.mk)
# (2) the name should be leaded with local- to prevent any conflict with global targets
local-pre-zip := local-zip-misc

# The local targets after the zip file is generated, could include 'zip2sd' to 
# deliver the zip file to phone, or to customize other actions

include $(PORT_BUILD)/porting.mk

# To define any local-target
local-zip-misc:
	@echo Add Arabic support-files
	cp prebuilt/arabic_support/fonts/* $(ZIP_DIR)/system/fonts/
	cp prebuilt/arabic_support/lib/* $(ZIP_DIR)/system/lib/
	@echo Replacing boot.img
	cp prebuilt/boot.img $(ZIP_DIR)/boot.img
	@echo add MIUI Sound
	cp prebuilt/audio/alarms/* $(ZIP_DIR)/system/media/audio/alarms/
	cp prebuilt/audio/notifications/* $(ZIP_DIR)/system/media/audio/notifications/
	cp prebuilt/audio/ringtones/* $(ZIP_DIR)/system/media/audio/ringtones/
	cp prebuilt/audio/ui/* $(ZIP_DIR)/system/media/audio/ui/
	@echo add GoogleApps
	cp prebuilt/com.google.android.maps.jar $(ZIP_DIR)/
	cp -R prebuilt/gapps/ $(ZIP_DIR)/gapps
	mv $(ZIP_DIR)/gapps/GoogleBackupTransport.apk $(ZIP_DIR)/system/app/GoogleBackupTransport.apk
	cp -R prebuilt/apps/ $(ZIP_DIR)/
	@echo Add custom boot animation support
	cp prebuilt/samsungani $(ZIP_DIR)/system/bin/
	cp prebuilt/bootanimation $(ZIP_DIR)/system/bin/
	cp prebuilt/bootanimation.zip $(ZIP_DIR)/system/media/
	@echo Add Telocation Database
	cp prebuilt/telocation.db $(ZIP_DIR)/system/etc/telocation.db
	@echo Replacing build.prop
	cp prebuilt/build.prop $(ZIP_DIR)/system/build.prop
	@echo Replacing 1_Power_on.ogg
	cp prebuilt/PowerOn.wav $(ZIP_DIR)/system/etc/1_Power_on.ogg
	@echo Replacing PowerOn.wav
	cp prebuilt/PowerOn.wav $(ZIP_DIR)/system/etc/PowerOn.wav
	@echo Replacing updater script
	cp prebuilt/updater-script $(ZIP_DIR)/META-INF/com/google/android/updater-script
	@echo Replacing mod build date
	find $(ZIP_DIR)/system/build.prop -type f | xargs perl -pi -e "s/tmp_dt/`date`/g;"
