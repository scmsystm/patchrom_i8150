.class Lcom/android/phone/CallFeaturesSetting$7;
.super Landroid/content/BroadcastReceiver;
.source "CallFeaturesSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/CallFeaturesSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CallFeaturesSetting;


# direct methods
.method constructor <init>(Lcom/android/phone/CallFeaturesSetting;)V
    .locals 0
    .parameter

    .prologue
    .line 2334
    iput-object p1, p0, Lcom/android/phone/CallFeaturesSetting$7;->this$0:Lcom/android/phone/CallFeaturesSetting;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .parameter "context"
    .parameter "intent"

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    const-string v12, "state"

    .line 2337
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2339
    .local v0, action:Ljava/lang/String;
    const-string v9, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2340
    const-string v9, "state"

    invoke-virtual {p2, v12, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 2342
    .local v8, state:Ljava/lang/Boolean;
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2343
    iget-object v9, p0, Lcom/android/phone/CallFeaturesSetting$7;->this$0:Lcom/android/phone/CallFeaturesSetting;

    const-string v10, "sip_settings_category_key"

    invoke-virtual {v9, v10}, Lcom/android/phone/CallFeaturesSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .line 2344
    .local v7, sipSettings:Landroid/preference/Preference;
    iget-object v9, p0, Lcom/android/phone/CallFeaturesSetting$7;->this$0:Lcom/android/phone/CallFeaturesSetting;

    invoke-virtual {v9}, Lcom/android/phone/CallFeaturesSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    .line 2345
    .local v6, screen:Landroid/preference/PreferenceScreen;
    invoke-virtual {v6}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    .line 2346
    .local v1, count:I
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    if-ge v3, v1, :cond_2

    .line 2347
    invoke-virtual {v6, v3}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v5

    .line 2348
    .local v5, pref:Landroid/preference/Preference;
    if-eq v5, v7, :cond_0

    .line 2349
    invoke-virtual {v5, v11}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 2346
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2352
    .end local v1           #count:I
    .end local v3           #i:I
    .end local v5           #pref:Landroid/preference/Preference;
    .end local v6           #screen:Landroid/preference/PreferenceScreen;
    .end local v7           #sipSettings:Landroid/preference/Preference;
    :cond_1
    iget-object v9, p0, Lcom/android/phone/CallFeaturesSetting$7;->this$0:Lcom/android/phone/CallFeaturesSetting;

    invoke-virtual {v9}, Lcom/android/phone/CallFeaturesSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v9

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 2366
    .end local v8           #state:Ljava/lang/Boolean;
    :cond_2
    :goto_1
    return-void

    .line 2354
    :cond_3
    const-string v9, "com.android.phone.HEADSET_PLUG_ACTION"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2355
    const-string v9, "state"

    invoke-virtual {p2, v12, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2356
    .local v2, headset_state:I
    const-string v9, "CallFeaturesSetting"

    const-string v10, "HEADSET_PLUG_ACTION: headset_state - "

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2357
    if-nez v2, :cond_2

    iget-object v9, p0, Lcom/android/phone/CallFeaturesSetting$7;->this$0:Lcom/android/phone/CallFeaturesSetting;

    #getter for: Lcom/android/phone/CallFeaturesSetting;->mButtonTTY:Landroid/preference/ListPreference;
    invoke-static {v9}, Lcom/android/phone/CallFeaturesSetting;->access$800(Lcom/android/phone/CallFeaturesSetting;)Landroid/preference/ListPreference;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 2358
    const/4 v4, 0x0

    .line 2359
    .local v4, mode:I
    iget-object v9, p0, Lcom/android/phone/CallFeaturesSetting$7;->this$0:Lcom/android/phone/CallFeaturesSetting;

    #calls: Lcom/android/phone/CallFeaturesSetting;->handleTTYChange(I)V
    invoke-static {v9, v4}, Lcom/android/phone/CallFeaturesSetting;->access$900(Lcom/android/phone/CallFeaturesSetting;I)V

    goto :goto_1

    .line 2361
    .end local v2           #headset_state:I
    .end local v4           #mode:I
    :cond_4
    const-string v9, "com.android.phone.END_CALL_ACTION"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2362
    iget-object v9, p0, Lcom/android/phone/CallFeaturesSetting$7;->this$0:Lcom/android/phone/CallFeaturesSetting;

    #getter for: Lcom/android/phone/CallFeaturesSetting;->mButtonTTY:Landroid/preference/ListPreference;
    invoke-static {v9}, Lcom/android/phone/CallFeaturesSetting;->access$800(Lcom/android/phone/CallFeaturesSetting;)Landroid/preference/ListPreference;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 2363
    iget-object v9, p0, Lcom/android/phone/CallFeaturesSetting$7;->this$0:Lcom/android/phone/CallFeaturesSetting;

    #getter for: Lcom/android/phone/CallFeaturesSetting;->mButtonTTY:Landroid/preference/ListPreference;
    invoke-static {v9}, Lcom/android/phone/CallFeaturesSetting;->access$800(Lcom/android/phone/CallFeaturesSetting;)Landroid/preference/ListPreference;

    move-result-object v9

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_1
.end method
