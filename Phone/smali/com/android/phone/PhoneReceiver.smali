.class public Lcom/android/phone/PhoneReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhoneReceiver.java"


# instance fields
.field private final ACTION_GAN:Ljava/lang/String;

.field private GanStatus:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 22
    const-string v0, "com.android.kineto.GanState"

    iput-object v0, p0, Lcom/android/phone/PhoneReceiver;->ACTION_GAN:Ljava/lang/String;

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/PhoneReceiver;->GanStatus:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .parameter "context"
    .parameter "intent"

    .prologue
    const-string v2, "PhoneReceiver"

    .line 27
    const-string v0, "PhoneReceiver"

    const-string v0, "[onReceive] BroadcastReceiver; "

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.kineto.GanState"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    const-string v0, "GanState"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/PhoneReceiver;->GanStatus:Ljava/lang/String;

    .line 31
    const-string v0, "PhoneReceiver"

    const-string v0, "[onReceive] BroadcastReceiver; GanStatus = "

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    const-string v0, "REGISTERED"

    iget-object v1, p0, Lcom/android/phone/PhoneReceiver;->GanStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GAN_CS_CALL"

    iget-object v1, p0, Lcom/android/phone/PhoneReceiver;->GanStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GAN_CS_PS_CALL"

    iget-object v1, p0, Lcom/android/phone/PhoneReceiver;->GanStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 34
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/phone/PhoneUtils;->setGanOn(Z)V

    .line 38
    :cond_1
    :goto_0
    return-void

    .line 36
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/phone/PhoneUtils;->setGanOn(Z)V

    goto :goto_0
.end method
