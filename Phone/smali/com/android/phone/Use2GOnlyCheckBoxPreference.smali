.class public Lcom/android/phone/Use2GOnlyCheckBoxPreference;
.super Landroid/preference/ListPreference;
.source "Use2GOnlyCheckBoxPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;
    }
.end annotation


# static fields
.field private static final DBG:Z

.field private static final salesCode:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mDisconnectDataConnectionDialogListener:Landroid/content/DialogInterface$OnClickListener;

.field private mHandler:Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;

.field private mIsDisconnectingData:Z

.field private mNetworkType:I

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->DBG:Z

    .line 44
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->salesCode:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v3, 0x7f070019

    const v2, 0x7f070017

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    iput-boolean v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mIsDisconnectingData:Z

    .line 51
    iput v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    .line 54
    new-instance v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference$1;

    invoke-direct {v1, p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference$1;-><init>(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)V

    iput-object v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 153
    new-instance v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference$2;

    invoke-direct {v1, p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference$2;-><init>(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)V

    iput-object v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mDisconnectDataConnectionDialogListener:Landroid/content/DialogInterface$OnClickListener;

    .line 80
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 81
    iput-object p1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mContext:Landroid/content/Context;

    .line 83
    invoke-direct {p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->getNetworkModeOption()I

    move-result v0

    .line 85
    .local v0, currentNetworkModeOption:I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 86
    invoke-virtual {p0, v3}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->setEntries(I)V

    .line 87
    invoke-virtual {p0, v3}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->setEntryValues(I)V

    .line 93
    :cond_0
    :goto_0
    new-instance v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;-><init>(Lcom/android/phone/Use2GOnlyCheckBoxPreference;Lcom/android/phone/Use2GOnlyCheckBoxPreference$1;)V

    iput-object v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mHandler:Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;

    .line 95
    iget-object v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 96
    iget-object v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x40

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 98
    return-void

    .line 88
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 89
    invoke-virtual {p0, v2}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->setEntries(I)V

    .line 90
    invoke-virtual {p0, v2}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->setEntryValues(I)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mIsDisconnectingData:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/phone/Use2GOnlyCheckBoxPreference;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mIsDisconnectingData:Z

    return p1
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 37
    sget-boolean v0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->DBG:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->changeNetworkType()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mHandler:Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)Landroid/app/AlertDialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget v0, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    return v0
.end method

.method static synthetic access$802(Lcom/android/phone/Use2GOnlyCheckBoxPreference;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 37
    iput p1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    return p1
.end method

.method static synthetic access$900(Lcom/android/phone/Use2GOnlyCheckBoxPreference;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->getNetworkModeOption()I

    move-result v0

    return v0
.end method

.method private changeNetworkType()V
    .locals 4

    .prologue
    .line 146
    sget-boolean v0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "Use2GOnlyCheckBoxPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeNetworkType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->setEnabled(Z)V

    .line 148
    iget-object v0, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    iget v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    iget-object v2, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mHandler:Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    .line 150
    return-void
.end method

.method private getNetworkModeOption()I
    .locals 2

    .prologue
    .line 292
    const-string v0, "HUI"

    sget-object v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->salesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "TRG"

    sget-object v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->salesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    :cond_0
    const/4 v0, 0x2

    .line 297
    :goto_0
    return v0

    .line 294
    :cond_1
    const-string v0, "YOG"

    sget-object v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->salesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "3IE"

    sget-object v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->salesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "H3G"

    sget-object v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->salesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "HTD"

    sget-object v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->salesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "HTS"

    sget-object v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->salesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "DRE"

    sget-object v1, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->salesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 295
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 297
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getPreferredNetworkType()V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mHandler:Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/phone/Use2GOnlyCheckBoxPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/internal/telephony/Phone;->getPreferredNetworkType(Landroid/os/Message;)V

    .line 103
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 7
    .parameter "positiveResult"

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 107
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onDialogClosed(Z)V

    .line 108
    invoke-direct {p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->getNetworkModeOption()I

    move-result v0

    .line 110
    .local v0, currentNetworkModeOption:I
    if-eqz p1, :cond_0

    iget v2, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    invoke-virtual {p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    .line 115
    if-ne v0, v5, :cond_2

    .line 116
    iget v2, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    if-ne v5, v2, :cond_2

    .line 117
    iput v6, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    .line 126
    :cond_2
    iget-object v2, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v1

    .line 128
    .local v1, state:I
    sget-boolean v2, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->DBG:Z

    if-eqz v2, :cond_3

    const-string v2, "Use2GOnlyCheckBoxPreference"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDialogClosed : change to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mNetworkType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Data state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_3
    if-eq v1, v6, :cond_4

    if-ne v1, v5, :cond_5

    .line 132
    :cond_4
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mContext:Landroid/content/Context;

    const v4, 0x7f0d02da

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mContext:Landroid/content/Context;

    const v4, 0x7f0d02dd

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mContext:Landroid/content/Context;

    const v4, 0x7f0d0002

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mDisconnectDataConnectionDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mContext:Landroid/content/Context;

    const v4, 0x7f0d0067

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mDisconnectDataConnectionDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->mDialog:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 141
    :cond_5
    invoke-direct {p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->changeNetworkType()V

    goto/16 :goto_0
.end method

.method public setEntries(I)V
    .locals 2
    .parameter "entriesResId"

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->getNetworkModeOption()I

    move-result v0

    .line 178
    .local v0, currentNetworkModeOption:I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 179
    const v1, 0x7f070019

    invoke-super {p0, v1}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 185
    :goto_0
    return-void

    .line 180
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 181
    const v1, 0x7f070017

    invoke-super {p0, v1}, Landroid/preference/ListPreference;->setEntries(I)V

    goto :goto_0

    .line 183
    :cond_1
    const v1, 0x7f070015

    invoke-super {p0, v1}, Landroid/preference/ListPreference;->setEntries(I)V

    goto :goto_0
.end method

.method public setEntryValues(I)V
    .locals 2
    .parameter "entriesResId"

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/android/phone/Use2GOnlyCheckBoxPreference;->getNetworkModeOption()I

    move-result v0

    .line 190
    .local v0, currentNetworkModeOption:I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 191
    const v1, 0x7f07001a

    invoke-super {p0, v1}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 197
    :goto_0
    return-void

    .line 192
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 193
    const v1, 0x7f070018

    invoke-super {p0, v1}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto :goto_0

    .line 195
    :cond_1
    const v1, 0x7f070016

    invoke-super {p0, v1}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto :goto_0
.end method
