.class public Lcom/android/settings/wifi/WifiAPStationStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WifiAPStationStatusReceiver.java"


# instance fields
.field mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method clearNotification(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    .line 52
    const-string v0, "notification"

    .line 53
    .local v0, ns:Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/android/settings/wifi/WifiAPStationStatusReceiver;->mNotificationManager:Landroid/app/NotificationManager;

    .line 54
    iget-object v1, p0, Lcom/android/settings/wifi/WifiAPStationStatusReceiver;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v2, 0x3e5

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 55
    return-void
.end method

.method createNotification(Landroid/content/Context;)V
    .locals 10
    .parameter "context"

    .prologue
    const/4 v9, 0x0

    .line 39
    const v2, 0x108008a

    .line 40
    .local v2, icon:I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0801db

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 41
    .local v6, title:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0801dc

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, body:Ljava/lang/String;
    new-instance v3, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-direct {v3, v2, v6, v7, v8}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 43
    .local v3, notification:Landroid/app/Notification;
    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAXAP_NOTI"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 44
    .local v4, notificationIntent:Landroid/content/Intent;
    invoke-static {p1, v9, v4, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 45
    .local v1, contentIntent:Landroid/app/PendingIntent;
    invoke-virtual {v3, p1, v6, v0, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 46
    const-string v5, "notification"

    .line 47
    .local v5, ns:Ljava/lang/String;
    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    iput-object v7, p0, Lcom/android/settings/wifi/WifiAPStationStatusReceiver;->mNotificationManager:Landroid/app/NotificationManager;

    .line 48
    iget-object v7, p0, Lcom/android/settings/wifi/WifiAPStationStatusReceiver;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v8, 0x3e5

    invoke-virtual {v7, v8, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 49
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .parameter "context"
    .parameter "intent"

    .prologue
    const-string v4, "WifiAPStationStatusReceiver"

    .line 21
    const-string v2, "WifiAPStationStatusReceiver"

    const-string v2, "WifiAPStationStatusReceiver"

    invoke-static {v4, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 23
    .local v1, action:Ljava/lang/String;
    const-string v2, "android.intent.action.WIFI_AP_STA_STATUS_CHANGE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 24
    const-string v2, "STA_NUM"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 25
    .local v0, ClientNum:I
    const-string v2, "WifiAPStationStatusReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "event detected [ACTION_WIFI_AP_STA_STATUS_CHANGED] ClientNum = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    const/4 v2, 0x5

    if-lt v0, v2, :cond_1

    .line 27
    invoke-virtual {p0, p1}, Lcom/android/settings/wifi/WifiAPStationStatusReceiver;->createNotification(Landroid/content/Context;)V

    .line 36
    .end local v0           #ClientNum:I
    :cond_0
    :goto_0
    return-void

    .line 29
    .restart local v0       #ClientNum:I
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/settings/wifi/WifiAPStationStatusReceiver;->clearNotification(Landroid/content/Context;)V

    goto :goto_0

    .line 30
    .end local v0           #ClientNum:I
    :cond_2
    const-string v2, "android.intent.action.MAXAP_NOTI"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 31
    invoke-virtual {p0, p1}, Lcom/android/settings/wifi/WifiAPStationStatusReceiver;->clearNotification(Landroid/content/Context;)V

    goto :goto_0

    .line 33
    :cond_3
    const-string v2, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    invoke-virtual {p0, p1}, Lcom/android/settings/wifi/WifiAPStationStatusReceiver;->clearNotification(Landroid/content/Context;)V

    goto :goto_0
.end method
