.class Lcom/android/providers/telephony/TelephonyProvider$1;
.super Landroid/content/BroadcastReceiver;
.source "TelephonyProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/telephony/TelephonyProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/telephony/TelephonyProvider;


# direct methods
.method constructor <init>(Lcom/android/providers/telephony/TelephonyProvider;)V
    .locals 0
    .parameter

    .prologue
    .line 113
    iput-object p1, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->this$0:Lcom/android/providers/telephony/TelephonyProvider;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 116
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.CSC_CONNECTION_RESET_DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    monitor-enter p0

    .line 118
    :try_start_0
    iget-object v0, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->this$0:Lcom/android/providers/telephony/TelephonyProvider;

    const/4 v1, 0x1

    #setter for: Lcom/android/providers/telephony/TelephonyProvider;->mReceivedCscResetIntent:Z
    invoke-static {v0, v1}, Lcom/android/providers/telephony/TelephonyProvider;->access$002(Lcom/android/providers/telephony/TelephonyProvider;Z)Z

    .line 119
    monitor-exit p0

    .line 121
    :cond_0
    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
