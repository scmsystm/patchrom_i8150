.class public abstract Lcom/android/internal/telephony/CallTracker;
.super Landroid/os/Handler;
.source "CallTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/CallTracker$3;
    }
.end annotation


# static fields
.field private static final DBG_POLL:Z = false

.field protected static final EVENT_CALL_STATE_CHANGE:I = 0x2

.field protected static final EVENT_CALL_WAITING_INFO_CDMA:I = 0xf

.field protected static final EVENT_CONFERENCE_RESULT:I = 0xb

.field protected static final EVENT_DEFLECT_RESULT:I = 0x64

.field protected static final EVENT_ECT_RESULT:I = 0xd

.field protected static final EVENT_EXIT_ECM_RESPONSE_CDMA:I = 0xe

.field protected static final EVENT_GET_LAST_CALL_FAIL_CAUSE:I = 0x5

.field protected static final EVENT_OPERATION_COMPLETE:I = 0x4

.field protected static final EVENT_POLL_CALLS_RESULT:I = 0x1

.field protected static final EVENT_RADIO_AVAILABLE:I = 0x9

.field protected static final EVENT_RADIO_NOT_AVAILABLE:I = 0xa

.field protected static final EVENT_REPOLL_AFTER_DELAY:I = 0x3

.field protected static final EVENT_SEPARATE_RESULT:I = 0xc

.field protected static final EVENT_SWITCH_RESULT:I = 0x8

.field protected static final EVENT_THREE_WAY_DIAL_L2_RESULT_CDMA:I = 0x10

.field static final POLL_DELAY_MSEC:I = 0xfa

.field private static iqClientUnderContruction:Ljava/lang/Object;

.field private static volatile mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

.field private static submissionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/carrieriq/iqagent/client/Metric;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile submissionThread:Ljava/lang/Thread;

.field private static submissionThreadExists:Ljava/lang/Object;


# instance fields
.field public cm:Lcom/android/internal/telephony/CommandsInterface;

.field protected lastRelevantPoll:Landroid/os/Message;

.field protected needsPoll:Z

.field protected pendingOperations:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/internal/telephony/CallTracker;->iqClientUnderContruction:Ljava/lang/Object;

    .line 143
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/internal/telephony/CallTracker;->submissionThreadExists:Ljava/lang/Object;

    .line 144
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/android/internal/telephony/CallTracker;->submissionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 297
    return-void
.end method

.method static synthetic access$000()Lcom/carrieriq/iqagent/client/IQClient;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    return-object v0
.end method

.method static synthetic access$002(Lcom/carrieriq/iqagent/client/IQClient;)Lcom/carrieriq/iqagent/client/IQClient;
    .locals 0
    .parameter "x0"

    .prologue
    .line 43
    sput-object p0, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    return-object p0
.end method

.method static synthetic access$100()Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->submissionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/CallTracker;Lcom/carrieriq/iqagent/client/Metric;)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/CallTracker;->checkSubmit(Lcom/carrieriq/iqagent/client/Metric;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$302(Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .parameter "x0"

    .prologue
    .line 43
    sput-object p0, Lcom/android/internal/telephony/CallTracker;->submissionThread:Ljava/lang/Thread;

    return-object p0
.end method

.method private checkNoOperationsPending()Z
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized checkSubmit(Lcom/carrieriq/iqagent/client/Metric;)Z
    .locals 3
    .parameter "metric"

    .prologue
    const/4 v2, 0x0

    .line 206
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    move v0, v2

    .line 218
    :goto_0
    monitor-exit p0

    return v0

    .line 210
    :cond_0
    :try_start_1
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    sget v1, Lcom/carrieriq/iqagent/client/metrics/ui/UI01;->ID:I

    invoke-virtual {v0, v1}, Lcom/carrieriq/iqagent/client/IQClient;->shouldSubmitMetric(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 211
    const-string v0, "IQClient[TELEPHONY]"

    const-string v1, "[NG] iqClient.shouldSubmitMetric(UI01) == false --> renew IQClient"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    invoke-virtual {v0}, Lcom/carrieriq/iqagent/client/IQClient;->disconnect()V

    .line 213
    const/4 v0, 0x0

    sput-object v0, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    .line 214
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallTracker;->ensureIQClient()V

    move v0, v2

    .line 215
    goto :goto_0

    .line 217
    :cond_1
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    invoke-virtual {v0, p1}, Lcom/carrieriq/iqagent/client/IQClient;->submitMetric(Lcom/carrieriq/iqagent/client/Metric;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 218
    const/4 v0, 0x1

    goto :goto_0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static phoneStateMapingforICQ(Lcom/android/internal/telephony/Call$State;)B
    .locals 4
    .parameter "sIndex"

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 278
    sget-object v0, Lcom/android/internal/telephony/CallTracker$3;->$SwitchMap$com$android$internal$telephony$Call$State:[I

    invoke-virtual {p0}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 297
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 281
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 283
    :pswitch_1
    const/4 v0, 0x5

    goto :goto_0

    .line 285
    :pswitch_2
    const/16 v0, 0x8

    goto :goto_0

    :pswitch_3
    move v0, v2

    .line 287
    goto :goto_0

    :pswitch_4
    move v0, v3

    .line 289
    goto :goto_0

    :pswitch_5
    move v0, v3

    .line 291
    goto :goto_0

    :pswitch_6
    move v0, v2

    .line 293
    goto :goto_0

    .line 295
    :pswitch_7
    const/4 v0, 0x6

    goto :goto_0

    .line 278
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private submitMetric(Lcom/carrieriq/iqagent/client/Metric;)V
    .locals 3
    .parameter "metric"

    .prologue
    const-string v2, "IQClient[TELEPHONY]"

    .line 164
    const-string v0, "IQClient[TELEPHONY]"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Submit telephony metric: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/internal/telephony/CallTracker;->submissionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/CallTracker;->checkSubmit(Lcom/carrieriq/iqagent/client/Metric;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    const-string v0, "IQClient[TELEPHONY]"

    const-string v0, "Direct submission"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/CallTracker;->submitMetric(Lcom/carrieriq/iqagent/client/Metric;)V

    goto :goto_0

    .line 171
    :cond_2
    const-string v0, "IQClient[TELEPHONY]"

    const-string v0, "Put metric to queue"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->submissionQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 173
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->submissionThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 174
    sget-object v0, Lcom/android/internal/telephony/CallTracker;->submissionThreadExists:Ljava/lang/Object;

    monitor-enter v0

    .line 175
    :try_start_0
    sget-object v1, Lcom/android/internal/telephony/CallTracker;->submissionThread:Ljava/lang/Thread;

    if-nez v1, :cond_3

    .line 176
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/internal/telephony/CallTracker$2;

    invoke-direct {v2, p0}, Lcom/android/internal/telephony/CallTracker$2;-><init>(Lcom/android/internal/telephony/CallTracker;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v1, Lcom/android/internal/telephony/CallTracker;->submissionThread:Ljava/lang/Thread;

    .line 198
    sget-object v1, Lcom/android/internal/telephony/CallTracker;->submissionThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 200
    :cond_3
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method protected declared-synchronized ensureIQClient()V
    .locals 3

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    if-nez v1, :cond_1

    .line 149
    sget-object v1, Lcom/android/internal/telephony/CallTracker;->iqClientUnderContruction:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 150
    :try_start_1
    sget-object v2, Lcom/android/internal/telephony/CallTracker;->mIQClient:Lcom/carrieriq/iqagent/client/IQClient;

    if-nez v2, :cond_0

    .line 151
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/android/internal/telephony/CallTracker$1;

    invoke-direct {v2, p0}, Lcom/android/internal/telephony/CallTracker$1;-><init>(Lcom/android/internal/telephony/CallTracker;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 157
    .local v0, iqClientContructionThread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 159
    .end local v0           #iqClientContructionThread:Ljava/lang/Thread;
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    :cond_1
    monitor-exit p0

    return-void

    .line 159
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 148
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public abstract handleMessage(Landroid/os/Message;)V
.end method

.method protected abstract handlePollCalls(Landroid/os/AsyncResult;)V
.end method

.method protected handleRadioAvailable()V
    .locals 0

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallTracker;->pollCallsWhenSafe()V

    .line 106
    return-void
.end method

.method protected isCommandExceptionRadioNotAvailable(Ljava/lang/Throwable;)Z
    .locals 2
    .parameter "e"

    .prologue
    .line 97
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/android/internal/telephony/CommandException;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/internal/telephony/CommandException;

    .end local p1
    invoke-virtual {p1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v0

    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract log(Ljava/lang/String;)V
.end method

.method protected obtainNoPollCompleteMessage(I)Landroid/os/Message;
    .locals 1
    .parameter "what"

    .prologue
    .line 118
    iget v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/internal/telephony/CallTracker;->pendingOperations:I

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    .line 120
    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallTracker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method protected pollCallsAfterDelay()V
    .locals 3

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/android/internal/telephony/CallTracker;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 91
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 92
    const-wide/16 v1, 0xfa

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/internal/telephony/CallTracker;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 93
    return-void
.end method

.method protected pollCallsWhenSafe()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 79
    iput-boolean v1, p0, Lcom/android/internal/telephony/CallTracker;->needsPoll:Z

    .line 81
    invoke-direct {p0}, Lcom/android/internal/telephony/CallTracker;->checkNoOperationsPending()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0, v1}, Lcom/android/internal/telephony/CallTracker;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    .line 83
    iget-object v0, p0, Lcom/android/internal/telephony/CallTracker;->cm:Lcom/android/internal/telephony/CommandsInterface;

    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker;->lastRelevantPoll:Landroid/os/Message;

    invoke-interface {v0, v1}, Lcom/android/internal/telephony/CommandsInterface;->getCurrentCalls(Landroid/os/Message;)V

    .line 85
    :cond_0
    return-void
.end method

.method protected submitGS01(Landroid/content/Context;Lcom/android/internal/telephony/Connection;ZZLjava/lang/String;)V
    .locals 0
    .parameter "context"
    .parameter "connection"
    .parameter "isOutgoing"
    .parameter "isVideo"
    .parameter "number"

    .prologue
    .line 238
    return-void
.end method

.method protected submitGS02(Landroid/content/Context;Lcom/android/internal/telephony/Connection;)V
    .locals 1
    .parameter "context"
    .parameter "connection"

    .prologue
    .line 255
    invoke-virtual {p2}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/CallTracker;->phoneStateMapingforICQ(Lcom/android/internal/telephony/Call$State;)B

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/internal/telephony/CallTracker;->submitGS02(Landroid/content/Context;Lcom/android/internal/telephony/Connection;I)V

    .line 256
    return-void
.end method

.method protected submitGS02(Landroid/content/Context;Lcom/android/internal/telephony/Connection;I)V
    .locals 0
    .parameter "context"
    .parameter "connection"
    .parameter "callState"

    .prologue
    .line 252
    return-void
.end method

.method protected submitSG03(Landroid/content/Context;Lcom/android/internal/telephony/Connection;S)V
    .locals 0
    .parameter "context"
    .parameter "connection"
    .parameter "termCode"

    .prologue
    .line 275
    return-void
.end method
