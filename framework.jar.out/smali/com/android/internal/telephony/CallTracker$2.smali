.class Lcom/android/internal/telephony/CallTracker$2;
.super Ljava/lang/Object;
.source "CallTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/telephony/CallTracker;->submitMetric(Lcom/carrieriq/iqagent/client/Metric;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/CallTracker;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/CallTracker;)V
    .locals 0
    .parameter

    .prologue
    .line 176
    iput-object p1, p0, Lcom/android/internal/telephony/CallTracker$2;->this$0:Lcom/android/internal/telephony/CallTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const-string v3, "IQClient[TELEPHONY]"

    .line 178
    const-string v1, "IQClient[TELEPHONY]"

    const-string v1, "Submission thread created"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    invoke-static {}, Lcom/android/internal/telephony/CallTracker;->access$100()Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/carrieriq/iqagent/client/Metric;

    .line 181
    .local v0, cur:Lcom/carrieriq/iqagent/client/Metric;
    :goto_0
    if-eqz v0, :cond_2

    .line 182
    :goto_1
    invoke-static {}, Lcom/android/internal/telephony/CallTracker;->access$000()Lcom/carrieriq/iqagent/client/IQClient;

    move-result-object v1

    if-nez v1, :cond_0

    .line 184
    const-wide/16 v1, 0xa

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 185
    :catch_0
    move-exception v1

    goto :goto_1

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/android/internal/telephony/CallTracker$2;->this$0:Lcom/android/internal/telephony/CallTracker;

    #calls: Lcom/android/internal/telephony/CallTracker;->checkSubmit(Lcom/carrieriq/iqagent/client/Metric;)Z
    invoke-static {v1, v0}, Lcom/android/internal/telephony/CallTracker;->access$200(Lcom/android/internal/telephony/CallTracker;Lcom/carrieriq/iqagent/client/Metric;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    invoke-static {}, Lcom/android/internal/telephony/CallTracker;->access$100()Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    .line 190
    const-string v1, "IQClient[TELEPHONY]"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Submitted from queue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_1
    invoke-static {}, Lcom/android/internal/telephony/CallTracker;->access$100()Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #cur:Lcom/carrieriq/iqagent/client/Metric;
    check-cast v0, Lcom/carrieriq/iqagent/client/Metric;

    .restart local v0       #cur:Lcom/carrieriq/iqagent/client/Metric;
    goto :goto_0

    .line 194
    :cond_2
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/android/internal/telephony/CallTracker;->access$302(Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 195
    const-string v1, "IQClient[TELEPHONY]"

    const-string v1, "Submission thread finished"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return-void
.end method
