.class public Lcom/carrieriq/iqagent/CIQFeature;
.super Ljava/lang/Object;
.source "CIQFeature.java"


# static fields
.field public static final APP_WATCHER:Z = true

.field public static final BATTERY_STATE:Z = true

.field public static final BP_3GPP:Z = false

.field public static final BP_3GPP_ONLY:Z = false

.field public static final CALL_SCANNER:Z = true

.field public static final CDMA:Z = false

.field public static final CONNECTIVITY_STATE:Z = true

.field public static final CSFB_GS46:Z = false

.field public static final EXTENDED:Z = false

.field public static final EXTENDED_ONLY:Z = false

.field public static final EXTENDED_PRELOAD:Z = true

.field public static final EXTENDED_PRELOAD_ONLY:Z = true

.field public static final EXTENDED_WO_PRE:Z = false

.field public static final GPS_SCANNER:Z = true

.field public static final PHONE_STATE:Z = true

.field public static final PRELOAD:Z = true

.field public static final PRELOAD_ONLY:Z = true

.field public static final PRELOAD_WO_3GPP:Z = true

.field public static final PRELOAD_WO_EXT:Z = true

.field public static final SURVEY:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
