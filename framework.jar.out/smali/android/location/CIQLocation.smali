.class public Landroid/location/CIQLocation;
.super Ljava/lang/Object;
.source "CIQLocation.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public lAltitude:J

.field public lHeading:J

.field public lLatitude:J

.field public lLongitude:J

.field public lUncertaintyAlong:J

.field public lUncertaintyAltitude:J

.field public lUncertaintyAngle:J

.field public lUncertaintyPerpendicular:J

.field public lVelocityHorizontal:J

.field public lVelocityVertical:J

.field public provider:Ljava/lang/String;

.field public ucFieldsValid:B

.field public ucGpsRequestType:B

.field public ucGpsResult:B

.field public ucGpsSource:B


# direct methods
.method public constructor <init>(BBBBDDDDDDDDDD)V
    .locals 4
    .parameter "_ucGpsRequestType"
    .parameter "_ucGpsSource"
    .parameter "_ucGpsResult"
    .parameter "_ucFieldsValid"
    .parameter "_lLatitude"
    .parameter "_lLongitude"
    .parameter "_lAltitude"
    .parameter "_lVelocityHorizontal"
    .parameter "_lVelocityVertical"
    .parameter "_lHeading"
    .parameter "_lUncertaintyAngle"
    .parameter "_lUncertaintyAlong"
    .parameter "_lUncertaintyPerpendicular"
    .parameter "_lUncertaintyAltitude"

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-string v0, "CIQ_GPS"

    iput-object v0, p0, Landroid/location/CIQLocation;->provider:Ljava/lang/String;

    .line 38
    iput-byte p1, p0, Landroid/location/CIQLocation;->ucGpsRequestType:B

    .line 39
    iput-byte p2, p0, Landroid/location/CIQLocation;->ucGpsSource:B

    .line 40
    iput-byte p3, p0, Landroid/location/CIQLocation;->ucGpsResult:B

    .line 41
    iput-byte p4, p0, Landroid/location/CIQLocation;->ucFieldsValid:B

    .line 42
    invoke-static {p5, p6}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lLatitude:J

    .line 43
    invoke-static {p7, p8}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lLongitude:J

    .line 44
    invoke-static {p9, p10}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lAltitude:J

    .line 45
    invoke-static/range {p11 .. p12}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lVelocityHorizontal:J

    .line 46
    invoke-static/range {p13 .. p14}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lVelocityVertical:J

    .line 47
    invoke-static/range {p15 .. p16}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lHeading:J

    .line 48
    invoke-static/range {p17 .. p18}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lUncertaintyAngle:J

    .line 49
    invoke-static/range {p19 .. p20}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lUncertaintyAlong:J

    .line 50
    invoke-static/range {p21 .. p22}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lUncertaintyPerpendicular:J

    .line 51
    invoke-static/range {p23 .. p24}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/location/CIQLocation;->lUncertaintyAltitude:J

    .line 52
    iget-object v0, p0, Landroid/location/CIQLocation;->provider:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Landroid/location/CIQLocation;->lLatitude:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iget-object v0, p0, Landroid/location/CIQLocation;->provider:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Landroid/location/CIQLocation;->lLongitude:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    iget-object v0, p0, Landroid/location/CIQLocation;->provider:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Landroid/location/CIQLocation;->lAltitude:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget-object v0, p0, Landroid/location/CIQLocation;->provider:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Landroid/location/CIQLocation;->lVelocityHorizontal:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    iget-object v0, p0, Landroid/location/CIQLocation;->provider:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Landroid/location/CIQLocation;->lVelocityVertical:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Landroid/location/CIQLocation;->provider:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Landroid/location/CIQLocation;->lUncertaintyAltitude:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    return-void
.end method
