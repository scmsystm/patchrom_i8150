.class public Landroid/text/StaticLayout;
.super Landroid/text/Layout;
.source "StaticLayout.java"


# static fields
.field private static final COLUMNS_ELLIPSIZE:I = 0x5

.field private static final COLUMNS_NORMAL:I = 0x3

.field private static final DESCENT:I = 0x2

.field private static final DIR:I = 0x0

.field private static final DIR_MASK:I = -0x40000000

.field private static final DIR_SHIFT:I = 0x1e

.field private static final ELLIPSIS_COUNT:I = 0x4

.field private static final ELLIPSIS_START:I = 0x3

.field private static final FIRST_CJK:C = '\u2e80'

.field private static final FIRST_RIGHT_TO_LEFT:C = '\u0590'

.field public static final OUTERHEIGHT_INFINITE:I = 0x0

.field private static final START:I = 0x0

.field private static final START_MASK:I = 0x1fffffff

.field private static final TAB:I = 0x0

.field private static final TAB_MASK:I = 0x20000000

.field private static final TOP:I = 0x1


# instance fields
.field private mBottomPadding:I

.field private mChdirs:[B

.field private mChs:[C

.field private mColumns:I

.field private mEllipsizedWidth:I

.field private mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

.field private mLineCount:I

.field private mLineDirections:[Landroid/text/Layout$Directions;

.field private mLines:[I

.field private mTopPadding:I

.field private mWidths:[F


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;IILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V
    .registers 32
    .parameter "source"
    .parameter "bufstart"
    .parameter "bufend"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "outerheight"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"
    .parameter "ellipsize"
    .parameter "ellipsizedWidth"

    .prologue
    if-nez p11, :cond_125

    move-object/from16 v3, p1

    :goto_4
    move-object/from16 v2, p0

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    invoke-direct/range {v2 .. v8}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    new-instance v2, Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {v2}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

    if-eqz p11, :cond_142

    invoke-virtual/range {p0 .. p0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v17

    check-cast v17, Landroid/text/Layout$Ellipsizer;

    .local v17, e:Landroid/text/Layout$Ellipsizer;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    iput-object v0, v1, Landroid/text/Layout$Ellipsizer;->mLayout:Landroid/text/Layout;

    move/from16 v0, p12

    move-object/from16 v1, v17

    iput v0, v1, Landroid/text/Layout$Ellipsizer;->mWidth:I

    move-object/from16 v0, p11

    move-object/from16 v1, v17

    iput-object v0, v1, Landroid/text/Layout$Ellipsizer;->mMethod:Landroid/text/TextUtils$TruncateAt;

    move/from16 v0, p12

    move-object/from16 v1, p0

    iput v0, v1, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    const/4 v2, 0x5

    move v0, v2

    move-object/from16 v1, p0

    iput v0, v1, Landroid/text/StaticLayout;->mColumns:I

    .end local v17           #e:Landroid/text/Layout$Ellipsizer;
    :goto_43
    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move v2, v0

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    move-result v2

    new-array v2, v2, [I

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mLines:[I

    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move v2, v0

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    move-result v2

    new-array v2, v2, [Landroid/text/Layout$Directions;

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    if-eqz p11, :cond_150

    const/4 v2, 0x1

    move v14, v2

    :goto_6b
    move/from16 v0, p12

    int-to-float v0, v0

    move v15, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p10

    move-object/from16 v16, p11

    invoke-virtual/range {v2 .. v16}, Landroid/text/StaticLayout;->generate(Ljava/lang/CharSequence;IILandroid/text/TextPaint;IILandroid/text/Layout$Alignment;FFZZZFLandroid/text/TextUtils$TruncateAt;)V

    if-eqz p6, :cond_10c

    invoke-virtual/range {p0 .. p0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v2

    move-object/from16 v0, p0

    move v1, v2

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v2

    move v0, v2

    move/from16 v1, p6

    if-le v0, v1, :cond_10c

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p11

    move-object v1, v2

    if-ne v0, v1, :cond_10c

    move-object/from16 v0, p0

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineForVertical(I)I

    move-result v18

    .local v18, lastline:I
    if-lez v18, :cond_10c

    invoke-virtual/range {p0 .. p0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v2

    move/from16 v0, v18

    move v1, v2

    if-ge v0, v1, :cond_10c

    add-int/lit8 v2, v18, 0x1

    invoke-virtual/range {p0 .. p0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v3

    if-ge v2, v3, :cond_10c

    add-int/lit8 v2, v18, 0x1

    move-object/from16 v0, p0

    move v1, v2

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v2

    move v0, v2

    move/from16 v1, p6

    if-le v0, v1, :cond_d0

    add-int/lit8 v18, v18, -0x1

    :cond_d0
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    move-object v2, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move v3, v0

    mul-int v3, v3, v18

    add-int/lit8 v3, v3, 0x3

    add-int/lit8 v4, v18, 0x1

    move-object/from16 v0, p0

    move v1, v4

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineStart(I)I

    move-result v4

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineStart(I)I

    move-result v5

    sub-int/2addr v4, v5

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    aput v4, v2, v3

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    move-object v2, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move v3, v0

    mul-int v3, v3, v18

    add-int/lit8 v3, v3, 0x4

    const/4 v4, 0x1

    aput v4, v2, v3

    add-int/lit8 v2, v18, 0x1

    move v0, v2

    move-object/from16 v1, p0

    iput v0, v1, Landroid/text/StaticLayout;->mLineCount:I

    .end local v18           #lastline:I
    :cond_10c
    const/4 v2, 0x0

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mChdirs:[B

    const/4 v2, 0x0

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mChs:[C

    const/4 v2, 0x0

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mWidths:[F

    const/4 v2, 0x0

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

    return-void

    :cond_125
    move-object/from16 v0, p1

    instance-of v0, v0, Landroid/text/Spanned;

    move v2, v0

    if-eqz v2, :cond_137

    new-instance v2, Landroid/text/Layout$SpannedEllipsizer;

    move-object v0, v2

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/text/Layout$SpannedEllipsizer;-><init>(Ljava/lang/CharSequence;)V

    move-object v3, v2

    goto/16 :goto_4

    :cond_137
    new-instance v2, Landroid/text/Layout$Ellipsizer;

    move-object v0, v2

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/text/Layout$Ellipsizer;-><init>(Ljava/lang/CharSequence;)V

    move-object v3, v2

    goto/16 :goto_4

    :cond_142
    const/4 v2, 0x3

    move v0, v2

    move-object/from16 v1, p0

    iput v0, v1, Landroid/text/StaticLayout;->mColumns:I

    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    goto/16 :goto_43

    :cond_150
    const/4 v2, 0x0

    move v14, v2

    goto/16 :goto_6b
.end method

.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .registers 22
    .parameter "source"
    .parameter "bufstart"
    .parameter "bufend"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"

    .prologue
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V
    .registers 25
    .parameter "source"
    .parameter "bufstart"
    .parameter "bufend"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"
    .parameter "ellipsize"
    .parameter "ellipsizedWidth"

    .prologue
    const/4 v6, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v0 .. v12}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;IILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .registers 18
    .parameter "source"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"

    .prologue
    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-void
.end method

.method constructor <init>(Z)V
    .registers 9
    .parameter "ellipsize"

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v4, v1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    new-instance v0, Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {v0}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    iput-object v0, p0, Landroid/text/StaticLayout;->mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

    const/4 v0, 0x5

    iput v0, p0, Landroid/text/StaticLayout;->mColumns:I

    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    move-result v0

    new-array v0, v0, [Landroid/text/Layout$Directions;

    iput-object v0, p0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    return-void
.end method

.method private static adjustTextWidths([FLjava/lang/CharSequence;III)V
    .registers 12
    .parameter "widths"
    .parameter "text"
    .parameter "curPos"
    .parameter "nextPos"
    .parameter "actualNum"

    .prologue
    const/4 v6, 0x1

    sub-int v5, p3, p2

    sub-int v1, v5, v6

    .local v1, dstIndex:I
    sub-int v4, p4, v6

    .local v4, srcIndex:I
    move v2, v1

    .end local v1           #dstIndex:I
    .local v2, dstIndex:I
    :goto_8
    if-ltz v4, :cond_34

    add-int v5, v2, p2

    :try_start_c
    invoke-interface {p1, v5}, Ljava/lang/CharSequence;->charAt(I)C
    :try_end_f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_c .. :try_end_f} :catch_2a

    move-result v0

    .local v0, c:C
    const v5, 0xd800

    if-lt v0, v5, :cond_20

    const v5, 0xdfff

    if-gt v0, v5, :cond_20

    add-int/lit8 v1, v2, -0x1

    .end local v2           #dstIndex:I
    .restart local v1       #dstIndex:I
    const/4 v5, 0x0

    :try_start_1d
    aput v5, p0, v2

    move v2, v1

    .end local v1           #dstIndex:I
    .restart local v2       #dstIndex:I
    :cond_20
    add-int/lit8 v1, v2, -0x1

    .end local v2           #dstIndex:I
    .restart local v1       #dstIndex:I
    aget v5, p0, v4

    aput v5, p0, v2
    :try_end_26
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1d .. :try_end_26} :catch_35

    add-int/lit8 v4, v4, -0x1

    move v2, v1

    .end local v1           #dstIndex:I
    .restart local v2       #dstIndex:I
    goto :goto_8

    .end local v0           #c:C
    :catch_2a
    move-exception v5

    move-object v3, v5

    move v1, v2

    .end local v2           #dstIndex:I
    .restart local v1       #dstIndex:I
    .local v3, e:Ljava/lang/IndexOutOfBoundsException;
    :goto_2d
    const-string v5, "text"

    const-string v6, "adjust text widths failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v1           #dstIndex:I
    .end local v3           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_34
    return-void

    .restart local v0       #c:C
    .restart local v1       #dstIndex:I
    :catch_35
    move-exception v5

    move-object v3, v5

    goto :goto_2d
.end method

.method static bidi(I[C[BIZ)I
    .registers 11
    .parameter "dir"
    .parameter "chs"
    .parameter "chInfo"
    .parameter "n"
    .parameter "hasInfo"

    .prologue
    .line 645
    invoke-static {p1, p2, p3}, Landroid/text/AndroidCharacter;->getDirectionalities([C[BI)V

    .line 650
    const/4 p4, 0x1

    if-eq p0, p4, :cond_18

    .end local p4
    const/4 p4, -0x1

    if-eq p0, p4, :cond_18

    .line 652
    const/4 p0, 0x1

    .line 653
    const/4 p4, 0x0

    .local p4, j:I
    :goto_b
    if-ge p4, p3, :cond_18

    .line 654
    aget-byte v0, p2, p4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_17

    aget-byte v0, p2, p4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2b

    .line 656
    :cond_17
    const/4 p0, -0x1

    .line 670
    .end local p4           #j:I
    :cond_18
    const/4 p4, 0x1

    if-ne p0, p4, :cond_2e

    const/4 p4, 0x0

    .line 677
    .local p4, SOR:B
    :goto_1c
    const/4 v0, 0x0

    .local v0, j:I
    :goto_1d
    if-ge v0, p3, :cond_38

    .line 678
    aget-byte v1, p2, v0

    const/4 v2, 0x6

    if-ne v1, v2, :cond_28

    .line 679
    if-nez v0, :cond_30

    .line 680
    aput-byte p4, p2, v0

    .line 677
    :cond_28
    :goto_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 653
    .end local v0           #j:I
    .local p4, j:I
    :cond_2b
    add-int/lit8 p4, p4, 0x1

    goto :goto_b

    .line 670
    .end local p4           #j:I
    :cond_2e
    const/4 p4, 0x1

    goto :goto_1c

    .line 682
    .restart local v0       #j:I
    .local p4, SOR:B
    :cond_30
    const/4 v1, 0x1

    sub-int v1, v0, v1

    aget-byte v1, p2, v1

    aput-byte v1, p2, v0

    goto :goto_28

    .line 689
    :cond_38
    const/4 v0, 0x0

    :goto_39
    if-ge v0, p3, :cond_46

    .line 690
    aget-byte v1, p2, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_43

    .line 691
    const/4 v1, 0x1

    aput-byte v1, p2, v0

    .line 689
    :cond_43
    add-int/lit8 v0, v0, 0x1

    goto :goto_39

    .line 697
    :cond_46
    const/4 v0, 0x1

    move v1, v0

    .end local v0           #j:I
    .local v1, j:I
    :goto_48
    const/4 v0, 0x1

    sub-int v0, p3, v0

    if-ge v1, v0, :cond_7e

    .line 698
    aget-byte v0, p2, v1

    .line 699
    .local v0, d:B
    const/4 v2, 0x1

    sub-int v2, v1, v2

    aget-byte v3, p2, v2

    .line 700
    .local v3, prev:B
    add-int/lit8 v2, v1, 0x1

    aget-byte v2, p2, v2

    .line 702
    .local v2, next:B
    const/4 v4, 0x4

    if-ne v0, v4, :cond_68

    .line 703
    const/4 v0, 0x3

    if-ne v3, v0, :cond_64

    .end local v0           #d:B
    const/4 v0, 0x3

    if-ne v2, v0, :cond_64

    .line 705
    const/4 v0, 0x3

    aput-byte v0, p2, v1

    .line 697
    :cond_64
    :goto_64
    add-int/lit8 v0, v1, 0x1

    .end local v1           #j:I
    .local v0, j:I
    move v1, v0

    .end local v0           #j:I
    .restart local v1       #j:I
    goto :goto_48

    .line 706
    .local v0, d:B
    :cond_68
    const/4 v4, 0x7

    if-ne v0, v4, :cond_64

    .line 707
    const/4 v0, 0x3

    if-ne v3, v0, :cond_74

    .end local v0           #d:B
    const/4 v0, 0x3

    if-ne v2, v0, :cond_74

    .line 709
    const/4 v0, 0x3

    aput-byte v0, p2, v1

    .line 710
    :cond_74
    const/4 v0, 0x6

    if-ne v3, v0, :cond_64

    const/4 v0, 0x6

    if-ne v2, v0, :cond_64

    .line 712
    const/4 v0, 0x6

    aput-byte v0, p2, v1

    goto :goto_64

    .line 719
    .end local v2           #next:B
    .end local v3           #prev:B
    :cond_7e
    const/4 v0, 0x0

    .line 720
    .local v0, adjacent:Z
    const/4 v1, 0x0

    move v2, v1

    .end local v1           #j:I
    .local v2, j:I
    :goto_81
    if-ge v2, p3, :cond_98

    .line 721
    aget-byte v1, p2, v2

    .line 723
    .local v1, d:B
    const/4 v3, 0x3

    if-ne v1, v3, :cond_8d

    .line 724
    const/4 v0, 0x1

    .line 720
    .end local v1           #d:B
    :goto_89
    add-int/lit8 v1, v2, 0x1

    .end local v2           #j:I
    .local v1, j:I
    move v2, v1

    .end local v1           #j:I
    .restart local v2       #j:I
    goto :goto_81

    .line 725
    .local v1, d:B
    :cond_8d
    const/4 v3, 0x5

    if-ne v1, v3, :cond_96

    if-eqz v0, :cond_96

    .line 726
    const/4 v1, 0x3

    aput-byte v1, p2, v2

    goto :goto_89

    .line 728
    :cond_96
    const/4 v0, 0x0

    goto :goto_89

    .line 735
    .end local v1           #d:B
    :cond_98
    const/4 v0, 0x0

    .line 736
    const/4 v1, 0x1

    sub-int v1, p3, v1

    .end local v2           #j:I
    .local v1, j:I
    move v2, v1

    .end local v1           #j:I
    .restart local v2       #j:I
    :goto_9d
    if-ltz v2, :cond_cb

    .line 737
    aget-byte v1, p2, v2

    .line 739
    .local v1, d:B
    const/4 v3, 0x3

    if-ne v1, v3, :cond_a9

    .line 740
    const/4 v0, 0x1

    .line 736
    .end local v1           #d:B
    :cond_a5
    :goto_a5
    add-int/lit8 v1, v2, -0x1

    .end local v2           #j:I
    .local v1, j:I
    move v2, v1

    .end local v1           #j:I
    .restart local v2       #j:I
    goto :goto_9d

    .line 741
    .local v1, d:B
    :cond_a9
    const/4 v3, 0x5

    if-ne v1, v3, :cond_b7

    .line 742
    if-eqz v0, :cond_b2

    .line 743
    const/4 v1, 0x3

    aput-byte v1, p2, v2

    goto :goto_a5

    .line 745
    :cond_b2
    const/16 v1, 0xd

    aput-byte v1, p2, v2

    goto :goto_a5

    .line 748
    :cond_b7
    const/4 v0, 0x0

    .line 750
    const/4 v3, 0x4

    if-eq v1, v3, :cond_c6

    const/4 v3, 0x7

    if-eq v1, v3, :cond_c6

    const/16 v3, 0xa

    if-eq v1, v3, :cond_c6

    const/16 v3, 0xb

    if-ne v1, v3, :cond_a5

    .line 754
    :cond_c6
    const/16 v1, 0xd

    aput-byte v1, p2, v2

    goto :goto_a5

    .line 761
    .end local v1           #d:B
    :cond_cb
    move v0, p4

    .line 762
    .local v0, cur:B
    const/4 v1, 0x0

    .end local v2           #j:I
    .local v1, j:I
    move v2, v1

    .end local v1           #j:I
    .restart local v2       #j:I
    :goto_ce
    if-ge v2, p3, :cond_113

    .line 763
    aget-byte v1, p2, v2

    .line 765
    .local v1, d:B
    if-eqz v1, :cond_d7

    const/4 v3, 0x1

    if-ne v1, v3, :cond_dd

    .line 767
    :cond_d7
    move v0, v1

    move v1, v2

    .line 762
    .end local v2           #j:I
    .local v1, j:I
    :goto_d9
    add-int/lit8 v1, v1, 0x1

    move v2, v1

    .end local v1           #j:I
    .restart local v2       #j:I
    goto :goto_ce

    .line 768
    .local v1, d:B
    :cond_dd
    const/4 v3, 0x3

    if-eq v1, v3, :cond_159

    const/4 v3, 0x6

    if-ne v1, v3, :cond_e5

    move v1, v2

    .end local v2           #j:I
    .local v1, j:I
    goto :goto_d9

    .line 770
    .local v1, d:B
    .restart local v2       #j:I
    :cond_e5
    if-ne v0, p4, :cond_eb

    .line 771
    aput-byte v0, p2, v2

    move v1, v2

    .end local v2           #j:I
    .local v1, j:I
    goto :goto_d9

    .line 773
    .local v1, d:B
    .restart local v2       #j:I
    :cond_eb
    move v1, p4

    .line 776
    .local v1, dd:B
    add-int/lit8 v3, v2, 0x1

    .local v3, k:I
    :goto_ee
    if-ge v3, p3, :cond_f7

    .line 777
    aget-byte v1, p2, v3

    .line 779
    if-eqz v1, :cond_f7

    const/4 v4, 0x1

    if-ne v1, v4, :cond_10c

    .line 785
    :cond_f7
    if-eq v1, v0, :cond_fa

    .line 786
    move v1, p4

    .line 788
    :cond_fa
    move v2, v2

    .local v2, y:I
    :goto_fb
    if-ge v2, v3, :cond_10f

    .line 789
    aget-byte v4, p2, v2

    const/4 v5, 0x3

    if-eq v4, v5, :cond_109

    aget-byte v4, p2, v2

    const/4 v5, 0x6

    if-eq v4, v5, :cond_109

    .line 791
    aput-byte v1, p2, v2

    .line 788
    :cond_109
    add-int/lit8 v2, v2, 0x1

    goto :goto_fb

    .line 776
    .local v2, j:I
    :cond_10c
    add-int/lit8 v3, v3, 0x1

    goto :goto_ee

    .line 793
    .local v2, y:I
    :cond_10f
    const/4 v1, 0x1

    sub-int v1, v3, v1

    .local v1, j:I
    goto :goto_d9

    .line 800
    .end local v1           #j:I
    .end local v3           #k:I
    .local v2, j:I
    :cond_113
    move v0, p4

    .line 801
    const/4 v1, 0x0

    .end local v2           #j:I
    .restart local v1       #j:I
    move v2, v1

    .end local v1           #j:I
    .restart local v2       #j:I
    :goto_116
    if-ge v2, p3, :cond_12f

    .line 802
    aget-byte v1, p2, v2

    .line 804
    .local v1, d:B
    if-eq v1, p4, :cond_121

    if-eqz v1, :cond_121

    const/4 v3, 0x1

    if-ne v1, v3, :cond_122

    .line 807
    :cond_121
    move v0, v1

    .line 809
    :cond_122
    const/4 v3, 0x3

    if-eq v1, v3, :cond_128

    const/4 v3, 0x6

    if-ne v1, v3, :cond_12b

    .line 811
    :cond_128
    const/4 v1, 0x0

    aput-byte v1, p2, v2

    .line 801
    .end local v1           #d:B
    :cond_12b
    add-int/lit8 v1, v2, 0x1

    .end local v2           #j:I
    .local v1, j:I
    move v2, v1

    .end local v1           #j:I
    .restart local v2       #j:I
    goto :goto_116

    .line 820
    :cond_12f
    const/4 v0, 0x0

    .end local v2           #j:I
    .local v0, j:I
    move v1, v0

    .end local v0           #j:I
    .restart local v1       #j:I
    :goto_131
    if-ge v1, p3, :cond_149

    .line 821
    aget-char v0, p1, v1

    .line 823
    .local v0, c:C
    const/16 v2, 0x9

    if-eq v0, v2, :cond_143

    const v2, 0xd800

    if-lt v0, v2, :cond_145

    const v2, 0xdfff

    if-gt v0, v2, :cond_145

    .line 824
    :cond_143
    aput-byte p4, p2, v1

    .line 820
    :cond_145
    add-int/lit8 v0, v1, 0x1

    .end local v1           #j:I
    .local v0, j:I
    move v1, v0

    .end local v0           #j:I
    .restart local v1       #j:I
    goto :goto_131

    .line 829
    :cond_149
    const/4 p4, 0x0

    .end local v1           #j:I
    .local p4, j:I
    :goto_14a
    if-ge p4, p3, :cond_158

    .line 830
    aget-char v0, p1, p4

    packed-switch v0, :pswitch_data_15c

    .line 829
    :goto_151
    add-int/lit8 p4, p4, 0x1

    goto :goto_14a

    .line 844
    :pswitch_154
    const/4 v0, 0x0

    aput-byte v0, p2, p4

    goto :goto_151

    .line 848
    :cond_158
    return p0

    .local v0, cur:B
    .local v1, d:B
    .restart local v2       #j:I
    .local p4, SOR:B
    :cond_159
    move v1, v2

    .end local v2           #j:I
    .local v1, j:I
    goto/16 :goto_d9

    .line 830
    :pswitch_data_15c
    .packed-switch 0x2b
        :pswitch_154
    .end packed-switch
.end method

.method private calculateEllipsis(II[FIIFLandroid/text/TextUtils$TruncateAt;IFLandroid/text/TextPaint;)V
    .registers 29
    .parameter "linestart"
    .parameter "lineend"
    .parameter "widths"
    .parameter "widstart"
    .parameter "widoff"
    .parameter "avail"
    .parameter "where"
    .parameter "line"
    .parameter "textwidth"
    .parameter "paint"

    .prologue
    sub-int v8, p2, p1

    .local v8, len:I
    cmpg-float v15, p9, p6

    if-gtz v15, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    move-object v15, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move/from16 v16, v0

    mul-int v16, v16, p8

    add-int/lit8 v16, v16, 0x3

    const/16 v17, 0x0

    aput v17, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    move-object v15, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move/from16 v16, v0

    mul-int v16, v16, p8

    add-int/lit8 v16, v16, 0x4

    const/16 v17, 0x0

    aput v17, v15, v16

    :goto_2c
    return-void

    :cond_2d
    const-string v15, "\u2026"

    move-object/from16 v0, p10

    move-object v1, v15

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    .local v4, ellipsiswid:F
    sget-object v15, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p7

    move-object v1, v15

    if-ne v0, v1, :cond_7c

    const/4 v13, 0x0

    .local v13, sum:F
    move v5, v8

    .local v5, i:I
    :goto_3f
    if-ltz v5, :cond_53

    const/4 v15, 0x1

    sub-int v15, v5, v15

    add-int v15, v15, p1

    sub-int v15, v15, p4

    add-int v15, v15, p5

    aget v14, p3, v15

    .local v14, w:F
    add-float v15, v14, v13

    add-float/2addr v15, v4

    cmpl-float v15, v15, p6

    if-lez v15, :cond_78

    .end local v14           #w:F
    :cond_53
    const/4 v3, 0x0

    .local v3, ellipsisStart:I
    move v2, v5

    .end local v5           #i:I
    .end local v13           #sum:F
    .local v2, ellipsisCount:I
    :goto_55
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    move-object v15, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move/from16 v16, v0

    mul-int v16, v16, p8

    add-int/lit8 v16, v16, 0x3

    aput v3, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    move-object v15, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move/from16 v16, v0

    mul-int v16, v16, p8

    add-int/lit8 v16, v16, 0x4

    aput v2, v15, v16

    goto :goto_2c

    .end local v2           #ellipsisCount:I
    .end local v3           #ellipsisStart:I
    .restart local v5       #i:I
    .restart local v13       #sum:F
    .restart local v14       #w:F
    :cond_78
    add-float/2addr v13, v14

    add-int/lit8 v5, v5, -0x1

    goto :goto_3f

    .end local v5           #i:I
    .end local v13           #sum:F
    .end local v14           #w:F
    :cond_7c
    sget-object v15, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p7

    move-object v1, v15

    if-eq v0, v1, :cond_8a

    sget-object v15, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p7

    move-object v1, v15

    if-ne v0, v1, :cond_a5

    :cond_8a
    const/4 v13, 0x0

    .restart local v13       #sum:F
    const/4 v5, 0x0

    .restart local v5       #i:I
    :goto_8c
    if-ge v5, v8, :cond_9d

    add-int v15, v5, p1

    sub-int v15, v15, p4

    add-int v15, v15, p5

    aget v14, p3, v15

    .restart local v14       #w:F
    add-float v15, v14, v13

    add-float/2addr v15, v4

    cmpl-float v15, v15, p6

    if-lez v15, :cond_a1

    .end local v14           #w:F
    :cond_9d
    move v3, v5

    .restart local v3       #ellipsisStart:I
    sub-int v2, v8, v5

    .restart local v2       #ellipsisCount:I
    goto :goto_55

    .end local v2           #ellipsisCount:I
    .end local v3           #ellipsisStart:I
    .restart local v14       #w:F
    :cond_a1
    add-float/2addr v13, v14

    add-int/lit8 v5, v5, 0x1

    goto :goto_8c

    .end local v5           #i:I
    .end local v13           #sum:F
    .end local v14           #w:F
    :cond_a5
    const/4 v9, 0x0

    .local v9, lsum:F
    const/4 v12, 0x0

    .local v12, rsum:F
    const/4 v7, 0x0

    .local v7, left:I
    move v11, v8

    .local v11, right:I
    sub-float v15, p6, v4

    const/high16 v16, 0x4000

    div-float v10, v15, v16

    .local v10, ravail:F
    move v11, v8

    :goto_b0
    if-ltz v11, :cond_c3

    const/4 v15, 0x1

    sub-int v15, v11, v15

    add-int v15, v15, p1

    sub-int v15, v15, p4

    add-int v15, v15, p5

    aget v14, p3, v15

    .restart local v14       #w:F
    add-float v15, v14, v12

    cmpl-float v15, v15, v10

    if-lez v15, :cond_dd

    .end local v14           #w:F
    :cond_c3
    sub-float v15, p6, v4

    sub-float v6, v15, v12

    .local v6, lavail:F
    const/4 v7, 0x0

    :goto_c8
    if-ge v7, v11, :cond_d8

    add-int v15, v7, p1

    sub-int v15, v15, p4

    add-int v15, v15, p5

    aget v14, p3, v15

    .restart local v14       #w:F
    add-float v15, v14, v9

    cmpl-float v15, v15, v6

    if-lez v15, :cond_e1

    .end local v14           #w:F
    :cond_d8
    move v3, v7

    .restart local v3       #ellipsisStart:I
    sub-int v2, v11, v7

    .restart local v2       #ellipsisCount:I
    goto/16 :goto_55

    .end local v2           #ellipsisCount:I
    .end local v3           #ellipsisStart:I
    .end local v6           #lavail:F
    .restart local v14       #w:F
    :cond_dd
    add-float/2addr v12, v14

    add-int/lit8 v11, v11, -0x1

    goto :goto_b0

    .restart local v6       #lavail:F
    :cond_e1
    add-float/2addr v9, v14

    add-int/lit8 v7, v7, 0x1

    goto :goto_c8
.end method

.method private static getFit(Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/CharSequence;IIF)I
    .registers 16
    .parameter "paint"
    .parameter "workPaint"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "wid"

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    add-int/lit8 v8, p4, 0x1

    .local v8, high:I
    sub-int v9, p3, v6

    .local v9, low:I
    :goto_6
    sub-int v0, v8, v9

    if-le v0, v6, :cond_1f

    add-int v0, v8, v9

    div-int/lit8 v4, v0, 0x2

    .local v4, guess:I
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v7, v5

    invoke-static/range {v0 .. v7}, Landroid/text/StaticLayout;->measureText(Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;Z[Ljava/lang/Object;)F

    move-result v0

    cmpl-float v0, v0, p5

    if-lez v0, :cond_1d

    move v8, v4

    goto :goto_6

    :cond_1d
    move v9, v4

    goto :goto_6

    .end local v4           #guess:I
    :cond_1f
    if-ge v9, p3, :cond_23

    move v0, p3

    :goto_22
    return v0

    :cond_23
    move v0, v9

    goto :goto_22
.end method

.method private static final isIdeographic(CZ)Z
    .registers 5
    .parameter "c"
    .parameter "includeNonStarters"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/16 v0, 0x2e80

    if-lt p0, v0, :cond_c

    const/16 v0, 0x2fff

    if-gt p0, v0, :cond_c

    move v0, v1

    :goto_b
    return v0

    :cond_c
    const/16 v0, 0x3000

    if-ne p0, v0, :cond_12

    move v0, v1

    goto :goto_b

    :cond_12
    const/16 v0, 0x3040

    if-lt p0, v0, :cond_23

    const/16 v0, 0x309f

    if-gt p0, v0, :cond_23

    if-nez p1, :cond_1f

    sparse-switch p0, :sswitch_data_88

    :cond_1f
    move v0, v1

    goto :goto_b

    :sswitch_21
    move v0, v2

    goto :goto_b

    :cond_23
    const/16 v0, 0x30a0

    if-lt p0, v0, :cond_34

    const/16 v0, 0x30ff

    if-gt p0, v0, :cond_34

    if-nez p1, :cond_30

    sparse-switch p0, :sswitch_data_ca

    :cond_30
    move v0, v1

    goto :goto_b

    :sswitch_32
    move v0, v2

    goto :goto_b

    :cond_34
    const/16 v0, 0x3400

    if-lt p0, v0, :cond_3e

    const/16 v0, 0x4db5

    if-gt p0, v0, :cond_3e

    move v0, v1

    goto :goto_b

    :cond_3e
    const/16 v0, 0x4e00

    if-lt p0, v0, :cond_49

    const v0, 0x9fbb

    if-gt p0, v0, :cond_49

    move v0, v1

    goto :goto_b

    :cond_49
    const v0, 0xf900

    if-lt p0, v0, :cond_55

    const v0, 0xfad9

    if-gt p0, v0, :cond_55

    move v0, v1

    goto :goto_b

    :cond_55
    const v0, 0xa000

    if-lt p0, v0, :cond_61

    const v0, 0xa48f

    if-gt p0, v0, :cond_61

    move v0, v1

    goto :goto_b

    :cond_61
    const v0, 0xa490

    if-lt p0, v0, :cond_6d

    const v0, 0xa4cf

    if-gt p0, v0, :cond_6d

    move v0, v1

    goto :goto_b

    :cond_6d
    const v0, 0xfe62

    if-lt p0, v0, :cond_79

    const v0, 0xfe66

    if-gt p0, v0, :cond_79

    move v0, v1

    goto :goto_b

    :cond_79
    const v0, 0xff10

    if-lt p0, v0, :cond_85

    const v0, 0xff19

    if-gt p0, v0, :cond_85

    move v0, v1

    goto :goto_b

    :cond_85
    move v0, v2

    goto :goto_b

    nop

    :sswitch_data_88
    .sparse-switch
        0x3041 -> :sswitch_21
        0x3043 -> :sswitch_21
        0x3045 -> :sswitch_21
        0x3047 -> :sswitch_21
        0x3049 -> :sswitch_21
        0x3063 -> :sswitch_21
        0x3083 -> :sswitch_21
        0x3085 -> :sswitch_21
        0x3087 -> :sswitch_21
        0x308e -> :sswitch_21
        0x3095 -> :sswitch_21
        0x3096 -> :sswitch_21
        0x309b -> :sswitch_21
        0x309c -> :sswitch_21
        0x309d -> :sswitch_21
        0x309e -> :sswitch_21
    .end sparse-switch

    :sswitch_data_ca
    .sparse-switch
        0x30a0 -> :sswitch_32
        0x30a1 -> :sswitch_32
        0x30a3 -> :sswitch_32
        0x30a5 -> :sswitch_32
        0x30a7 -> :sswitch_32
        0x30a9 -> :sswitch_32
        0x30c3 -> :sswitch_32
        0x30e3 -> :sswitch_32
        0x30e5 -> :sswitch_32
        0x30e7 -> :sswitch_32
        0x30ee -> :sswitch_32
        0x30f5 -> :sswitch_32
        0x30f6 -> :sswitch_32
        0x30fb -> :sswitch_32
        0x30fc -> :sswitch_32
        0x30fd -> :sswitch_32
        0x30fe -> :sswitch_32
    .end sparse-switch
.end method

.method private out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZI[BIZZZZ[FIILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;)I
    .registers 63
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "above"
    .parameter "below"
    .parameter "top"
    .parameter "bottom"
    .parameter "v"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "chooseht"
    .parameter "choosehtv"
    .parameter "fm"
    .parameter "tab"
    .parameter "needMultiply"
    .parameter "pstart"
    .parameter "chdirs"
    .parameter "dir"
    .parameter "easy"
    .parameter "last"
    .parameter "includepad"
    .parameter "trackpad"
    .parameter "widths"
    .parameter "widstart"
    .parameter "widoff"
    .parameter "ellipsize"
    .parameter "ellipsiswidth"
    .parameter "textwidth"
    .parameter "paint"

    .prologue
    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mLineCount:I

    move v13, v0

    .local v13, j:I
    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move v5, v0

    mul-int v31, v13, v5

    .local v31, off:I
    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move v5, v0

    add-int v5, v5, v31

    add-int/lit8 v32, v5, 0x1

    .local v32, want:I
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    move-object/from16 v29, v0

    .local v29, lines:[I
    move-object/from16 v0, v29

    array-length v0, v0

    move v5, v0

    move/from16 v0, v32

    move v1, v5

    if-lt v0, v1, :cond_6a

    add-int/lit8 v5, v32, 0x1

    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    move-result v30

    .local v30, nlen:I
    move/from16 v0, v30

    new-array v0, v0, [I

    move-object/from16 v22, v0

    .local v22, grow:[I
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v29

    array-length v0, v0

    move v7, v0

    move-object/from16 v0, v29

    move v1, v5

    move-object/from16 v2, v22

    move v3, v6

    move v4, v7

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mLines:[I

    move-object/from16 v29, v22

    move/from16 v0, v30

    new-array v0, v0, [Landroid/text/Layout$Directions;

    move-object/from16 v23, v0

    .local v23, grow2:[Landroid/text/Layout$Directions;
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    move-object v5, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    move-object v8, v0

    array-length v8, v8

    move-object v0, v5

    move v1, v6

    move-object/from16 v2, v23

    move v3, v7

    move v4, v8

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    .end local v22           #grow:[I
    .end local v23           #grow2:[Landroid/text/Layout$Directions;
    .end local v30           #nlen:I
    :cond_6a
    if-eqz p11, :cond_d7

    move/from16 v0, p4

    move-object/from16 v1, p13

    iput v0, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    move/from16 v0, p5

    move-object/from16 v1, p13

    iput v0, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    move/from16 v0, p6

    move-object/from16 v1, p13

    iput v0, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    move/from16 v0, p7

    move-object/from16 v1, p13

    iput v0, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    const/16 v25, 0x0

    .local v25, i:I
    :goto_86
    move-object/from16 v0, p11

    array-length v0, v0

    move v5, v0

    move/from16 v0, v25

    move v1, v5

    if-ge v0, v1, :cond_bf

    aget-object v5, p11, v25

    instance-of v5, v5, Landroid/text/style/LineHeightSpan$WithDensity;

    if-eqz v5, :cond_ad

    aget-object v5, p11, v25

    check-cast v5, Landroid/text/style/LineHeightSpan$WithDensity;

    aget v9, p12, v25

    move-object/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v10, p8

    move-object/from16 v11, p13

    move-object/from16 v12, p29

    invoke-interface/range {v5 .. v12}, Landroid/text/style/LineHeightSpan$WithDensity;->chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V

    :goto_aa
    add-int/lit8 v25, v25, 0x1

    goto :goto_86

    :cond_ad
    aget-object v5, p11, v25

    aget v9, p12, v25

    move-object/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v10, p8

    move-object/from16 v11, p13

    invoke-interface/range {v5 .. v11}, Landroid/text/style/LineHeightSpan;->chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;)V

    goto :goto_aa

    :cond_bf
    move-object/from16 v0, p13

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    move/from16 p4, v0

    move-object/from16 v0, p13

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    move/from16 p5, v0

    move-object/from16 v0, p13

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    move/from16 p6, v0

    move-object/from16 v0, p13

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    move/from16 p7, v0

    .end local v25           #i:I
    :cond_d7
    if-nez v13, :cond_e6

    if-eqz p22, :cond_e2

    sub-int v5, p6, p4

    move v0, v5

    move-object/from16 v1, p0

    iput v0, v1, Landroid/text/StaticLayout;->mTopPadding:I

    :cond_e2
    if-eqz p21, :cond_e6

    move/from16 p4, p6

    :cond_e6
    if-eqz p20, :cond_f5

    if-eqz p22, :cond_f1

    sub-int v5, p7, p5

    move v0, v5

    move-object/from16 v1, p0

    iput v0, v1, Landroid/text/StaticLayout;->mBottomPadding:I

    :cond_f1
    if-eqz p21, :cond_f5

    move/from16 p5, p7

    :cond_f5
    if-eqz p15, :cond_17e

    sub-int v5, p5, p4

    int-to-float v5, v5

    const/high16 v6, 0x3f80

    sub-float v6, p9, v6

    mul-float/2addr v5, v6

    add-float v5, v5, p10

    move v0, v5

    float-to-double v0, v0

    move-wide/from16 v19, v0

    .local v19, ex:D
    const-wide/16 v5, 0x0

    cmpl-double v5, v19, v5

    if-ltz v5, :cond_171

    const-wide/high16 v5, 0x3fe0

    add-double v5, v5, v19

    move-wide v0, v5

    double-to-int v0, v0

    move/from16 v21, v0

    .end local v19           #ex:D
    .local v21, extra:I
    :goto_113
    add-int/lit8 v5, v31, 0x0

    aput p2, v29, v5

    add-int/lit8 v5, v31, 0x1

    aput p8, v29, v5

    add-int/lit8 v5, v31, 0x2

    add-int v6, p5, v21

    aput v6, v29, v5

    sub-int v5, p5, p4

    add-int v5, v5, v21

    add-int p8, p8, v5

    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move v5, v0

    add-int v5, v5, v31

    add-int/lit8 v5, v5, 0x0

    aput p3, v29, v5

    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    move v5, v0

    add-int v5, v5, v31

    add-int/lit8 v5, v5, 0x1

    aput p8, v29, v5

    if-eqz p14, :cond_148

    add-int/lit8 v5, v31, 0x0

    aget v6, v29, v5

    const/high16 v7, 0x2000

    or-int/2addr v6, v7

    aput v6, v29, v5

    :cond_148
    add-int/lit8 v5, v31, 0x0

    aget v6, v29, v5

    shl-int/lit8 v7, p18, 0x1e

    or-int/2addr v6, v7

    aput v6, v29, v5

    const/16 v18, 0x0

    .local v18, cur:I
    const/16 v16, 0x0

    .local v16, count:I
    if-nez p19, :cond_181

    move/from16 v26, p2

    .local v26, k:I
    :goto_159
    move/from16 v0, v26

    move/from16 v1, p3

    if-ge v0, v1, :cond_181

    sub-int v5, v26, p16

    aget-byte v5, p17, v5

    move v0, v5

    move/from16 v1, v18

    if-eq v0, v1, :cond_16e

    add-int/lit8 v16, v16, 0x1

    sub-int v5, v26, p16

    aget-byte v18, p17, v5

    :cond_16e
    add-int/lit8 v26, v26, 0x1

    goto :goto_159

    .end local v16           #count:I
    .end local v18           #cur:I
    .end local v21           #extra:I
    .end local v26           #k:I
    .restart local v19       #ex:D
    :cond_171
    move-wide/from16 v0, v19

    neg-double v0, v0

    move-wide v5, v0

    const-wide/high16 v7, 0x3fe0

    add-double/2addr v5, v7

    double-to-int v5, v5

    move v0, v5

    neg-int v0, v0

    move/from16 v21, v0

    .restart local v21       #extra:I
    goto :goto_113

    .end local v19           #ex:D
    .end local v21           #extra:I
    :cond_17e
    const/16 v21, 0x0

    .restart local v21       #extra:I
    goto :goto_113

    .restart local v16       #count:I
    .restart local v18       #cur:I
    :cond_181
    if-nez v16, :cond_1bb

    sget-object v28, Landroid/text/StaticLayout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    .local v28, linedirs:Landroid/text/Layout$Directions;
    :goto_185
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    move-object v5, v0

    aput-object v28, v5, v13

    if-eqz p26, :cond_1ae

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p26

    move-object v1, v5

    if-ne v0, v1, :cond_197

    if-eqz v13, :cond_1ae

    :cond_197
    move-object/from16 v5, p0

    move/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v8, p23

    move/from16 v9, p24

    move/from16 v10, p25

    move/from16 v11, p27

    move-object/from16 v12, p26

    move/from16 v14, p28

    move-object/from16 v15, p29

    invoke-direct/range {v5 .. v15}, Landroid/text/StaticLayout;->calculateEllipsis(II[FIIFLandroid/text/TextUtils$TruncateAt;IFLandroid/text/TextPaint;)V

    :cond_1ae
    move-object/from16 v0, p0

    iget v0, v0, Landroid/text/StaticLayout;->mLineCount:I

    move v5, v0

    add-int/lit8 v5, v5, 0x1

    move v0, v5

    move-object/from16 v1, p0

    iput v0, v1, Landroid/text/StaticLayout;->mLineCount:I

    return p8

    .end local v28           #linedirs:Landroid/text/Layout$Directions;
    :cond_1bb
    add-int/lit8 v5, v16, 0x1

    move v0, v5

    new-array v0, v0, [S

    move-object/from16 v27, v0

    .local v27, ld:[S
    const/16 v18, 0x0

    const/16 v16, 0x0

    move/from16 v24, p2

    .local v24, here:I
    move/from16 v26, p2

    .restart local v26       #k:I
    move/from16 v17, v16

    .end local v16           #count:I
    .local v17, count:I
    :goto_1cc
    move/from16 v0, v26

    move/from16 v1, p3

    if-ge v0, v1, :cond_1ed

    sub-int v5, v26, p16

    aget-byte v5, p17, v5

    move v0, v5

    move/from16 v1, v18

    if-eq v0, v1, :cond_20f

    add-int/lit8 v16, v17, 0x1

    .end local v17           #count:I
    .restart local v16       #count:I
    sub-int v5, v26, v24

    int-to-short v5, v5

    aput-short v5, v27, v17

    sub-int v5, v26, p16

    aget-byte v18, p17, v5

    move/from16 v24, v26

    :goto_1e8
    add-int/lit8 v26, v26, 0x1

    move/from16 v17, v16

    .end local v16           #count:I
    .restart local v17       #count:I
    goto :goto_1cc

    :cond_1ed
    sub-int v5, p3, v24

    int-to-short v5, v5

    aput-short v5, v27, v17

    const/4 v5, 0x1

    move/from16 v0, v17

    move v1, v5

    if-ne v0, v1, :cond_202

    const/4 v5, 0x0

    aget-short v5, v27, v5

    if-nez v5, :cond_202

    sget-object v28, Landroid/text/StaticLayout;->DIRS_ALL_RIGHT_TO_LEFT:Landroid/text/Layout$Directions;

    .restart local v28       #linedirs:Landroid/text/Layout$Directions;
    move/from16 v16, v17

    .end local v17           #count:I
    .restart local v16       #count:I
    goto :goto_185

    .end local v16           #count:I
    .end local v28           #linedirs:Landroid/text/Layout$Directions;
    .restart local v17       #count:I
    :cond_202
    new-instance v28, Landroid/text/Layout$Directions;

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Landroid/text/Layout$Directions;-><init>([S)V

    .restart local v28       #linedirs:Landroid/text/Layout$Directions;
    move/from16 v16, v17

    .end local v17           #count:I
    .restart local v16       #count:I
    goto/16 :goto_185

    .end local v16           #count:I
    .end local v28           #linedirs:Landroid/text/Layout$Directions;
    .restart local v17       #count:I
    :cond_20f
    move/from16 v16, v17

    .end local v17           #count:I
    .restart local v16       #count:I
    goto :goto_1e8
.end method


# virtual methods
.method generate(Ljava/lang/CharSequence;IILandroid/text/TextPaint;IILandroid/text/Layout$Alignment;FFZZZFLandroid/text/TextUtils$TruncateAt;)V
    .registers 177
    .parameter "source" # p1
    .parameter "bufstart" # p2
    .parameter "bufend" # p3
    .parameter "paint" # p4
    .parameter "outerwidth" # p5
    .parameter "outerheight" # p6
    .parameter "align" # p7
    .parameter "spacingmult" # p8
    .parameter "spacingadd" # p9
    .parameter "includepad" # p10
    .parameter "trackpad" # p11
    .parameter "breakOnlyAtSpaces" # p12
    .parameter "ellipsizedWidth" # p13
    .parameter "where" # p14

    .prologue
    const/4 v5, 0x0

    move v0, v5

    move-object/from16 v1, p0

    iput v0, v1, Landroid/text/StaticLayout;->mLineCount:I

    const/16 v20, 0x0

    .local v20, v:I
    const/high16 v5, 0x3f80

    cmpl-float v5, p8, v5

    if-nez v5, :cond_13

    const/4 v5, 0x0

    cmpl-float v5, p9, v5

    if-eqz v5, :cond_d7

    :cond_13
    const/4 v5, 0x1

    move/from16 v27, v5

    .local v27, needMultiply:Z
    :goto_16
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

    move-object v11, v0

    .local v11, fm:Landroid/graphics/Paint$FontMetricsInt;
    const/16 v24, 0x0

    .local v24, choosehtv:[I
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move v1, v5

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CII)I

    move-result v141

    .local v141, end:I
    if-ltz v141, :cond_dc

    sub-int v5, v141, p2

    move/from16 v137, v5

    .local v137, bufsiz:I
    :goto_30
    const/16 v142, 0x1

    .local v142, first:Z
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mChdirs:[B

    move-object v5, v0

    if-nez v5, :cond_62

    add-int/lit8 v5, v137, 0x1

    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealByteArraySize(I)I

    move-result v5

    new-array v5, v5, [B

    move-object v0, v5

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mChdirs:[B

    add-int/lit8 v5, v137, 0x1

    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealCharArraySize(I)I

    move-result v5

    new-array v5, v5, [C

    move-object v0, v5

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mChs:[C

    add-int/lit8 v5, v137, 0x1

    mul-int/lit8 v5, v5, 0x2

    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    move-result v5

    new-array v5, v5, [F

    move-object v0, v5

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mWidths:[F

    :cond_62
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mChdirs:[B

    move-object/from16 v29, v0

    .local v29, chdirs:[B
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mChs:[C

    move-object/from16 v139, v0

    .local v139, chs:[C
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWidths:[F

    move-object v10, v0

    .local v10, widths:[F
    const/16 v132, 0x0

    .local v132, alter:Landroid/text/AlteredCharSequence;
    const/4 v7, 0x0

    .local v7, spanned:Landroid/text/Spanned;
    move-object/from16 v0, p1

    instance-of v0, v0, Landroid/text/Spanned;

    move v5, v0

    if-eqz v5, :cond_82

    move-object/from16 v0, p1

    check-cast v0, Landroid/text/Spanned;

    move-object v7, v0

    :cond_82
    const/16 v117, 0x1

    .local v117, DEFAULT_DIR:I
    move/from16 v28, p2

    .local v28, start:I
    :goto_86
    move/from16 v0, v28

    move/from16 v1, p3

    if-gt v0, v1, :cond_6e6

    if-eqz v142, :cond_e2

    const/16 v142, 0x0

    :goto_90
    if-gez v141, :cond_f0

    move/from16 v141, p3

    :goto_94
    const/16 v143, 0x1

    .local v143, firstWidthLineCount:I
    move/from16 v144, p5

    .local v144, firstwidth:I
    move/from16 v153, p5

    .local v153, restwidth:I
    const/16 v23, 0x0

    .local v23, chooseht:[Landroid/text/style/LineHeightSpan;
    if-eqz v7, :cond_14a

    const-class v5, Landroid/text/style/LeadingMarginSpan;

    move-object v0, v7

    move/from16 v1, v28

    move/from16 v2, v141

    move-object v3, v5

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v154

    check-cast v154, [Landroid/text/style/LeadingMarginSpan;

    .local v154, sp:[Landroid/text/style/LeadingMarginSpan;
    const/4 v8, 0x0

    .local v8, i:I
    :goto_ad
    move-object/from16 v0, v154

    array-length v0, v0

    move v5, v0

    if-ge v8, v5, :cond_f3

    aget-object v150, v154, v8

    .local v150, lms:Landroid/text/style/LeadingMarginSpan;
    aget-object v5, v154, v8

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Landroid/text/style/LeadingMarginSpan;->getLeadingMargin(Z)I

    move-result v5

    sub-int v144, v144, v5

    aget-object v5, v154, v8

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/text/style/LeadingMarginSpan;->getLeadingMargin(Z)I

    move-result v5

    sub-int v153, v153, v5

    move-object/from16 v0, v150

    instance-of v0, v0, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    move v5, v0

    if-eqz v5, :cond_d4

    check-cast v150, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    .end local v150           #lms:Landroid/text/style/LeadingMarginSpan;
    invoke-interface/range {v150 .. v150}, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;->getLeadingMarginLineCount()I

    move-result v143

    :cond_d4
    add-int/lit8 v8, v8, 0x1

    goto :goto_ad

    .end local v7           #spanned:Landroid/text/Spanned;
    .end local v8           #i:I
    .end local v10           #widths:[F
    .end local v11           #fm:Landroid/graphics/Paint$FontMetricsInt;
    .end local v23           #chooseht:[Landroid/text/style/LineHeightSpan;
    .end local v24           #choosehtv:[I
    .end local v27           #needMultiply:Z
    .end local v28           #start:I
    .end local v29           #chdirs:[B
    .end local v117           #DEFAULT_DIR:I
    .end local v132           #alter:Landroid/text/AlteredCharSequence;
    .end local v137           #bufsiz:I
    .end local v139           #chs:[C
    .end local v141           #end:I
    .end local v142           #first:Z
    .end local v143           #firstWidthLineCount:I
    .end local v144           #firstwidth:I
    .end local v153           #restwidth:I
    .end local v154           #sp:[Landroid/text/style/LeadingMarginSpan;
    :cond_d7
    const/4 v5, 0x0

    move/from16 v27, v5

    goto/16 :goto_16

    .restart local v11       #fm:Landroid/graphics/Paint$FontMetricsInt;
    .restart local v24       #choosehtv:[I
    .restart local v27       #needMultiply:Z
    .restart local v141       #end:I
    :cond_dc
    sub-int v5, p3, p2

    move/from16 v137, v5

    goto/16 :goto_30

    .restart local v7       #spanned:Landroid/text/Spanned;
    .restart local v10       #widths:[F
    .restart local v28       #start:I
    .restart local v29       #chdirs:[B
    .restart local v117       #DEFAULT_DIR:I
    .restart local v132       #alter:Landroid/text/AlteredCharSequence;
    .restart local v137       #bufsiz:I
    .restart local v139       #chs:[C
    .restart local v142       #first:Z
    :cond_e2
    const/16 v5, 0xa

    move-object/from16 v0, p1

    move v1, v5

    move/from16 v2, v28

    move/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CII)I

    move-result v141

    goto :goto_90

    :cond_f0
    add-int/lit8 v141, v141, 0x1

    goto :goto_94

    .restart local v8       #i:I
    .restart local v23       #chooseht:[Landroid/text/style/LineHeightSpan;
    .restart local v143       #firstWidthLineCount:I
    .restart local v144       #firstwidth:I
    .restart local v153       #restwidth:I
    .restart local v154       #sp:[Landroid/text/style/LeadingMarginSpan;
    :cond_f3
    const-class v5, Landroid/text/style/LineHeightSpan;

    move-object v0, v7

    move/from16 v1, v28

    move/from16 v2, v141

    move-object v3, v5

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v23

    .end local v23           #chooseht:[Landroid/text/style/LineHeightSpan;
    check-cast v23, [Landroid/text/style/LineHeightSpan;

    .restart local v23       #chooseht:[Landroid/text/style/LineHeightSpan;
    move-object/from16 v0, v23

    array-length v0, v0

    move v5, v0

    if-eqz v5, :cond_14a

    if-eqz v24, :cond_113

    move-object/from16 v0, v24

    array-length v0, v0

    move v5, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move v6, v0

    if-ge v5, v6, :cond_120

    :cond_113
    move-object/from16 v0, v23

    array-length v0, v0

    move v5, v0

    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    move-result v5

    move v0, v5

    new-array v0, v0, [I

    move-object/from16 v24, v0

    :cond_120
    const/4 v8, 0x0

    :goto_121
    move-object/from16 v0, v23

    array-length v0, v0

    move v5, v0

    if-ge v8, v5, :cond_14a

    aget-object v5, v23, v8

    invoke-interface {v7, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v152

    .local v152, o:I
    move/from16 v0, v152

    move/from16 v1, v28

    if-ge v0, v1, :cond_147

    move-object/from16 v0, p0

    move/from16 v1, v152

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineForOffset(I)I

    move-result v5

    move-object/from16 v0, p0

    move v1, v5

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v5

    aput v5, v24, v8

    :goto_144
    add-int/lit8 v8, v8, 0x1

    goto :goto_121

    :cond_147
    aput v20, v24, v8

    goto :goto_144

    .end local v8           #i:I
    .end local v152           #o:I
    .end local v154           #sp:[Landroid/text/style/LeadingMarginSpan;
    :cond_14a
    sub-int v5, v141, v28

    move-object/from16 v0, v29

    array-length v0, v0

    move v6, v0

    if-le v5, v6, :cond_163

    sub-int v5, v141, v28

    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealByteArraySize(I)I

    move-result v5

    move v0, v5

    new-array v0, v0, [B

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mChdirs:[B

    :cond_163
    sub-int v5, v141, v28

    move-object/from16 v0, v139

    array-length v0, v0

    move v6, v0

    if-le v5, v6, :cond_17c

    sub-int v5, v141, v28

    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealCharArraySize(I)I

    move-result v5

    move v0, v5

    new-array v0, v0, [C

    move-object/from16 v139, v0

    move-object/from16 v0, v139

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mChs:[C

    :cond_17c
    sub-int v5, v141, v28

    mul-int/lit8 v5, v5, 0x2

    array-length v6, v10

    if-le v5, v6, :cond_192

    sub-int v5, v141, v28

    mul-int/lit8 v5, v5, 0x2

    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    move-result v5

    new-array v10, v5, [F

    move-object v0, v10

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/text/StaticLayout;->mWidths:[F

    :cond_192
    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v141

    move-object/from16 v3, v139

    move v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    sub-int v151, v141, v28

    .local v151, n:I
    const/16 v31, 0x1

    .local v31, easy:Z
    const/16 v133, 0x0

    .local v133, altered:Z
    move/from16 v30, v117

    .local v30, dir:I
    const/4 v8, 0x0

    .restart local v8       #i:I
    :goto_1a8
    move v0, v8

    move/from16 v1, v151

    if-ge v0, v1, :cond_1b5

    aget-char v5, v139, v8

    const/16 v6, 0x590

    if-lt v5, v6, :cond_21f

    const/16 v31, 0x0

    :cond_1b5
    if-nez v31, :cond_1c7

    if-ltz v30, :cond_222

    const/4 v5, 0x2

    :goto_1ba
    const/4 v6, 0x0

    move v0, v5

    move-object/from16 v1, v139

    move-object/from16 v2, v29

    move/from16 v3, v151

    move v4, v6

    invoke-static {v0, v1, v2, v3, v4}, Landroid/text/StaticLayout;->bidi(I[C[BIZ)I

    move-result v30

    :cond_1c7
    const/4 v5, 0x1

    move/from16 v0, v30

    move v1, v5

    if-ne v0, v1, :cond_224

    const/4 v5, 0x0

    move/from16 v129, v5

    .local v129, SOR:B
    :goto_1d0
    move-object/from16 v0, p1

    instance-of v0, v0, Landroid/text/Spanned;

    move v5, v0

    if-eqz v5, :cond_22b

    move-object/from16 v0, p1

    check-cast v0, Landroid/text/Spanned;

    move-object/from16 v154, v0

    .local v154, sp:Landroid/text/Spanned;
    const-class v5, Landroid/text/style/ReplacementSpan;

    move-object/from16 v0, v154

    move/from16 v1, v28

    move/from16 v2, v141

    move-object v3, v5

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v155

    check-cast v155, [Landroid/text/style/ReplacementSpan;

    .local v155, spans:[Landroid/text/style/ReplacementSpan;
    const/16 v161, 0x0

    .local v161, y:I
    :goto_1ee
    move-object/from16 v0, v155

    array-length v0, v0

    move v5, v0

    move/from16 v0, v161

    move v1, v5

    if-ge v0, v1, :cond_22b

    aget-object v5, v155, v161

    move-object/from16 v0, v154

    move-object v1, v5

    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v130

    .local v130, a:I
    aget-object v5, v155, v161

    move-object/from16 v0, v154

    move-object v1, v5

    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v134

    .local v134, b:I
    move/from16 v160, v130

    .local v160, x:I
    :goto_20b
    move/from16 v0, v160

    move/from16 v1, v134

    if-ge v0, v1, :cond_228

    sub-int v5, v160, v28

    aput-byte v129, v29, v5

    sub-int v5, v160, v28

    const v6, 0xfffc

    aput-char v6, v139, v5

    add-int/lit8 v160, v160, 0x1

    goto :goto_20b

    .end local v129           #SOR:B
    .end local v130           #a:I
    .end local v134           #b:I
    .end local v154           #sp:Landroid/text/Spanned;
    .end local v155           #spans:[Landroid/text/style/ReplacementSpan;
    .end local v160           #x:I
    .end local v161           #y:I
    :cond_21f
    add-int/lit8 v8, v8, 0x1

    goto :goto_1a8

    :cond_222
    const/4 v5, -0x2

    goto :goto_1ba

    :cond_224
    const/4 v5, 0x1

    move/from16 v129, v5

    goto :goto_1d0

    .restart local v129       #SOR:B
    .restart local v130       #a:I
    .restart local v134       #b:I
    .restart local v154       #sp:Landroid/text/Spanned;
    .restart local v155       #spans:[Landroid/text/style/ReplacementSpan;
    .restart local v160       #x:I
    .restart local v161       #y:I
    :cond_228
    add-int/lit8 v161, v161, 0x1

    goto :goto_1ee

    .end local v130           #a:I
    .end local v134           #b:I
    .end local v154           #sp:Landroid/text/Spanned;
    .end local v155           #spans:[Landroid/text/style/ReplacementSpan;
    .end local v160           #x:I
    .end local v161           #y:I
    :cond_22b
    if-nez v31, :cond_25c

    const/4 v8, 0x0

    :goto_22e
    move v0, v8

    move/from16 v1, v151

    if-ge v0, v1, :cond_25c

    aget-byte v5, v29, v8

    const/4 v6, 0x1

    if-eq v5, v6, :cond_23d

    aget-byte v5, v29, v8

    const/4 v6, 0x2

    if-ne v5, v6, :cond_256

	:cond_23d
    move/from16 v149, v8

    .local v149, j:I
    :goto_23a
    move/from16 v0, v149

    move/from16 v1, v151

    if-ge v0, v1, :cond_245

    aget-byte v5, v29, v149

    const/4 v6, 0x1

    if-eq v5, v6, :cond_259

    aget-byte v5, v29, v149

    const/4 v6, 0x2

    if-eq v5, v6, :cond_259

    .line 280
    :cond_245
    sub-int v5, v149, v8

    move-object/from16 v0, v139

    move v1, v8

    move v2, v5

    invoke-static {v0, v1, v2}, Landroid/text/AndroidCharacter;->mirror([CII)Z

    move-result v5

    if-eqz v5, :cond_253

    const/16 v133, 0x1

    :cond_253
    const/4 v5, 0x1

    sub-int v8, v149, v5

    .end local v149           #j:I
    :cond_256
    add-int/lit8 v8, v8, 0x1

    goto :goto_22e

    .restart local v149       #j:I
    :cond_259
    add-int/lit8 v149, v149, 0x1

    goto :goto_23a

    .end local v149           #j:I
    :cond_25c
    if-eqz v133, :cond_3dd

    if-nez v132, :cond_3d0

    move-object/from16 v0, p1

    move-object/from16 v1, v139

    move/from16 v2, v28

    move/from16 v3, v141

    invoke-static {v0, v1, v2, v3}, Landroid/text/AlteredCharSequence;->make(Ljava/lang/CharSequence;[CII)Landroid/text/AlteredCharSequence;

    move-result-object v132

    :goto_26c
    move-object/from16 v156, v132

    .local v156, sub:Ljava/lang/CharSequence;
    :goto_26e
    move/from16 v159, v144

    .local v159, width:I
    const/16 v98, 0x0

    .local v98, w:F
    move/from16 v14, v28

    .local v14, here:I
    move/from16 v15, v28

    .local v15, ok:I
    move/from16 v40, v98

    .local v40, okwidth:F
    const/16 v16, 0x0

    .local v16, okascent:I
    const/16 v17, 0x0

    .local v17, okdescent:I
    const/16 v18, 0x0

    .local v18, oktop:I
    const/16 v19, 0x0

    .local v19, okbottom:I
    move/from16 v44, v28

    .local v44, fit:I
    move/from16 v69, v98

    .local v69, fitwidth:F
    const/16 v45, 0x0

    .local v45, fitascent:I
    const/16 v46, 0x0

    .local v46, fitdescent:I
    const/16 v47, 0x0

    .local v47, fittop:I
    const/16 v48, 0x0

    .local v48, fitbottom:I
    const/16 v26, 0x0

    .local v26, tab:Z
    move/from16 v8, v28

    :goto_290
    move v0, v8

    move/from16 v1, v141

    if-ge v0, v1, :cond_672

    if-nez v7, :cond_3e1

    move/from16 v9, v141

    .local v9, next:I
    :goto_299
    if-nez v7, :cond_3ee

    move-object/from16 v0, p4

    move-object/from16 v1, v156

    move v2, v8

    move v3, v9

    move-object v4, v10

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/CharSequence;II[F)I

    move-result v131

    .local v131, actualNum:I
    sub-int v5, v9, v8

    move v0, v5

    move/from16 v1, v131

    if-le v0, v1, :cond_2b7

    move-object v0, v10

    move-object/from16 v1, v156

    move v2, v8

    move v3, v9

    move/from16 v4, v131

    invoke-static {v0, v1, v2, v3, v4}, Landroid/text/StaticLayout;->adjustTextWidths([FLjava/lang/CharSequence;III)V

    :cond_2b7
    const/4 v5, 0x0

    sub-int v6, v141, v28

    sub-int v12, v8, v28

    add-int/2addr v6, v12

    sub-int v12, v9, v8

    invoke-static {v10, v5, v10, v6, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p4

    move-object v1, v11

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    :goto_2c8
    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    move/from16 v148, v0

    .local v148, fmtop:I
    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    move/from16 v146, v0

    .local v146, fmbottom:I
    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    move/from16 v145, v0

    .local v145, fmascent:I
    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    move/from16 v147, v0

    .local v147, fmdescent:I
    move/from16 v149, v8

    .restart local v149       #j:I
    :goto_2de
    move/from16 v0, v149

    move v1, v9

    if-ge v0, v1, :cond_66f

    sub-int v5, v149, v28

    aget-char v138, v139, v5

    .local v138, c:C
    move/from16 v135, v98

    .local v135, before:F
    const/16 v5, 0xa

    move/from16 v0, v138

    move v1, v5

    if-ne v0, v1, :cond_459

    :goto_2f0
    move/from16 v0, v159

    int-to-float v0, v0

    move v5, v0

    cmpg-float v5, v98, v5

    if-gtz v5, :cond_4ee

    move/from16 v69, v98

    add-int/lit8 v44, v149, 0x1

    move/from16 v0, v148

    move/from16 v1, v47

    if-ge v0, v1, :cond_304

    move/from16 v47, v148

    :cond_304
    move/from16 v0, v145

    move/from16 v1, v45

    if-ge v0, v1, :cond_30c

    move/from16 v45, v145

    :cond_30c
    move/from16 v0, v147

    move/from16 v1, v46

    if-le v0, v1, :cond_314

    move/from16 v46, v147

    :cond_314
    move/from16 v0, v146

    move/from16 v1, v48

    if-le v0, v1, :cond_31c

    move/from16 v48, v146

    :cond_31c
    const/16 v5, 0x20

    move/from16 v0, v138

    move v1, v5

    if-eq v0, v1, :cond_3a8

    const/16 v5, 0x9

    move/from16 v0, v138

    move v1, v5

    if-eq v0, v1, :cond_3a8

    const/16 v5, 0x2e

    move/from16 v0, v138

    move v1, v5

    if-eq v0, v1, :cond_346

    const/16 v5, 0x2c

    move/from16 v0, v138

    move v1, v5

    if-eq v0, v1, :cond_346

    const/16 v5, 0x3a

    move/from16 v0, v138

    move v1, v5

    if-eq v0, v1, :cond_346

    const/16 v5, 0x3b

    move/from16 v0, v138

    move v1, v5

    if-ne v0, v1, :cond_368

    :cond_346
    const/4 v5, 0x1

    sub-int v5, v149, v5

    if-lt v5, v14, :cond_358

    const/4 v5, 0x1

    sub-int v5, v149, v5

    sub-int v5, v5, v28

    aget-char v5, v139, v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-nez v5, :cond_368

    :cond_358
    add-int/lit8 v5, v149, 0x1

    if-ge v5, v9, :cond_3a8

    add-int/lit8 v5, v149, 0x1

    sub-int v5, v5, v28

    aget-char v5, v139, v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_3a8

    :cond_368
    const/16 v5, 0x2f

    move/from16 v0, v138

    move v1, v5

    if-eq v0, v1, :cond_376

    const/16 v5, 0x2d

    move/from16 v0, v138

    move v1, v5

    if-ne v0, v1, :cond_386

    :cond_376
    add-int/lit8 v5, v149, 0x1

    if-ge v5, v9, :cond_3a8

    add-int/lit8 v5, v149, 0x1

    sub-int v5, v5, v28

    aget-char v5, v139, v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_3a8

    :cond_386
    const/16 v5, 0x2e80

    move/from16 v0, v138

    move v1, v5

    if-lt v0, v1, :cond_3cc

    const/4 v5, 0x1

    move/from16 v0, v138

    move v1, v5

    invoke-static {v0, v1}, Landroid/text/StaticLayout;->isIdeographic(CZ)Z

    move-result v5

    if-eqz v5, :cond_3cc

    add-int/lit8 v5, v149, 0x1

    if-ge v5, v9, :cond_3cc

    add-int/lit8 v5, v149, 0x1

    sub-int v5, v5, v28

    aget-char v5, v139, v5

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/text/StaticLayout;->isIdeographic(CZ)Z

    move-result v5

    if-eqz v5, :cond_3cc

    :cond_3a8
    move/from16 v40, v98

    add-int/lit8 v15, v149, 0x1

    move/from16 v0, v47

    move/from16 v1, v18

    if-ge v0, v1, :cond_3b4

    move/from16 v18, v47

    :cond_3b4
    move/from16 v0, v45

    move/from16 v1, v16

    if-ge v0, v1, :cond_3bc

    move/from16 v16, v45

    :cond_3bc
    move/from16 v0, v46

    move/from16 v1, v17

    if-le v0, v1, :cond_3c4

    move/from16 v17, v46

    :cond_3c4
    move/from16 v0, v48

    move/from16 v1, v19

    if-le v0, v1, :cond_3cc

    move/from16 v19, v48

    :cond_3cc
    :goto_3cc
    add-int/lit8 v149, v149, 0x1

    goto/16 :goto_2de

    .end local v9           #next:I
    .end local v14           #here:I
    .end local v15           #ok:I
    .end local v16           #okascent:I
    .end local v17           #okdescent:I
    .end local v18           #oktop:I
    .end local v19           #okbottom:I
    .end local v26           #tab:Z
    .end local v40           #okwidth:F
    .end local v44           #fit:I
    .end local v45           #fitascent:I
    .end local v46           #fitdescent:I
    .end local v47           #fittop:I
    .end local v48           #fitbottom:I
    .end local v69           #fitwidth:F
    .end local v98           #w:F
    .end local v131           #actualNum:I
    .end local v135           #before:F
    .end local v138           #c:C
    .end local v145           #fmascent:I
    .end local v146           #fmbottom:I
    .end local v147           #fmdescent:I
    .end local v148           #fmtop:I
    .end local v149           #j:I
    .end local v156           #sub:Ljava/lang/CharSequence;
    .end local v159           #width:I
    :cond_3d0
    move-object/from16 v0, v132

    move-object/from16 v1, v139

    move/from16 v2, v28

    move/from16 v3, v141

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/AlteredCharSequence;->update([CII)V

    goto/16 :goto_26c

    :cond_3dd
    move-object/from16 v156, p1

    .restart local v156       #sub:Ljava/lang/CharSequence;
    goto/16 :goto_26e

    .restart local v14       #here:I
    .restart local v15       #ok:I
    .restart local v16       #okascent:I
    .restart local v17       #okdescent:I
    .restart local v18       #oktop:I
    .restart local v19       #okbottom:I
    .restart local v26       #tab:Z
    .restart local v40       #okwidth:F
    .restart local v44       #fit:I
    .restart local v45       #fitascent:I
    .restart local v46       #fitdescent:I
    .restart local v47       #fittop:I
    .restart local v48       #fitbottom:I
    .restart local v69       #fitwidth:F
    .restart local v98       #w:F
    .restart local v159       #width:I
    :cond_3e1
    const-class v5, Landroid/text/style/MetricAffectingSpan;

    move-object v0, v7

    move v1, v8

    move/from16 v2, v141

    move-object v3, v5

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v9

    .restart local v9       #next:I
    goto/16 :goto_299

    :cond_3ee
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWorkPaint:Landroid/text/TextPaint;

    move-object v5, v0

    const/4 v6, 0x0

    iput v6, v5, Landroid/text/TextPaint;->baselineShift:I

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWorkPaint:Landroid/text/TextPaint;

    move-object v6, v0

    move-object/from16 v5, p4

    invoke-static/range {v5 .. v11}, Landroid/text/Styled;->getTextWidths(Landroid/text/TextPaint;Landroid/text/TextPaint;Landroid/text/Spanned;II[FLandroid/graphics/Paint$FontMetricsInt;)I

    move-result v131

    .restart local v131       #actualNum:I
    sub-int v5, v9, v8

    move v0, v5

    move/from16 v1, v131

    if-le v0, v1, :cond_411

    move-object v0, v10

    move-object v1, v7

    move v2, v8

    move v3, v9

    move/from16 v4, v131

    invoke-static {v0, v1, v2, v3, v4}, Landroid/text/StaticLayout;->adjustTextWidths([FLjava/lang/CharSequence;III)V

    :cond_411
    const/4 v5, 0x0

    sub-int v6, v141, v28

    sub-int v12, v8, v28

    add-int/2addr v6, v12

    sub-int v12, v9, v8

    invoke-static {v10, v5, v10, v6, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWorkPaint:Landroid/text/TextPaint;

    move-object v5, v0

    iget v5, v5, Landroid/text/TextPaint;->baselineShift:I

    if-gez v5, :cond_43f

    iget v5, v11, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWorkPaint:Landroid/text/TextPaint;

    move-object v6, v0

    iget v6, v6, Landroid/text/TextPaint;->baselineShift:I

    add-int/2addr v5, v6

    iput v5, v11, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v5, v11, Landroid/graphics/Paint$FontMetricsInt;->top:I

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWorkPaint:Landroid/text/TextPaint;

    move-object v6, v0

    iget v6, v6, Landroid/text/TextPaint;->baselineShift:I

    add-int/2addr v5, v6

    iput v5, v11, Landroid/graphics/Paint$FontMetricsInt;->top:I

    goto/16 :goto_2c8

    :cond_43f
    iget v5, v11, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWorkPaint:Landroid/text/TextPaint;

    move-object v6, v0

    iget v6, v6, Landroid/text/TextPaint;->baselineShift:I

    add-int/2addr v5, v6

    iput v5, v11, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v5, v11, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWorkPaint:Landroid/text/TextPaint;

    move-object v6, v0

    iget v6, v6, Landroid/text/TextPaint;->baselineShift:I

    add-int/2addr v5, v6

    iput v5, v11, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    goto/16 :goto_2c8

    .restart local v135       #before:F
    .restart local v138       #c:C
    .restart local v145       #fmascent:I
    .restart local v146       #fmbottom:I
    .restart local v147       #fmdescent:I
    .restart local v148       #fmtop:I
    .restart local v149       #j:I
    :cond_459
    const/16 v5, 0x9

    move/from16 v0, v138

    move v1, v5

    if-ne v0, v1, :cond_472

    const/4 v5, 0x0

    move-object/from16 v0, v156

    move/from16 v1, v28

    move/from16 v2, v141

    move/from16 v3, v98

    move-object v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Landroid/text/Layout;->nextTab(Ljava/lang/CharSequence;IIF[Ljava/lang/Object;)F

    move-result v98

    const/16 v26, 0x1

    goto/16 :goto_2f0

    :cond_472
    invoke-static/range {v138 .. v138}, Lmiui/text/util/EmojiSmileys;->isEmoji(I)Z

    move-result v5

    if-eqz v5, :cond_41f

    if-nez v7, :cond_452

    move-object/from16 v157, p4

    .local v157, whichPaint:Landroid/graphics/Paint;
    :goto_442
    invoke-virtual/range {v157 .. v157}, Landroid/graphics/Paint;->descent()F

    move-result v5

    invoke-virtual/range {v157 .. v157}, Landroid/graphics/Paint;->ascent()F

    move-result v6

    sub-float/2addr v5, v6

    sget v6, Landroid/text/StaticLayout;->EMOJI_PADDING_PX:I

    mul-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    add-float v154, v5, v6

    .local v154, wid:F
    add-float v98, v98, v154

    const/16 v26, 0x1

    goto/16 :goto_2f0

    .end local v157           #whichPaint:Landroid/graphics/Paint;
    .end local v154           #wid:F
    :cond_452
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWorkPaint:Landroid/text/TextPaint;

    move-object/from16 v157, v0

    .restart local v157       #whichPaint:Landroid/graphics/Paint;
    goto :goto_442

    .end local v157           #whichPaint:Landroid/graphics/Paint;
    :cond_41f
    sub-int v5, v149, v28

    sub-int v6, v141, v28

    add-int/2addr v5, v6

    aget v5, v10, v5

    add-float v98, v98, v5

    goto/16 :goto_2f0

    :cond_4ee
    if-eqz p12, :cond_552

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p14

    move-object v1, v5

    if-eq v0, v1, :cond_552

    if-nez p6, :cond_552

    if-eq v15, v14, :cond_52c

    :goto_4f2
    if-ge v15, v9, :cond_4ff

    sub-int v5, v15, v28

    aget-char v5, v139, v5

    const/16 v6, 0x20

    if-ne v5, v6, :cond_4ff

    add-int/lit8 v15, v15, 0x1

    goto :goto_4f2

    :cond_4ff
    move v0, v15

    move/from16 v1, p3

    if-ne v0, v1, :cond_528

    const/4 v5, 0x1

    move/from16 v32, v5

    :goto_507
    sub-int v37, v141, v28

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move/from16 v21, p8

    move/from16 v22, p9

    move-object/from16 v25, v11

    move/from16 v33, p10

    move/from16 v34, p11

    move-object/from16 v35, v10

    move/from16 v36, v28

    move-object/from16 v38, p14

    move/from16 v39, p13

    move-object/from16 v41, p4

    invoke-direct/range {v12 .. v41}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZI[BIZZZZ[FIILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;)I

    move-result v20

    move v14, v15

    goto/16 :goto_3cc

    :cond_528
    const/4 v5, 0x0

    move/from16 v32, v5

    goto :goto_507

    :cond_52c
    move/from16 v69, v98

    add-int/lit8 v44, v149, 0x1

    move/from16 v0, v148

    move/from16 v1, v47

    if-ge v0, v1, :cond_538

    move/from16 v47, v148

    :cond_538
    move/from16 v0, v145

    move/from16 v1, v45

    if-ge v0, v1, :cond_540

    move/from16 v45, v145

    :cond_540
    move/from16 v0, v147

    move/from16 v1, v46

    if-le v0, v1, :cond_548

    move/from16 v46, v147

    :cond_548
    move/from16 v0, v146

    move/from16 v1, v48

    if-le v0, v1, :cond_3cc

    move/from16 v48, v146

    goto/16 :goto_3cc

    :cond_552
    if-eq v15, v14, :cond_5ae

    :goto_554
    if-ge v15, v9, :cond_561

    sub-int v5, v15, v28

    aget-char v5, v139, v5

    const/16 v6, 0x20

    if-ne v5, v6, :cond_561

    add-int/lit8 v15, v15, 0x1

    goto :goto_554

    :cond_561
    move v0, v15

    move/from16 v1, p3

    if-ne v0, v1, :cond_5aa

    const/4 v5, 0x1

    move/from16 v32, v5

    :goto_569
    sub-int v37, v141, v28

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move/from16 v21, p8

    move/from16 v22, p9

    move-object/from16 v25, v11

    move/from16 v33, p10

    move/from16 v34, p11

    move-object/from16 v35, v10

    move/from16 v36, v28

    move-object/from16 v38, p14

    move/from16 v39, p13

    move-object/from16 v41, p4

    invoke-direct/range {v12 .. v41}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZI[BIZZZZ[FIILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;)I

    move-result v20

    move v14, v15

    .end local v98           #w:F
    :goto_588
    if-ge v14, v8, :cond_66a

    move v9, v14

    move/from16 v149, v14

    :goto_58d
    move/from16 v44, v14

    move v15, v14

    const/16 v98, 0x0

    .restart local v98       #w:F
    const/16 v48, 0x0

    move/from16 v47, v48

    move/from16 v46, v48

    move/from16 v45, v48

    const/16 v19, 0x0

    move/from16 v18, v19

    move/from16 v17, v19

    move/from16 v16, v19

    add-int/lit8 v143, v143, -0x1

    if-gtz v143, :cond_3cc

    move/from16 v159, v153

    goto/16 :goto_3cc

    :cond_5aa
    const/4 v5, 0x0

    move/from16 v32, v5

    goto :goto_569

    :cond_5ae
    move/from16 v0, v44

    move v1, v14

    if-eq v0, v1, :cond_5f5

    move/from16 v0, v44

    move/from16 v1, p3

    if-ne v0, v1, :cond_5f1

    const/4 v5, 0x1

    move/from16 v61, v5

    :goto_5bc
    sub-int v66, v141, v28

    move-object/from16 v41, p0

    move-object/from16 v42, p1

    move/from16 v43, v14

    move/from16 v49, v20

    move/from16 v50, p8

    move/from16 v51, p9

    move-object/from16 v52, v23

    move-object/from16 v53, v24

    move-object/from16 v54, v11

    move/from16 v55, v26

    move/from16 v56, v27

    move/from16 v57, v28

    move-object/from16 v58, v29

    move/from16 v59, v30

    move/from16 v60, v31

    move/from16 v62, p10

    move/from16 v63, p11

    move-object/from16 v64, v10

    move/from16 v65, v28

    move-object/from16 v67, p14

    move/from16 v68, p13

    move-object/from16 v70, p4

    invoke-direct/range {v41 .. v70}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZI[BIZZZZ[FIILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;)I

    move-result v20

    move/from16 v14, v44

    goto :goto_588

    :cond_5f1
    const/4 v5, 0x0

    move/from16 v61, v5

    goto :goto_5bc

    :cond_5f5
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/text/StaticLayout;->mWorkPaint:Landroid/text/TextPaint;

    move-object/from16 v33, v0

    add-int/lit8 v36, v14, 0x1

    const/16 v39, 0x0

    move-object/from16 v32, p4

    move-object/from16 v34, p1

    move/from16 v35, v14

    move-object/from16 v37, v11

    move/from16 v38, v26

    invoke-static/range {v32 .. v39}, Landroid/text/StaticLayout;->measureText(Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;Z[Ljava/lang/Object;)F

    add-int/lit8 v73, v14, 0x1

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    move/from16 v74, v0

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    move/from16 v75, v0

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    move/from16 v76, v0

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    move/from16 v77, v0

    add-int/lit8 v5, v14, 0x1

    move v0, v5

    move/from16 v1, p3

    if-ne v0, v1, :cond_666

    const/4 v5, 0x1

    move/from16 v90, v5

    :goto_62c
    sub-int v95, v141, v28

    sub-int v5, v14, v28

    aget v98, v10, v5

    .end local v98           #w:F
    move-object/from16 v70, p0

    move-object/from16 v71, p1

    move/from16 v72, v14

    move/from16 v78, v20

    move/from16 v79, p8

    move/from16 v80, p9

    move-object/from16 v81, v23

    move-object/from16 v82, v24

    move-object/from16 v83, v11

    move/from16 v84, v26

    move/from16 v85, v27

    move/from16 v86, v28

    move-object/from16 v87, v29

    move/from16 v88, v30

    move/from16 v89, v31

    move/from16 v91, p10

    move/from16 v92, p11

    move-object/from16 v93, v10

    move/from16 v94, v28

    move-object/from16 v96, p14

    move/from16 v97, p13

    move-object/from16 v99, p4

    invoke-direct/range {v70 .. v99}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZI[BIZZZZ[FIILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;)I

    move-result v20

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_588

    .restart local v98       #w:F
    :cond_666
    const/4 v5, 0x0

    move/from16 v90, v5

    goto :goto_62c

    .end local v98           #w:F
    :cond_66a
    const/4 v5, 0x1

    sub-int v149, v14, v5

    goto/16 :goto_58d

    .end local v135           #before:F
    .end local v138           #c:C
    .restart local v98       #w:F
    :cond_66f
    move v8, v9

    goto/16 :goto_290

    .end local v9           #next:I
    .end local v131           #actualNum:I
    .end local v145           #fmascent:I
    .end local v146           #fmbottom:I
    .end local v147           #fmdescent:I
    .end local v148           #fmtop:I
    .end local v149           #j:I
    :cond_672
    move/from16 v0, v141

    move v1, v14

    if-eq v0, v1, :cond_6de

    or-int v5, v47, v48

    or-int v5, v5, v46

    or-int v5, v5, v45

    if-nez v5, :cond_699

    move-object/from16 v0, p4

    move-object v1, v11

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    move/from16 v47, v0

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    move/from16 v48, v0

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    move/from16 v45, v0

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    move/from16 v46, v0

    :cond_699
    move/from16 v0, v141

    move/from16 v1, p3

    if-ne v0, v1, :cond_74b

    const/4 v5, 0x1

    move/from16 v90, v5

    :goto_6a2
    sub-int v95, v141, v28

    move-object/from16 v70, p0

    move-object/from16 v71, p1

    move/from16 v72, v14

    move/from16 v73, v141

    move/from16 v74, v45

    move/from16 v75, v46

    move/from16 v76, v47

    move/from16 v77, v48

    move/from16 v78, v20

    move/from16 v79, p8

    move/from16 v80, p9

    move-object/from16 v81, v23

    move-object/from16 v82, v24

    move-object/from16 v83, v11

    move/from16 v84, v26

    move/from16 v85, v27

    move/from16 v86, v28

    move-object/from16 v87, v29

    move/from16 v88, v30

    move/from16 v89, v31

    move/from16 v91, p10

    move/from16 v92, p11

    move-object/from16 v93, v10

    move/from16 v94, v28

    move-object/from16 v96, p14

    move/from16 v97, p13

    move-object/from16 v99, p4

    invoke-direct/range {v70 .. v99}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZI[BIZZZZ[FIILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;)I

    move-result v20

    :cond_6de
    move/from16 v28, v141

    move/from16 v0, v141

    move/from16 v1, p3

    if-ne v0, v1, :cond_750

    .end local v8           #i:I
    .end local v14           #here:I
    .end local v15           #ok:I
    .end local v16           #okascent:I
    .end local v17           #okdescent:I
    .end local v18           #oktop:I
    .end local v19           #okbottom:I
    .end local v23           #chooseht:[Landroid/text/style/LineHeightSpan;
    .end local v26           #tab:Z
    .end local v30           #dir:I
    .end local v31           #easy:Z
    .end local v40           #okwidth:F
    .end local v44           #fit:I
    .end local v45           #fitascent:I
    .end local v46           #fitdescent:I
    .end local v47           #fittop:I
    .end local v48           #fitbottom:I
    .end local v69           #fitwidth:F
    .end local v98           #w:F
    .end local v129           #SOR:B
    .end local v133           #altered:Z
    .end local v143           #firstWidthLineCount:I
    .end local v144           #firstwidth:I
    .end local v151           #n:I
    .end local v153           #restwidth:I
    .end local v156           #sub:Ljava/lang/CharSequence;
    .end local v159           #width:I
    :cond_6e6
    move/from16 v0, p3

    move/from16 v1, p2

    if-eq v0, v1, :cond_6fa

    const/4 v5, 0x1

    sub-int v5, p3, v5

    move-object/from16 v0, p1

    move v1, v5

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    const/16 v6, 0xa

    if-ne v5, v6, :cond_74a

    :cond_6fa
    move-object/from16 v0, p4

    move-object v1, v11

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    move/from16 v103, v0

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    move/from16 v104, v0

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    move/from16 v105, v0

    move-object v0, v11

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    move/from16 v106, v0

    const/16 v110, 0x0

    const/16 v111, 0x0

    const/16 v113, 0x0

    const/16 v118, 0x1

    const/16 v119, 0x1

    const/16 v124, 0x0

    const/16 v127, 0x0

    move-object/from16 v99, p0

    move-object/from16 v100, p1

    move/from16 v101, p3

    move/from16 v102, p3

    move/from16 v107, v20

    move/from16 v108, p8

    move/from16 v109, p9

    move-object/from16 v112, v11

    move/from16 v114, v27

    move/from16 v115, p3

    move-object/from16 v116, v29

    move/from16 v120, p10

    move/from16 v121, p11

    move-object/from16 v122, v10

    move/from16 v123, p2

    move-object/from16 v125, p14

    move/from16 v126, p13

    move-object/from16 v128, p4

    invoke-direct/range {v99 .. v128}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZI[BIZZZZ[FIILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;)I

    move-result v20

    :cond_74a
    return-void

    .restart local v8       #i:I
    .restart local v14       #here:I
    .restart local v15       #ok:I
    .restart local v16       #okascent:I
    .restart local v17       #okdescent:I
    .restart local v18       #oktop:I
    .restart local v19       #okbottom:I
    .restart local v23       #chooseht:[Landroid/text/style/LineHeightSpan;
    .restart local v26       #tab:Z
    .restart local v30       #dir:I
    .restart local v31       #easy:Z
    .restart local v40       #okwidth:F
    .restart local v44       #fit:I
    .restart local v45       #fitascent:I
    .restart local v46       #fitdescent:I
    .restart local v47       #fittop:I
    .restart local v48       #fitbottom:I
    .restart local v69       #fitwidth:F
    .restart local v98       #w:F
    .restart local v129       #SOR:B
    .restart local v133       #altered:Z
    .restart local v143       #firstWidthLineCount:I
    .restart local v144       #firstwidth:I
    .restart local v151       #n:I
    .restart local v153       #restwidth:I
    .restart local v156       #sub:Ljava/lang/CharSequence;
    .restart local v159       #width:I
    :cond_74b
    const/4 v5, 0x0

    move/from16 v90, v5

    goto/16 :goto_6a2

    :cond_750
    move/from16 v28, v141

    goto/16 :goto_86
.end method

.method generate(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZZZFLandroid/text/TextUtils$TruncateAt;)V
    .registers 29
    .parameter "source"
    .parameter "bufstart"
    .parameter "bufend"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"
    .parameter "trackpad"
    .parameter "breakOnlyAtSpaces"
    .parameter "ellipsizedWidth"
    .parameter "where"

    .prologue

    const/4 v6, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    move-object/from16 v14, p13

    invoke-virtual/range {v0 .. v14}, Landroid/text/StaticLayout;->generate(Ljava/lang/CharSequence;IILandroid/text/TextPaint;IILandroid/text/Layout$Alignment;FFZZZFLandroid/text/TextUtils$TruncateAt;)V


    return-void
.end method

.method public getBottomPadding()I
    .registers 2

    .prologue
    iget v0, p0, Landroid/text/StaticLayout;->mBottomPadding:I

    return v0
.end method

.method public getEllipsisCount(I)I
    .registers 4
    .parameter "line"

    .prologue
    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_7

    const/4 v0, 0x0

    :goto_6
    return v0

    :cond_7
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x4

    aget v0, v0, v1

    goto :goto_6
.end method

.method public getEllipsisStart(I)I
    .registers 4
    .parameter "line"

    .prologue
    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_7

    const/4 v0, 0x0

    :goto_6
    return v0

    :cond_7
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x3

    aget v0, v0, v1

    goto :goto_6
.end method

.method public getEllipsizedWidth()I
    .registers 2

    .prologue
    iget v0, p0, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    return v0
.end method

.method public getLineContainsTab(I)Z
    .registers 4
    .parameter "line"

    .prologue
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x0

    aget v0, v0, v1

    const/high16 v1, 0x2000

    and-int/2addr v0, v1

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public getLineCount()I
    .registers 2

    .prologue
    iget v0, p0, Landroid/text/StaticLayout;->mLineCount:I

    return v0
.end method

.method public getLineDescent(I)I
    .registers 4
    .parameter "line"

    .prologue
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public final getLineDirections(I)Landroid/text/Layout$Directions;
    .registers 3
    .parameter "line"

    .prologue
    iget-object v0, p0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getLineForVertical(I)I
    .registers 8
    .parameter "vertical"

    .prologue
    iget v1, p0, Landroid/text/StaticLayout;->mLineCount:I

    .local v1, high:I
    const/4 v3, -0x1

    .local v3, low:I
    iget-object v2, p0, Landroid/text/StaticLayout;->mLines:[I

    .local v2, lines:[I
    :goto_5
    sub-int v4, v1, v3

    const/4 v5, 0x1

    if-le v4, v5, :cond_1b

    add-int v4, v1, v3

    shr-int/lit8 v0, v4, 0x1

    .local v0, guess:I
    iget v4, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v4, v0

    add-int/lit8 v4, v4, 0x1

    aget v4, v2, v4

    if-le v4, p1, :cond_19

    move v1, v0

    goto :goto_5

    :cond_19
    move v3, v0

    goto :goto_5

    .end local v0           #guess:I
    :cond_1b
    if-gez v3, :cond_1f

    const/4 v4, 0x0

    :goto_1e
    return v4

    :cond_1f
    move v4, v3

    goto :goto_1e
.end method

.method public getLineStart(I)I
    .registers 4
    .parameter "line"

    .prologue
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x0

    aget v0, v0, v1

    const v1, 0x1fffffff

    and-int/2addr v0, v1

    return v0
.end method

.method public getLineTop(I)I
    .registers 4
    .parameter "line"

    .prologue
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public getParagraphDirection(I)I
    .registers 3
    .parameter "line"

    .prologue
    iget-object v0, p0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/text/Layout$Directions;->hasRTL()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, -0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x1

    goto :goto_b
.end method

.method public getTopPadding()I
    .registers 2

    .prologue
    iget v0, p0, Landroid/text/StaticLayout;->mTopPadding:I

    return v0
.end method
