.class public Landroid/text/util/RTLTextUtils;
.super Ljava/lang/Object;
.source "RTLTextUtils.java"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hasArabicCharacters(Ljava/lang/String;II)Z
    .registers 6
    .parameter "text"
    .parameter "start"
    .parameter "end"

    .prologue
    const/4 v2, 0x0

    .line 76
    if-nez p0, :cond_5

    move v1, v2

    .line 85
    :goto_4
    return v1

    .line 80
    :cond_5
    move v0, p1

    .local v0, i:I
    :goto_6
    if-ge v0, p2, :cond_17

    .line 81
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Landroid/text/util/RTLTextUtils;->isArabicCharacter(C)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 82
    const/4 v1, 0x1

    goto :goto_4

    .line 80
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_17
    move v1, v2

    .line 85
    goto :goto_4
.end method

.method private static hasArabicCharacters([CII)Z
    .registers 6
    .parameter "text"
    .parameter "start"
    .parameter "end"

    .prologue
    const/4 v2, 0x0

    .line 59
    if-nez p0, :cond_5

    move v1, v2

    .line 68
    :goto_4
    return v1

    .line 63
    :cond_5
    move v0, p1

    .local v0, i:I
    :goto_6
    if-ge v0, p2, :cond_15

    .line 64
    aget-char v1, p0, v0

    invoke-static {v1}, Landroid/text/util/RTLTextUtils;->isArabicCharacter(C)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 65
    const/4 v1, 0x1

    goto :goto_4

    .line 63
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_15
    move v1, v2

    .line 68
    goto :goto_4
.end method

.method public static hasRTLCharacters(Ljava/lang/CharSequence;II)Z
    .registers 6
    .parameter "text"
    .parameter "start"
    .parameter "end"

    .prologue
    const/4 v2, 0x0

    .line 93
    if-nez p0, :cond_5

    move v1, v2

    .line 102
    :goto_4
    return v1

    .line 97
    :cond_5
    move v0, p1

    .local v0, i:I
    :goto_6
    if-ge v0, p2, :cond_17

    .line 98
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Landroid/text/util/RTLTextUtils;->isRTLCharacter(C)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 99
    const/4 v1, 0x1

    goto :goto_4

    .line 97
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_17
    move v1, v2

    .line 102
    goto :goto_4
.end method

.method private static hasRTLCharacters([CII)Z
    .registers 6
    .parameter "text"
    .parameter "start"
    .parameter "end"

    .prologue
    const/4 v2, 0x0

    .line 42
    if-nez p0, :cond_5

    move v1, v2

    .line 51
    :goto_4
    return v1

    .line 46
    :cond_5
    move v0, p1

    .local v0, i:I
    :goto_6
    if-ge v0, p2, :cond_15

    .line 47
    aget-char v1, p0, v0

    invoke-static {v1}, Landroid/text/util/RTLTextUtils;->isRTLCharacter(C)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 48
    const/4 v1, 0x1

    goto :goto_4

    .line 46
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_15
    move v1, v2

    .line 51
    goto :goto_4
.end method

.method private static isArabicCharacter(C)Z
    .registers 2
    .parameter "c"

    .prologue
    .line 31
    const/16 v0, 0x600

    if-lt p0, v0, :cond_8

    const/16 v0, 0x6ff

    if-le p0, v0, :cond_24

    :cond_8
    const/16 v0, 0x750

    if-lt p0, v0, :cond_10

    const/16 v0, 0x77f

    if-le p0, v0, :cond_24

    :cond_10
    const v0, 0xfb50

    if-lt p0, v0, :cond_1a

    const v0, 0xfdff

    if-le p0, v0, :cond_24

    :cond_1a
    const v0, 0xfe70

    if-lt p0, v0, :cond_26

    const v0, 0xfefe

    if-gt p0, v0, :cond_26

    :cond_24
    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25
.end method

.method private static isRTLCharacter(C)Z
    .registers 2
    .parameter "c"

    .prologue
    .line 19
    const/16 v0, 0x590

    if-lt p0, v0, :cond_8

    const/16 v0, 0x5ff

    if-le p0, v0, :cond_2e

    :cond_8
    const v0, 0xfb1d

    if-lt p0, v0, :cond_12

    const v0, 0xfb4f

    if-le p0, v0, :cond_2e

    :cond_12
    const/16 v0, 0x600

    if-lt p0, v0, :cond_1a

    const/16 v0, 0x7bf

    if-le p0, v0, :cond_2e

    :cond_1a
    const v0, 0xfb50

    if-lt p0, v0, :cond_24

    const v0, 0xfdff

    if-le p0, v0, :cond_2e

    :cond_24
    const v0, 0xfe70

    if-lt p0, v0, :cond_30

    const v0, 0xfefe

    if-gt p0, v0, :cond_30

    :cond_2e
    const/4 v0, 0x1

    :goto_2f
    return v0

    :cond_30
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method public static processBidi(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "src"

    .prologue
    .line 112
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p0, v0, v1}, Landroid/text/util/RTLTextUtils;->processBidi(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static processBidi(Ljava/lang/String;II)Ljava/lang/String;
    .registers 4
    .parameter "src"
    .parameter "start"
    .parameter "end"

    .prologue
    .line 154
    if-eqz p0, :cond_15

    invoke-static {p0, p1, p2}, Landroid/text/util/RTLTextUtils;->hasRTLCharacters(Ljava/lang/CharSequence;II)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/text/util/RTLTextUtils;->processBidi([CII)[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    :goto_14
    return-object v0

    :cond_15
    move-object v0, p0

    goto :goto_14
.end method

.method public static processBidi([C)[C
    .registers 3
    .parameter "src"

    .prologue
    .line 132
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Landroid/text/util/RTLTextUtils;->processBidi([CII)[C

    move-result-object v0

    goto :goto_3
.end method

.method public static processBidi([CII)[C
    .registers 4
    .parameter "src"
    .parameter "start"
    .parameter "end"

    .prologue
    .line 179
    if-eqz p0, :cond_d

    invoke-static {p0, p1, p2}, Landroid/text/util/RTLTextUtils;->hasRTLCharacters([CII)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-static {p0, p1, p2}, Landroid/text/util/RTLTextUtils;->processBidiChars([CII)[C

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    move-object v0, p0

    goto :goto_c
.end method

.method private static processBidiChars([CII)[C
    .registers 9
    .parameter "src"
    .parameter "start"
    .parameter "end"

    .prologue
    .line 207
    sub-int v4, p2, p1

    :try_start_2
    new-array v2, v4, [C

    .line 208
    .local v2, outputTxt:[C
    invoke-virtual {p0}, [C->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [C

    .line 210
    .local v3, ret:[C
    sub-int v4, p2, p1

    invoke-static {v3, v2, p1, v4}, Landroid/text/util/ArabicShaper;->reorderAndReshapeBidiText([C[CII)I

    move-result v1

    .line 212
    .local v1, outputSize:I
    sub-int v4, p2, p1

    if-eq v1, v4, :cond_20

    .line 213
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "Error Processing Bidi Reordering And Reshaping"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4

    .line 219
    .end local v1           #outputSize:I
    .end local v2           #outputTxt:[C
    .end local v3           #ret:[C
    :catch_1c
    move-exception v4

    move-object v0, v4

    .local v0, e:Ljava/lang/Exception;
    move-object v4, p0

    .line 221
    .end local v0           #e:Ljava/lang/Exception;
    :goto_1f
    return-object v4

    .line 215
    .restart local v1       #outputSize:I
    .restart local v2       #outputTxt:[C
    .restart local v3       #ret:[C
    :cond_20
    const/4 v4, 0x0

    sub-int v5, p2, p1

    invoke-static {v2, v4, v3, p1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_26} :catch_1c

    move-object v4, v3

    .line 217
    goto :goto_1f
.end method

.method public static reshapeArabic(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "src"

    .prologue
    .line 122
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p0, v0, v1}, Landroid/text/util/RTLTextUtils;->reshapeArabic(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static reshapeArabic(Ljava/lang/String;II)Ljava/lang/String;
    .registers 4
    .parameter "src"
    .parameter "start"
    .parameter "end"

    .prologue
    .line 166
    if-eqz p0, :cond_15

    invoke-static {p0, p1, p2}, Landroid/text/util/RTLTextUtils;->hasArabicCharacters(Ljava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/text/util/RTLTextUtils;->reshapeArabicChars([CII)[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    :goto_14
    return-object v0

    :cond_15
    move-object v0, p0

    goto :goto_14
.end method

.method public static reshapeArabic([C)[C
    .registers 3
    .parameter "src"

    .prologue
    .line 142
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Landroid/text/util/RTLTextUtils;->reshapeArabicChars([CII)[C

    move-result-object v0

    goto :goto_3
.end method

.method public static reshapeArabic([CII)[C
    .registers 4
    .parameter "src"
    .parameter "start"
    .parameter "end"

    .prologue
    .line 192
    if-eqz p0, :cond_d

    invoke-static {p0, p1, p2}, Landroid/text/util/RTLTextUtils;->hasArabicCharacters([CII)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-static {p0, p1, p2}, Landroid/text/util/RTLTextUtils;->reshapeArabicChars([CII)[C

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    move-object v0, p0

    goto :goto_c
.end method

.method private static reshapeArabicChars([CII)[C
    .registers 9
    .parameter "src"
    .parameter "start"
    .parameter "end"

    .prologue
    .line 237
    sub-int v4, p2, p1

    :try_start_2
    new-array v2, v4, [C

    .line 238
    .local v2, outputTxt:[C
    invoke-virtual {p0}, [C->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [C

    .line 240
    .local v3, ret:[C
    sub-int v4, p2, p1

    invoke-static {v3, v2, p1, v4}, Landroid/text/util/ArabicShaper;->reshapeReversedArabicText([C[CII)I

    move-result v1

    .line 242
    .local v1, outputSize:I
    sub-int v4, p2, p1

    if-eq v1, v4, :cond_20

    .line 243
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "Error Processing Bidi Reordering And Reshaping"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4

    .line 249
    .end local v1           #outputSize:I
    .end local v2           #outputTxt:[C
    .end local v3           #ret:[C
    :catch_1c
    move-exception v4

    move-object v0, v4

    .local v0, e:Ljava/lang/Exception;
    move-object v4, p0

    .line 251
    .end local v0           #e:Ljava/lang/Exception;
    :goto_1f
    return-object v4

    .line 245
    .restart local v1       #outputSize:I
    .restart local v2       #outputTxt:[C
    .restart local v3       #ret:[C
    :cond_20
    const/4 v4, 0x0

    sub-int v5, p2, p1

    invoke-static {v2, v4, v3, p1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_26} :catch_1c

    move-object v4, v3

    .line 247
    goto :goto_1f
.end method
