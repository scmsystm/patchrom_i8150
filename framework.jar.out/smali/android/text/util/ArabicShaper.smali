.class public Landroid/text/util/ArabicShaper;
.super Ljava/lang/Object;
.source "ArabicShaper.java"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 13
    const-string v0, "icuuc-arabic"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 14
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static reorderAndReshapeBidiText([C[CII)I
    .registers 6
    .parameter "chs"
    .parameter "outputChs"
    .parameter "off"
    .parameter "len"

    .prologue
    .line 28
    if-nez p0, :cond_8

    .line 29
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31
    :cond_8
    if-ltz p2, :cond_11

    if-ltz p3, :cond_11

    add-int v0, p2, p3

    array-length v1, p0

    if-le v0, v1, :cond_17

    .line 32
    :cond_11
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 34
    :cond_17
    invoke-static {p0, p1, p2, p3}, Landroid/text/util/ArabicShaper;->reorderReshapeBidiText([C[CII)I

    move-result v0

    return v0
.end method

.method private static native reorderReshapeBidiText([C[CII)I
.end method

.method private static native reshapeArabicText([C[CII)I
.end method

.method public static reshapeReversedArabicText([C[CII)I
    .registers 6
    .parameter "chs"
    .parameter "outputChs"
    .parameter "off"
    .parameter "len"

    .prologue
    .line 49
    if-nez p0, :cond_8

    .line 50
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52
    :cond_8
    if-ltz p2, :cond_11

    if-ltz p3, :cond_11

    add-int v0, p2, p3

    array-length v1, p0

    if-le v0, v1, :cond_17

    .line 53
    :cond_11
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 55
    :cond_17
    invoke-static {p0, p1, p2, p3}, Landroid/text/util/ArabicShaper;->reshapeArabicText([C[CII)I

    move-result v0

    return v0
.end method
